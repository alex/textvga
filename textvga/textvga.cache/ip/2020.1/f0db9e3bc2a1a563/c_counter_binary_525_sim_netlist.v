// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Sat Sep  5 03:07:56 2020
// Host        : T460p running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_525_sim_netlist.v
// Design      : c_counter_binary_525
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_525,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [9:0]Q;

  wire CLK;
  wire [9:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1000001100" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1000001100" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1000001100" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1000001100" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "10" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [9:0]L;
  output THRESH0;
  output [9:0]Q;

  wire CLK;
  wire [9:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1000001100" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1000001100" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
oOfSC0vWceRXCYC/1W1u1jz1oxZiQX7oYk5O9OtxUjaqIw75lzDk1AtbP6wpRQBgJuVT0U6yFu7+
AHHam1S4TiR89K4m0KUg5KaFo126Hrtz4sZbPHvkC2YCQQy+V0lGSRJgo9xyiLE0TM7rOswUMWa1
xg6xCW0ooyqLwKlAPJiI4H0FJQzX3Gj5ld848UySKfypORcGSw3XhA36B5bR9EHaU2n/XlL5wVay
bKNdyb2xPup6f5fjb9RwGsGzaQ7eaOZCSe4jzgOX7j3Upw0P02ZSkNrQitRpB4Y8FJQYomusaHXH
ruPWZhmm6gYDxAXFMCkbBnOLTyhTLmbSPhbotw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
U5tmOZubn9gYWqm79VzOPILgo/c/BTmk0nVZftcut4ZoGpr4VA1qGEGK4s28Z2//G67gyWyhPUXN
APrYH67QnfEDE/bUAe7SPUvuPANiDlvLiioHGjx1PK2MGaxgSmrdd3XbaZBoNgMbXMCWXHaKbNqm
yTLZtf0t2JnLAwlzHoYKFl6JxlRXnM6VMnjqBDL73UZFxWfnEvLsLeBzajzVeqqRsNVJwaCizy1x
9zOGTex9u8LVsK2CIjMuLtdMyY+y8a5kif5FEPorvFx88ChvkzBHS1wDTbP2R4YGa8DGTDB32DtR
QfdKgbXdF6bu+aCU9sBZzBWNUUV0t29swSxqmQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7920)
`pragma protect data_block
wVPRz0Z22mgfFGHZd6947tjYl0ht82JAkKSrwwb9BpcsKevcp5G1Fm2fldKQeb5GszhiU7g+pXYX
/MVxVxVrUGoLcGlLjwFnDWguag32eLEVXkkTxd480xGFtX3RSiwWtzxZi/bgjPHOvy1LKlKvQ7CX
FWa2bB/S6jI9Tu5z7nXTrg2zrztanQ54iAsLG5emMhv2EX1sJM+1pM4SfoONBV6BVZ8txAmhjQxE
8Cg89bM6Dln2BrIzVtu6A4dkMSpckp3ZPiPdbQWABl/aGYXlzoMsSAx7j/V0B7SqY87d4doJPUJT
XsDWjg8jy9hCRm3Hzf84W244Bk4oiVP8agzXol1NPQl3gw7ibAiVB8UvRSrJ272LDRoV4rFhCrVW
NktZrhkegwq8WXVhmXaAK6GB2E6slJNTWxuCzz5fgn/FfodGzvr85Lvov/KO/7e/dPv8JYmCE2kB
vJS/tFrAPrdnq0n3cU+yEwGpySCSDZNzPTWAnW/WZztMR5lb95hQVQpBiEmccw5GS5CPXUz0g1Il
fgxZCtt3h5PQaAv8c5c7eNK0vbWmqkRGlJEcyuNhBiu9dZU1+dzwklbXCZXH1KWtWwzHfbr5TavN
OcZtZqmf/fm7Ao/YPc3MQOtAFpUfkHnOXi91LMhTDtFMf8ZP6//u9XZgTYiAwX6/l8Q7MFXP7p+v
E5Y/fQ6iPNkn1A7lSxdOWD+iR+Eu6Cg1Gdtp5mLcR84H6NBl0wzIU9HBBxdCidQwf5hq6AZL8B0O
BjeFARWlyON52Oe//qHPYoWPJahsDIQOUA/ItKVmTDcjhKExbZFOJbHpQa+0MJK4gbiEOTZlrTcM
Sb8DzyJQVCvWMtgnclm56mNJg7x8v2WFwEXr12fho4j3zxNxBxQwhppgV/m0rkRYSMfClsSSWBvQ
54IXzWYX4zevFr/rkSLeGUbOBGRr1m+o9S6ygGpWtZDhoID/S4BrE9sMRS/1pq2Nrte+c0bDtedD
wvFyryTfrhmCCzIXKKQo17RjY0KX9BPjzjcL9X+HRDCPvM1nA947zuGWs/ULaQiO/eZ3TmULuJWu
KrUw91ZLauRkD0jAnbPzaIirLzxtFXDusyqHq8uaImMKTE+LnZ/bDG62ETMp3y5Baj35aXkyZ3Jh
3dvug4n+00xC03sOiKibjYSYxXQdTe2fk6HZVM75hPwz8sdO4Di7iMJ0XoevNXfr/gNYXPl/XTFT
loVRIJ8rPzxDYzbbJSg5xW+CMp8fIHUMHh0DMF4cwVR3Mf/x2CN+Fpe6uU40rkPFeF000IpOz5cC
1+0G9KB1Mo/lG83Oja2OOoAQXFptjlVaoYPzdxtDC1LsaijkZTMbfLdwzNKTA6dL6hMD7IkgCdoo
GRXRYS7fCluEF3f56Ddrt9MqttQfLpWZbTCp3rfHfteWM321WTIuqP00fpnqaZS4WYSzzy0LbSvG
hmBTle1Xz3Kwgc3CCD28IMfijyit8Fb6UKIUyzi8MCqJ5WL2GmSz1w/hdBeiF2bKNFyeKk6fvAC4
C/0eaAIhS9r2wmnWveR8qu/w1ntOAiLth2e8B6K8f+3UqofiL4T5pbFjtfhhW48d77s7K5gRFS0T
poBXtrNoguR7ca1z2V1NyWAEVphxmJJ7d0d/llyG/C4nMbfVIDgeDA9H/M/DBtfBUolBHcxQ6ivF
GImSGU0h/O39uh8qLPjs1DsZ16wBS5oeTmjwb7my7USHDEPnafMM1VdP2IWkwhRtEMkSjxNEY7bN
SW5Y+5rqPlzvp+GHs3nXpTv7/VpDzLLKyBjoOGWsBz/5v7uXKd52lQFdTJzywiUoa6XQt9pVsyvN
MHeG4rzI0AyyFFdc2I2qsw1nw2D98aRKVmJ8+XQG6WZDDkiU2DpiWX4HRPPyHzeHhyV5liFhUCWv
bSWnClFUdu2RIrV5ktDku7kRG7PIqUX6yvLPSqQJJ8rTb7JJgJJaMPJ+qdREx9ifKpUx/UqU3zkf
kUxiXpr4FjnVFVPFrsipyCyeXuDK8oRZOBi9wT3WDt3k2M5WYo631N3f3pSOnI5f50mK4r0C8aFt
BX2XyLqL+4uezG9FOT+Tt+CTQi26ZSU+F7LzmYuuoO6VDFldCbsn7DYPQvaMfU2pfUzBmiioLAEA
XrfX4Oh1NQwrEBASqt7pq+vfcG5idHz/uO0QJZTEPE5GuimSJQoU3UOt5lm+FGtb+25GefiO8WKU
SFDWUKYFiyelwkkFiDGpJS5UOgz79/CpP0cQ52WTDo825SZebcWoy0SsHTGhtmukW5+dw7xwanPT
oycJ7CsdAnQhvRpH6hTvaEaFeMOUXBhaNyxc0k10O7Cl5nM51LROnI355ifjNmFLGgNd+oKuRdpu
xQ7nQQoT7/jluCpey1cbk5Myderu7KE1A0vn1jpcdESMN6+nwhOkd0PshecYOy5vZzZsus8ZCKJq
985Q7ayPIcRinaLCVQBV7m1SR7z89kOtAMS9xfm99mUTaTYlau07aKRlb2tGXpg0y2wy4OZZOW6S
wzKq6UfiKiFHflBSdI4d5TFOCqGaGYf0OJAyC28gImAEqAmsQT/Me3Z6H4xcUsV3VLsD0k+g0/Vi
IL7e07Xpd1PAXYfWnulk+bMYr18jiWvSAjQ1zjW/THUHT7PzuTMker/ohhTflcHCopqBaBYW0YrB
LjS5lg3e3xzQxrhRxWv7pcPG4Sa1goil5nsRo/crboexekc78r1UM2SVMhEPxReUQ0wyQ5Q2tcRF
l27rd9qNOmlO5kTYfFNR2Z2WKqvnKqF65u8xYQbLUfOJx3T12Rm/M/dxDpzkn35cuXhAHMSkzV8a
Fa16K2KNGaBqtmQ4dLB7fvtztJymFvppG+yDZn2Pd0QxqQmOJweQkmp7rbs9ifiyAhTQ8MSpbG2A
bpGOQYFhQz9MM/l2V8F/KFqA8lEGeoxvA/6J19he0FxnasiZ6uBVJdKd/GXjz9bBTZm4LffrZ787
uyoDQvGRYM/F6sBgNhSySKCGlx9npevRFcX4H9WebS0JDH9EpwnBsjdRJ4JCd3HjzyE+kHFDFwRr
sE7azkSjtZzUoxYgpZ2idvToSttsr3y18spoh6fRjvnXZ+waqL/uxo6e75U4gQZgPRJLgoP+qQ0K
MBavypcfhi3FH0zJh1rPeSEmDTDz6wsj/bc9znwYZmnaeXMCRd9iYLQESOqIDHfGmCltn9BXe/XH
ff6kT6ZwylLHtkSHItwbGa/fLOWEArvHSLU5eWNwcGxkHcL35yLbPdLwE2ixhOqFULmSCtY66feU
goi2VQIdSjoBt7j1eaR6WbjXRbJg0bsIv7POGS0+OpMhmKSzypXuL1F+CRP0rx96VzGGe/khyc8Y
fN/dnG83tgRAXfn2A9Sf34WOfcjrdKK7e7fFgEZ03pYZIEcFLUt2ia5dotWiCBL4vu4x3qY3BacC
uTZyQlnldMt/zpJzXbq/q+g+uY/mtlecQ7OGW21h42/5kSRnetpYQJLcsjDRH1yAgX9mOP63OybL
wX3BSOrZIOr0N5jJbhMaySwe6DuSiMsZWq1oD4ADtxJYuFmyDGE8elGT0AU32eOSEVGCFfC0xn7o
SJrDPkH+ZFStyhf7EQEpIN/nbUyNIhPjN7fuDb2mshPYtML6OzvxTQhxCa1Yywm0EWSRV2OPDtGp
ELstAuJGV87XpOuchdI39khajyP2cx+iiF/E1xtg8Feue89TMaqI5i21Oa+t/jsJz3n/Z5JXFaXb
3VEqJZibETOZmAk+mu3MSzfEpXqQB4ohGLNYRIIeqH2ebrDk46kXueZzicErcC60G07jF72bYb/N
qaTgi6P5DgTG9twtVDEh64gSroVeRIMKNY2UaoBFuLqKN0R/QLI3vpSVnUh7GPrM6HHyVYguagRz
3FWnEWDsxudpvUXn1mP/RhOSaXRiABtyy3ZmTJW2bW6eArSoTG9Rctj7dAjRwWeC7FxsVoxAo86+
LufNma8mXT5AsryEcnh0SU6iwxjQ5cL2XTyHEpjPDdRJuIYRbgaBpUb0UpCv/YkS+feAkGM38cu/
0FC7BJCqFlY8fNaG5FRoj5EBlQnVFUXT/8E8CzPRDn+mDV8tB7DANBHqRtYkabsPDx0IBtrC78Z0
yFjUgDQcIDcnN0d8JJ9A3zn8YwjUyQqIH0pUdf/MTwP4vky6e5bjMpKKiXaS9R57FcxIxDf+BNdv
JZ0CVOrbhJcxF9unXGx2HeQpM4j35WGWtLrqMXlgLoH6iLxVglThjA2ZVpZfjNXgkl0yMUu28LEg
Bb3BAMOzWnjov22l/FQBwQi1bg/S+AU4OjKeLotf1WI1DDPjwJ4T9Qxvh/gG8j0B9H8wxesAhkWJ
owkGNPyu/3V/rMlpVuiRNKiAtqJsjmYwSfGwc/XkIjoCeDlc8KfqWZi9lzrtl0A5A99MXZhcbTJc
jKqgG/dEH9qUWKyWGHeNROtSBtPjQX+99dy+em0SjxTtwRHg4XFytf982MPu9f1FofnTgdklTWkI
CsyuAdFGp7nZ2eAQPSdp9NECbHKUgrtMXlWCovssI3ZgK5T4TSDfbq76qQFBfbnVsOYn6MPCwmz6
VUpzROFQKbSb9gpBlMpC0QK9tgAnGVB/WGwG3A38j+HQWZ1Yb3EllQxp7VXB+b+YQYXbSaUPhs16
jkHthGWb2VZxz6ISDsjh3PW3hx4Dm9B9EYXB7e+7/E9eT9T/qXyBG5dfwwp3CXu7qc/YoOUglCdr
8vKoUAQX+3rpEbGDaFSy1wIZ3QWDMxgoLj6gyANulRbStjMYH68zajadpYsKlcBwNeliIYz0HCql
sIOArc99yrGk5Vg3ry1BJxry1lTdimmyUqip1+5t9p/mFJBTpKtIsf29B28YUfaNHzsVeBGbPoS0
G/Xw5/K3R3yqtNrQfzeAPHMD3Rpok0LFyfWFgyXViGJ99V5y3bApHqS9L+x9m473bFNlFZqzmDWH
TAJ8/fvvMXLkt0iM17za5l2LKehyfwncUhSFBib6pKRDm1e/gGwLZ+MoDMfW1Sx9jeM46LuuPzZs
Vu7XIAuLOiEf+huJ4s+Cro5OKmg4pnlgF8SNCKjZHLbBLi2k48ba5HAetOZggPps+V6b0d2wmpZB
unLaify4D4B+NPfSAxH7yuVSzQSby2UDxqNZgpXGWAgIHNXLGqLY/hX2fR3IECzUeDZ97Hgdp/NQ
2rwYWYy+uhVwSPP9Ee15q2bEuJD0XTx0c8I1uYVbxUciItpuYJmY4vtd3AUjcZZ0komSfXd64up2
JfuENkmflVFDi5lCjk792gJeHFNXZFhPbT78sHVzPwltKvvZznaaqeEpiF1zGht945In1sxESot1
pGMZXAmDX0ABmtdsNYZJbe+NE3DZhXRmoWxorgMmA27I5FnGzEWhnfriDJJwLv8NMORvEt8CRn2M
XjJGGpI8uSPsP9K1o00FkxAIN66aAQu1wsk2tUxbWfwWn5SP03U2ABl6O1KqZJi5vnSIwzFawHYq
3kP2rido23eZE5aazBC2bFnyQdwkzgg+CLK/2ayPTrq8jUxiHWuyv1a3tQVAcoJoIqocS7dG5mNT
tyWpkeKRttEcScnJ7OggWBGovFtI8NUthStotNV3e5uc2i+lR3gg6sAMc8ZHD7l5ucGk3sZc49l4
oRbNwSDWFhGnKgeOHqF66KZtiE5JrTr6Vvf9SEr/hfX3R6/bjDf8lHk4n1RnNi+OSGL9vy8EPkrv
jEVa6nUTKrL2HNU1HUWiRyKfjaoR/pE5Q1f3HpbBwjKdUs3vR/kkRCQdr+DBRS2neco5t5F16H4X
72IrrQeCWNPi9v9P5XDNcWE3mVD6WnNEEwgTuyFn+X+QUGb5DCIf+bQ1QnItNyR6LHQvcrbx1eVa
L1JC5fdE/AP+m+aJMrv7QF4W9g86Y1NmpcstBdVGO7wpXLfvDVsViPkHR3uKpsxz2iwE08oNYEBU
Dj7LwZ3N+BOiLbCpejQXzLwTpVTO66k32BZ9bT4sLNaYYaHce7QChxQiThe07IxlGB7V8qQKtHgv
aSeJjK6JzhnguLlry0e+3cA5ASRo04qXhHv6ePvrXIQnPDWE9BN2gPvZB31Nk5uIZluBkjbR8LNa
fWUSilGzZjBZxEgO2TF6s+3aazFSIssNN5B/mdEV01nunMgsc+HA+zmi+teC9QCyI80oLOK17SSI
2G7f+E8izy9wA8cZYdDgs1gR8QlY0Llcpec5gI322WCD96dfeg7XVzS7xi8rX1cfDw5WwdCSzEg4
v4xa1mcREcp1SbTJYn7prVEXgoYUrszcYqwmZWDIIAU+cQmruEjlIYWT1bk5xj7bgUfXuSuYZUnc
cPWtrLNJwZr2wzS7+yMdAhbccSiL6/12JGzew5sTkrvuGXhb00pDvV2VL3DfLU6vyHlhttm0HqAv
jXSUCIfftcvCg+vIFfChhvIMbdeQBbzkGen7Ix+vK8f0JvChkhKvEuwkcRR7BSwK/miVq6QEYTJU
rH+jUBgep9OV99U1Uc1hcveR5roeAl7RheXCmwVjgJxXXhTiKVwU+x2mHYIlP8TlqPh51AhRum0G
lMpmzhaMXOnSwnd7jC2YeHXoFgrXgXUOHb1QfvirwrKQ4uTABa5MMdd3ryH0thPHiM9elPdO0sbO
co7kZkpWLasGqaCbU1Vyp3ZHMeWBDda7IkPSBwUL+a9ptm5yHis1xBioUXnMx5cEc+3BAPOGZvHe
jc3boFMyP2LwKcmb5HvM70LOOu7hYLy41jlDQlwiRxlrvSIc/bzMTlwscgZ2ehCC0u8cGAME5S5k
BzA/+RtILNKf5Jv+PBqKGnKGlrpC17BEZQbHimCmfNXj1ANxbF8DjegxoGtl5zaBMRqH84ymZ02a
qBoZKDIoz5O5ov0TnPHgJl7kHp/3GJG52Y+WvcVLlKEvcQ+cJCBXwmO2xY6cZU7It6ey7ywm52ao
8UoJf/w+cFv9YeOLr1A8dIIYQdPGK8s9wUXQ3lztALUewZ6CcKbnXMR1oOYvtUN2xd4hp11hyd3O
NY25PeJqnKTxkMzZq55hxbVTXWvyk3NdaJPU32FfTOqFBQUTMmG6u28uHCuzUJc4/q0WL0/9AZaf
h+WnlHnG9+V28xxN/nSnwLmHPRTqAVa+04hQ6zcb3RrkqcJo5kcC1TuJGlb/8CYc7MtBJxV463E8
8bHHbgfTwDM0idDuGCXlEuOr0Cjjdd8/sAUFouD+/2D18x6pwhUHUnVsjSjytAPweVZeviOVHdk/
aD7b+HchSrWmkCPicSBrWGaHF3GcWPHhk+hZL+FVE6V9Dewt24fo/QPCDVr2sjDw5xi+ZDKZbVST
3OD/Z1iQpME5s8OfKZHwSYRITmFPVroKy4F49Dc2WhjiJuOPItk4YoPqt9JmnU3GIFsbbnCtxPaE
qlFTwiowVhVcHXEIRCr4Ls083KQBkKPlCwvY6jirm8C5jYRBu2FgdfIZOBQsHRJzxn8k4Y7Zk1J4
PrGA4iPdroIkMpeKuaZdQSyw7ryWia1EVYFkf6hxSYkmLjsb3NzO8db+Rpm+t1vFaFFKcKOVHKeK
z5oRGJfXXcW0P5uQpk1L0yeU573/4GPd/YSBLrXxowLthTsr00tvXj4N8Hk1Vec/fp0yORevoBRb
Vj0xHn9UtaWjzBJk58R3nqki9i4+fDZLPCB5rZDbCxJMcHu2uhBZLWLioDyKiQlbv/DHqSEw3gIF
caflv0XEtd0e7eamBI8ReM3XdXkEOJnivBtapsIVgcq67/LgeSCSEE6A3nFONpQaEzRs9d9RaSdy
cZy0i5qM/9F+ieSFy2Cm+F8b7vJdlT3RAtwbz1E0jmQ9gm3NnpdQ5JVFNTzebxGd3fVRfw+h7gTo
/edVl8LHtwyDM983dEbgtR3tHOBrJYJKd3FW1zbjXn8omVnKo/2xSBnN417g/FEqqGlyJKVbrcGr
guF/1sNCpgajbBhtvZk3j13Rx6Z48JF9hlQX/X/iqX40EwKeZP0oJaze2n/nyE7nIEh7U/W6rr/c
fzylSIJYoieYxj8PV2ILB7AGTqMJx4+pp7vONvcSflqNlo0FCXEq14QzxK7vyEDFIEHBwRBEMt0Y
MiKeC9xfQkcxruUgFGK1a8obnvEBuB68x70o8nmpKKQTtiyeBBUaDhaiuhAewuwoYHmrxC5v7l+m
4bdFpAWzOMuJtOBU1dmHW2vPrdsutaDE5VZHBGa8X8m7nKpSm1LlXQYuTp6ftTub9j/Pu+l2wA+7
7s5hcMErqsT86alHqoRzrcE5Pmkz2RjH3DDN8Kuf4j1hvyrHhmyV+4yVZlrCLQcSE/O0Spb62O0u
T2SHgD8sSt5igKTXw94nxBbaVlRca/uLaeEHmgT8oLbFpWfln633Tsssn/6QFuvFqd5B+klYh9OZ
a8FY2y50yF24m/5QiYS0eEtCiF1NcemR5unUYbJccLBF2Kb+1/tquH6009JVuKv4xVpanPG1CWfO
ZgasYd3OadSsicRs31Nk7mT5kpeS1RxAlHryDlWSp4gG3e0RRbhekgta4qVBVvrM7byVD18JNaOD
ZpxeCe7lGDZb8yFjlEmB8VetWF2M5+QJ113+OygDBzES4Kv11Qp78F5zVCil+Pqdh5LXKaIDKC55
p3hCTnyR7b4jDEYrOoVsYw88Go260CHbN92GVI+P6xGPilbFmDkw6hNLnZ8X+L3XG6OcBIEcVm96
gvXNAgEaZyPkbPVZYk90zXZb7fNXCIY1k+0a7OGSmGT0ymKF9z4kJ2YmLwTEhsrm9tNyKigAmTta
KzGsFyu6KZH96goXi3LJ50GytmwxaUYy+jnShFAnqGaCDlvPO0xcdzezCPwxURsRVTaj1w5/KSOc
fWxwYs/pgYBHf2tnPQGu+705F8lfy1QxrqWCFwQa4CastXBCII5psQ0ZO64Qjiq8/ehelbEZdq0I
AOezdU2yxHDCek4ZgS1utvT+d1teU/9MHuS98HCFaO1Ls/tG9Llg1vfSZvkZlc40ab69ljiVupfJ
K1Eh7p56q2sTRpU3vPBca+N515Zlyqsxt/u5tjxyixcrW5znkyYVMtm3KNUE2rkddXwtG9iwpLuQ
Is0ACc9tzv7UDyPEv/Yy6lGqqZ71+nNnQs7BYazNSLHvd0Q8/jYEVZ28xeH/Lph+7a4FDHB7XdPn
rKI0uxgcdZxs3MWwNkXDlvQPhC0u/D2E+k5jDSytlPTGr456SelhPo/hjVVMB5gnS+D0Wik17GqG
BCR2pFUYqeYBz1sL2NQCbMrIkFMwow4SSrMhgYgkxweLhjhUlDUW8K0uafv6puXGQHJ7G0vsDYOk
rNyo9mo8N3jlmteKMGsC9M1ED66nMJmtObsg0MwjTtfUlpNGxY1aQy24iDrm+cYNapyEmf1emp2s
C9x8xcIPlvS2GgKwaoOd0qar1CCcqDlwqdIQ6idH0FWb94kTqsL2nxdIJiFa12Mh/qrH+tMYiEQx
OH+U2gUQnBOeqpxzIp3Q7RsFsy7Ef3M3qCCyLo3sx1+/pmPdbp4qg23Ew/KwOUh/Tb7axVGWmzCW
67hYZUwMyoGwMPfmia9CD2AR45SsDNzSTPZv7Cdn5P3dNJqWwwi7W4XBpy7qiYWvyZihLtqmQhzo
vDxhYyMDQapuInUS062qOrNq18S4IGKAszev5zNs1/kGUwGjZj2nhprKXaZCWriRBFop3GOT5N+k
UOI1HqAnYfwdaj3hfzqheJP3eVTN2YELDtNqPevVjkeXeFY4NDg7ELQyMax+FoD1YAzajlJoh3x2
ORFedD8C2TzeStQPO4qi/ug2Bn+ururtoYPOje3jXTVSlyWXjUlN+eJfXqxWg9JcLSvVQCrEFojT
LjnuDm0NRpO13cynxEzzZSF6hLTp8vs8x41I+BCcaWUX+Kd4xBw297DpNMGXtLDoh3STY2x5kz0Y
zv6fNDr+Tn9wSSgx8DX+PDPNmRwO4HI1KrlOU3Kq+F1T72v1KStX0e7ASlqlT4+QaHWVTpmgCzdp
wpSsb5zMmfSXTaT6c+t/9wa0uGMNaeaMdGL9uUgHnBcEyRcXyOhE06e374d1Shy4gQd6AhRtM2kt
zsFdkF0crN1zKVPC90yem1Q784siP15nCI38OcWb3xc7jzzg3vZZUgeHFLuVpI8ultc5YtrTIL1m
2CkWzSKLahTmiIACUaSpaZHr7GX9Jgv5GVR7PKRMU69hTXr73DTiXyPqzVoOgQsetcoTjr4hDVkC
fIP9N4+jFk3Dh9gAPxJhrvxpqdGKWZVZ6BRes67opSp8X8bmW8RQJh4z6pvhYojqK3DooZ90slx1
q4pxLZV5HGHuraQN+hik4wCoH++G2WJCHhd92FgTSAew+W8O/afdbiAJdhJ8p8VUglvTjK94BHVc
AahqsMpWI2oDny+QJVD+kSVMMIXN698kk8d6PGAF6RnEuoNhBVxKuRdnaR2eJ+/LcmLcInTpCmhb
6o2OHbq8LYC3D+lI8iPwrJHPA7oZJDZqjE7TOukMqukrPyAHjJzZR6OT32NXfs8kMwDvV7jtHijS
VQVnPVvii0Igoi8is4mJLKpICCr64YBzInxul5gX6rC8UnCQkUc+w1rLrlhuMKlc+Rehw4cD
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
