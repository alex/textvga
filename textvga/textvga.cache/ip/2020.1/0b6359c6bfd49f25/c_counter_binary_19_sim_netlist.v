// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Thu Sep  3 23:01:46 2020
// Host        : T460p running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_19_sim_netlist.v
// Design      : c_counter_binary_19
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_19,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [4:0]Q;

  wire CLK;
  wire [4:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "5" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "10010" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "10010" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "10010" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "10010" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [4:0]L;
  output THRESH0;
  output [4:0]Q;

  wire CLK;
  wire [4:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "5" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "10010" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "10010" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GrF+7luj8Mk0W6qQiPfJoObNrDXX34ohqNaoIcklKFXwLHpnQvYE3XJ3vGeBaBdOMA4vdfDsarSq
W+LVO8qDix567NV4KBAV6qN73XSZg/qNY+t8A2AQpp06LJdSQTioh3/K4k2H+LP9GT/7uxtXiz7/
cciYwXE+weWQT6sVJAYD5hiJJmW2aZPHoR+ds+JFpPeWErzEzcAaqR6EDDGqYZU1eLJr/gm+mADZ
NQb+crwKkdcQnWei+t5ImrLR5JkO7qAjVxouYhueTxJ9L1Rwr/TVIM0m6KrdGxZ1gCxbNLsxYvlk
dGTkWPxGMSR7u+LCU23whEly/HOOCPbf4wdPlw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Xbm6XZaQvUlhZUdqKF2chKsyo134KNliik3TRXDgf1LUp1HGIocrmv5qUmSnaSsExfXvZySdOLvE
ezHjwbGza6g4S0NO1iwVo3DXcRCz7a5/gl9GOjyw5u6E+F68C6UCyOTUXRAyk7BMQNGm/McB/Qnl
CogYdA5oWZSWn5aEZ9/NU+7a6a+TmQkd0KdunuAargf8ymQG9Q1AYBSV0lo5Ft1zqIKiBZis1PxS
s/Kp/4hK9hz9DaKTK2t4nnuSoKBAXO0k4QHvLryInhBzwC9bCyA9pVc3rBywCFYcLCGXQzFjdpyF
05vbiZmkagS7/rQdejMMvfglwYOIvGUeHvrCWQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 6176)
`pragma protect data_block
wylXNeTy65sHSxODex8Uo9StHrzl6VPecEgSE5g1G+j76yxCWm4sCAJ3rjcVVbcUtbr3oGh4pbtK
MONAdJZOaOWSN1OM1LpqAY8KHyjQ+PFFwqBRk6ITFEdnvGa8vCVRWdMXS33sCGJAs+jrY4o3WI6V
heiqkLMgWtKpTCHQedH1vw2RBGfrNvylnMm20D5q2IDZzlh1k4cOgjeUZkDFxkGoX2OGA5VH56e6
x5STCgV8MxaN6p6df/9VqLvlNaGIkaR9yMYZQiJSGVLTx+7bAzLKGWrQnmbk5+oDXc9uRmLiaknK
9cmoLd4UdfLZ/Ve7gN0J096x1skfZdbG17Bq7bX7vimkef1bs2OZDWq3ZEBApALrw8ixaQSHKWQ8
48tn4iFgkRZN3+PZ4XjJrw33VsYNXGotZH23KAZ/ync0bLnFmqjEsLgVmfrAqAl0jHTcK9MeaO8K
ju7k1UNUW1Fs78+bdjcQOQjntdQKrxuescAbhTcAiMyxh/rxtkSN6Cisy6pd42Wz5LShnlmdD3wT
bXuTwU+yEH0vwVxgMBofOcY9gKIojt4yWR2xcxOYHjGeoHPDZRlnNesWs065uJvIVXEZD6F4Cm9a
l2/qMYe04mEVCYzMImGPDTECVOFEAoL/4ut9Sn0FVuTGHKaB1yxdP/+gJye97N4L8CwKbc60WizB
/saz88scVHwwh74+nN9JaUgG7X19DjaMPtJAmkce+jlt+YM3rDyaIjoHAhUfZnYqSYWbJdaL78p7
cdQVwQrGifdm0KELWKG9Oh3GIm3EKEVkDnzXxXWZuvmmZOpD3xOaqC/YBWpAhR602M3YqZhGjK3H
J9GV0o/ZwhNi2By/ZfAyAnoSw2Kr/AnCRL+3QWXB1qYtHdZpLRB67SgxOu2rFHbnOaPMyedZYxOJ
dZ9kM9VHeS4KxPv94Y7y/cZN+25LYP2J4rOmCwMBtEkVRUKwR4iCIEqtHAJ/gXCuPsBhHeEpOQSE
J8fBpuIyWz/A0E2YrxGNAUJPCkfb+BgqKbHWCwodNyfnV+1B/ie6lQ+VX4s29kHnqTYOJO5jz7zL
bHt9P6OA7FyyYrA/+EInFQ4nhdJtY1JulC+FUEGYEhyWvJAuIQKJC+Mu+E4efAdqwQSPK+kEI15Z
2E7yDEQQapaRI2og/TDuB04bn7zSNSDDVnXsxm43OExpCfPAzb0DaeObCnqIGV3pKV+G0YbYf752
2pFm5eJMQlX1vu0cpFwQfsGltZBeAWO8LQpPadOe0PaD++kHCVeAJW9B39my4v77GUpihO1o/HJP
qM4H4r+E8EeqEesvd8o/+pPJz8C774BHdzPKAQQK17gmd1/LINYhimwUQ1tEJroMdjWSBx1FXZAl
STQ5dcxJRdYBZyghqPeGvRVo0ioVH9kkyDxdv4PZ9z2b13tNooRqiQ1npqjVPcZKRWDUOllcp3MB
mPycxDzmFaQDfpExozOGJ3re9jWMMGDwt5UNM+UpgyUNTPHYfDXpuPEFimHeSFmnk7/sA5EHFxZK
IexuZznK+OVxx5T2Tlh6zxsmkWkPv+Rj8TT0nBkO1E51G8HXks5mgZe+fPX2Y/8KQaT6W+wMhxka
GED8j9mOC0phBxqR0rgr8QFZxsUNJ/pKrQT83uneS398KYlRZl/Vg8ikG2YEj8Na40TAEYkRERlh
FdORRIkaxdF+OHSHIYJmEfCHxOiYlbtbbpJYoRPuk+vglxY2V46PcBrBkQoxrFoVTc3NhfprKU1D
xKh3+glkC2GNibqrluAoAjKTOwNknvl0tHEnkSDcM9zN2/FNx7cgQ4aXdDcpkDeOtmgdsd1u+EZz
VrqtGDJ89t8+xQivbINieVZXCDU1NuSbc+Xnqbb5xs5BXIRBo1yiDe7b0TM3lLXvb0NVaKo96VIS
KNZ1gVvDasjEEHmXEsKReQHgadzjVqBzJpsjHNtcMbBRliDsWgq7KDEHtr5SU3ywg9ovXKquxuDo
J3+9JivGueWo9Vrtg5JGuuc/6nyJGj4tdzXyOG+xtDk5I9vXe00zkEJD6gropeelNGBXB9rpBjQD
iaOWa0JLLuIhiqX3t4S4m/3qcNls/OGD9S5cabVpxwnuc2HpArgtMqe8ntragFOPfn2ybzLaixbo
Z5SNiQ9QYN2DwmD15XwUE8T01TAIddkIzvJnNlw2mskqe/F/aE5reIg/pbUFzTpmEUoQXjKUwSVD
XmNuGo7S+gU5TsiD9PqbDNx6PDm2g/iFQ0qlJsl2V9K38ISsKc9an7HAjLWxGZ/5MWrSK8J1pTin
Q/1h4EIxzVi20YVaSRFf2+hYyE4+PdMI3L0KYO6DcdohPwsnYAM4KtMxzq2UmMJO+JOqlTFPx7bJ
/EZ/xyV9+zEt7vwHh8GoW/0E8H/KT5WRme+TCrFrWXCza2ve6YqZU4J/XHxyl6MjhgqDXuaZ53f6
LuUXeWweEa1/Ofic1tJf4yuFtmbeCHfRSy3dLcWddbAFUIEy3qJbEjbpMIqNpn1gWAzJpTxBPxYC
aT1pUj1NEhxzY8IH8ZrhMqz4IHHSVtgRiIksk5fXWzVVamRekwgl5gOzl4++NNODHciq1OvXST8Y
qUMhuxQ6YwafAMiUxwhLjovzB9Wdr7A/wBmszwYY6ZZgomd4BR71xjh4vTlVsTO5LF99WwHs6jxs
mwdUUTQuBpBqjZzVUOMFoZzFoEaSIt6nP74td4XeHMIFvJuUZRcIcMRphxlDXo1brL32Py1Oi/BH
meZ5A3gJZHjv7Vjc39XzxSDAuI0eRjM8cAWA/yf5GzG9HWLCPiXn7Yc7a3f5e2azgFmZtPFNCaPt
T/2acX2xA5lnFJvkuku8p5HjHEBeTx6iocP1dV5o7aOhWCKj/vASKJOqWpKOLm6SpYt2Mm3qkI0F
vuv7XPzbQ9YqlFtJYKm9hmJ02DBMZMJSV1MEiY4J7ujfiGnjjyYRpA+GJC6klsXf4+quknvKSIhG
mqgF1nfthMtn4+B7lvqWcqN7QH+VOeo99focdz5MnHn0ryQFoQUKbSz2Wm8mkxEpUxG4KWY13clp
9MQU8eyAAq7Wx7xFHehkYg/JiJDg7y4BL/0UI9wnwqjV/8hc8Ah0vrD94xlKcUCrtVDmxZKDVcue
1dOh6zM0JXYN4BmrYk0mWnoXq/K69VYcEeO+a7ZBSqTOs+jOngoTj7nyquCehA9Yi/TGTHbBmk/F
DRgJN8o9aMCWfKN+JIN9WrjZr4+GN27EzHvp2vWTmziiUCPxm3a5jlfW1/ydBwW8vbyUMh34hhjB
9mv1vkmZZjKYgp2XWKU1OZQaUqUc58Hr828lP02gpnFeE5zihgSp7iSfHzaeN1hD0xSzT4/t5qqD
g8LpySF5x+swgMWhUv4w17E8ZhW0uqnFCFedt20BgIMORBFIqLlnWWHATy0kVZwB97DwdbkKb0Ju
R2lZb5pk8qHbl2eRZLMSP/40Xsne5B7BMl6R+Cv6LyIsXd/UP0cK9gu3Lohusaj2HvPlrDsSCwZz
jpwCgy8+RDOY/2v1rgbhY+7JXlZc8BKRE8ugwGiRMHpxClP6XGvqMZA3sBXPhWlOdVWGrRWc3ibO
yBeGGubCOBvfoPtCOwS1rP00gMVFClPYtuDMIGCeafziOe+PEwedC7938328ahJyUdzIdL/fj99b
BgHyUfraneVIKNMrtDQIG7fWd86B8Ld7xyISsy7mHcrEXyhZ58566hILpAPPAogTIr4i2Vh0ZCJp
WyRhNw1g191ZCIKKkdFzEMHT84RCqL8HVYvcvI4lrFrUBnEnsX2HQFgN8XQxrzs6KYydeZJLknv0
y8Af+o46AI29+HM7qjPfgYgf4G0RA4coCNTdlRaKYHFJnPlvPAOyGdLD0US/0eSLc2dGkbIPIBcj
5c2+3zJkBm1qQQnACzZI7O6RnlMvPh+KTI3cQw42kzjzuGRt34lsp5Q7mp5/29b24HNzntqSLuMI
Ufhf7wmU9CwwpZwErOj0FXRwsPp1QsF4zNNQw7wM4b+ygsXhWbu3cVfe2xcw1icKJ15CnEjlO9Wm
EW+XEcwE6uTKMBPvnUW0294ZXNrB5lrQdpOkGZImAg94bFwEdM/t1xuv9V8B8APwQxRtaINI260A
K3YVm5zzsu8KnU37UuqhGOR+J0FLIQDcCtIb2q3N9ODFHO404/U/MZqco050cDJcE74VXXOogYSD
d/jTqRHTwj56nNZwIxCvR+9oox6iI8wMekDZWSnd3HL67OFl0GhXhVzclo4R6M/lngD5wx8342DZ
hlMebuRW1O1+rlTFqrsMmGvAQTpHniY9SuZr1muAiDxYjRnql6s7scyNuFKObQiDMvkyOxy6LiAE
4izdJD+eRWmGN0DfLCg82/Vf8/A3jdAIyY5Di2vQ27pcH/gBqCFhP0YBa/Rhy32b24Up4tNMmBAT
0AZ7j++JP1oXmNm+Lo+497AUvM+uYIfyBhdufDfmLGmiXcrS8qhNmEWVXSEwej5sVcL4qottHKXb
+0Sz2mpP+7LBrW4DDb2Hosk4XRzHRefLGpx6RdtfrC8RP17kK4uG7NAwMMLAtA7VPr9lIWKuBIf3
IP19WRkFRz03Y3UWjkK6fIFGnLqbvpe+Z1m49njQJEOgtFf1CtEt+UtDr3zT+lTUFDL9efI8028g
80saiQNNVYV7u4MHUVfrdy4WQq1F+rHdqRzdnJVqTrx3tLElm+Oorp3Zd/KZIV6lnggtAqcyK4+R
qbQjLm73R/GId65NjkoswVlZ1BSgcB/mos3eniVFSyAvaCTUiYN9nqiIrW4scbrkJ1i/7OJLAl2w
9LlEPjg1xXtz+K9sAn60mgQ49D24hWHAHR2YiP0/WwCZ7rwLb+3qToeGM0NdbEXrdsQPikI3oR8R
hNopbfE+7+hN0//+dgFEb0lbJHkPZSdsev82GIAWC3VKEHWO0D01ck2zozqve3Bn45cb8F4H/oj9
nq2/sCo4ypp5nZEUkrjzMnJIrw7smrIXAL+AZJ64B17Q48pgpXhGKsPVjxW1QNu3n8RFLDjLmHtq
zcgM1a9JfuswkyXCgbAyydOshnouebOeT6qQrD2QaO39wgMiu18FGrszio+ARBEHd4sAEFqKs/kD
T8CSAtpYuqo9cvaq31GVOwz3S0HOWP22i1Em+E6fOJWkeh23s3toZyE953RNPE3jXOSjYg8NDlrh
TX9GFq6zmjAiWr7U8F6bu2PRJySobqAgc7E0MfIkcpxK2tZTgyacJZ/gip6BwZC9svUa2+ukYoiV
LWq3xCQmvEnY1uHLv/s4N7CVSzXMl1n4IkL5/nEWUEQBXqVCx1qibbJseJL+IoMpzsE/KZJ5N1n1
+lG5f99r2+RDqaVRO0plz8pd0Ucpl2+mEVO75BWxrPvPoEWO7scRFRawbyUYA8keSitoDAeO9XnE
IOsAk9NEHo6ER6pVJz/c1hddNbzC2m0zjXAMNMhvQRGFILtde57YCu4eM3T/Xg6iNPyTKcBmoj4e
9+086AO8QaoY4ywNz0fLMXirBx4e0Jv9ZgD2rILbLXgCZ370XEXcBMP3150OvLaC6H7nv7uvWX9U
n1okmdE8S5eBUkAo0V6t73pxuCrTdkiaNGRLph4EPNKii9T9I12imKXKKeT7D+DcDtY6pUNBXoRH
+FXjhZkn7pxu6JVIQApSyQnVNlrnrVFpjKUF0jlWgGvd3z/DhEJEs7oHMDHvMoSietssYk05fiJ8
IHpKhoYGBmUuXTEgvtxCAJ4qi9CJbxSAifZIiw19qS6GOXcIeXUlRAdFzKnSxRVvnV+TOCpwLj2O
0WS717yt4NRSKqdtikgTL2iVKzhnfw6rO42CsFM3LfoekOqwToOuCn7YkBptpXdZo4b6Nw0ghmLK
oFnZqmi7Cdu9vWZ36IBHtcego+nKyh5H7oQdHCn2lzS9wh5w6OwsJ9BAZP/rdlxBkNA15Qb8Wng5
RxTOYG/TMl7JpetMzWaw/Q33j4nwcOpfrp/l+W0Stw7EwYKL74A3arfrUVlWR8Vr6RLubuz1MTsh
ZnZjziff1DXHl9+JYHUssxpAS5GKFVOCUyA8C8aSmDPT2Cj/AhPggSC8xBGyTTOCmTeMGOKBwskt
Hwp1YtsdFJc2giWRHgJV3n/iUESS8cxxTE5ulXSNoESQu0diagdyJV3NAZ++rlZ9SqjD0Qy0RB2T
HPQtNzvOy1JUGP97SgQYwJjokA+PVya8NulbzarYJaUkHoA0yyUdAUUhyI30K2t3IzYFX2ut4V4p
99PeWPV4ZSMxaw3KXBYALCE34ivEtw+GIj46lufnZmkxceTZLVaWws0/VNdli8EvYaX397G+OCga
Mjp7wmx2QdC2VVUotxr7w64kejtVBFPe73UWXqVviSRjm1KUFuonibbXbRlRKEdM3fID1/yK0Ojk
UASu75iPmuOYjqRTBTTUrJjXXcc3kWEGsnOCivNIKKTx2Mp5ACLfhYI2nmca7na4oFqYACnO1KiN
X3qeftwFOBzz2xPqfHjaPQRtevBhYIAtBhxs0x8yYAUuy40O1Mex20eOkzSCePZ95oXi5v0uvwGm
WLJ+vrPWmAjHFU6sggeLwLLmV3VbYz3UpPrgGGzHhpp0u+n9qhZF1dTUhcECqk8AcSUI0WKNKFNV
YgIUChBrP5obURhFzqUZK+MPEaq4jf1b9zk1qJhMbrAKIJOzYgzLub7CTQlaftviKy/vFl1DtqYw
0dqPMzgE0xGVZwh/1UTnqTF2wKfqDiPSM2IPJcb5Kb+LcCunNpiER1E3KLUR06SPFmpPBh6vQk37
7j5lKlbqB/IYvVbFxDCbIGRi+qrQTP6DSxAHjGBkVqkoYxqCQg4Hd/Xbk80zma7MuKJwlbsgfuJk
ghs5EQkrar0bvNRzahGbLmbur3HqHIBvgBWJmNGFkdBaXrWyopDvikAZUBUjCq3CeTSWAog/48Q+
xsAkZUV/K4OXqUT63Gw/C6JHRLrkAg31LrAl5g+JzapyP+0thRvOaXoUdOCqV7SG16PtcoRv6AU9
pvZurM1VQFYgpOtb1abcbmx9jmepelSZUKZdN6CWP2q0s7n14/HDm1SZH//P7pZF/HXmnwoOf/ML
nsPnbx1MYM5kpjPOVowDKaMkrTI0h0AFD4OdsoAaDC7VD2nThZDKsaVd1lm5RJvtIQDCjFRq/07w
5HqOx9bo3Rt1p0SbQm6d49C1CIZcbIcGCTbBimMKmRAAwjplriLK1/N832zj8gVStZ/ajvviIoUv
hhSdYjn8URO9JA0Nupe+0K2lYByIk+byr4yybHzKmzmFLamZdf7IFsSbJ9aqf5nsBBL3TP2l3Hrn
vLlzuhohOWmkGsmnVrSbf4m53IvJsIjFMnxL2veZ38UfgNs8/E2Zm6bgifAiLmFp5f8HKK5cqJTT
bDB7+R4pix8xHwbd9OW+O0VZIjL8T95a7igN6Iwew61PVkr1HWakoSmPnQYeeBrAVnvCbr5msS9h
/ZScEHmV1NapTZLLfMiCQev76Jxwywhhxvc/cqsfARAkkL7MR0fZt5VmmE3xc5Za+zWtJwHYG0eO
NInFNvjNq1F7kd0Fe4UIOE1AE2eeNbo3vIwzZvOsAHBwjxAwnnquRPRF/eXa6NM15pp+wnQheTYX
I2+x/GqDdUAnZrxvL6lJn09wUlVjs5QG0ldWnTZYwgSSbUSY6fMSnm4Ps+AzIpSRrZmBy7FBBj9U
wuEPtCzpsgZ9W4yqoSDk3nMoEwm1xZ1UMqD7aycCw2LlBvulxWCqBODDlBnA0x7Vd97ACvdrdY0y
Q8jEwm2if8btyJbl0m2zhqtiEYMBpPJmoTO30GZPrllMdxDS3t43jlL2C90/xJA7k6UmgSo+i8wU
pjY4quIhUaiEbYDJBro/Ag/1mXHOuaEvr48JYBU1UImJ9Lr0WXBvsMz/8qYWETfHZQuejc63fU7l
7E5kjulBytbEPb3i0p/dbknByMdtVCbghQWous+MEy373DwfvMD5PUnOhucsiAIHaDhmnG7PYy+B
NM6BIZ1sgXNTwt6/mpotlDepvoUUmG4C23nYqgRbiE8sFkxoNzesd5EaFA9lRbUi3o1z5Jm8J6N6
i5lLmSeHeMOuSdLaZb9djo5MeA4tYgenpz2LLuNs5wRK1OYngnyg/CFSli499VJd/KtxfiOJAJI6
HE/bk+xTOc3rxPpndO6kC6mgMwiwhGHb2KQ2kk6Y0PrOHh+x11nzPBqLEqrkYqxlGA3P+SQRRsgr
+GtdCaccXJqEGp3JsthzhKN5ZDo=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
