-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Thu Sep  3 23:01:46 2020
-- Host        : T460p running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_19_sim_netlist.vhdl
-- Design      : c_counter_binary_19
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
DOilie4cW2gKF7m2jVw6j0BeQCiLEsbgVyEs0ymsO/LiY6GK/sPgOCzKKnLX17Z8TmBzU3nghant
+jxAoCbqVjYwQtnsFo2jWF2YQzHe0Zb7TzTEjDZ6aF/OCLkATfaI/dvjw4P7N1jothPzDL8RlMxB
11eG8maLhRI9k7pyNh6y84Unj5pFmlFyDtJo1iYXRgTpEPTlaUIyz03N+I6DdpgmRS70CE6NydYo
Cr3LVZ7gxDyUtWddt8SUT2kqSwlaUe4Lyhf5SqRt7BFPheItddngCqoN8N5fK0qT4THVHCcrk7La
i7bdjPt/yCvUOMSC0Fkya0Ttetwb+WAK6HKHkw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YVEb3+TlIJnAOi2VmggZdSasKJmmQ4runo2m1Gy2d8FAia+pp5J/T/CTghNvCD4aR1swJRIwyWiB
M8iNXPNBEFnaMZ+K05OeFN9qZ8+QJZphiLufg3Wv9TeXkOi0jFzTmHSO5fu9u1XC/3JpXLOqHvN4
6TSzrUlcnciCp1yZG4ua1P1y2PrXZUTpyKlKSZQedk3zio6PNN6uUekCzq9Tcml2n3/BklDjxqRv
BfBxcgLq9VoQrQVgDZbEIuUwrthx+b3kTzdTINC+r1kq0EB6ysuFEikojRJ2l/m7YehnvU4j5FMb
VrxF7pisydN8WrdXQsWkyIHLgUuHjMf/0hyDVA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13104)
`protect data_block
95fr1xzd1CqQ+I0IqKP8p8TQ+Jc0NlNpGpfi6WivRoV2FfniWBRcsm51b11zPxgxPMrwld9gPxp5
N1IyhliIPcV6r+1GhJhIOCOlJx50Ii7sOW7DrIwjX43f8xuJZemdipbApGYLZBvR1M3xz988hbBP
gJI3qHxWIwq1NYYd4Cr0VqEl5niD1juYPC87roeTd+pW3jSnbwOVHBM411EGndlTOmQ9GaW/v2bt
VO0SAl8rxY2ofjYFkq3k3SqMrpbZuZ/86cpPK1Wt2PWMyuP7ZsguUXoTeWl/eXTKwbdbB94wyTwH
aH4JOwNCWEKw8khf5vINPpSqOzxiV3g9ui+23/J8idJgZPNhPZuc91aQRrmDIXP6HTCzB7nC+/Z8
VgJfZdl2JaYUsnluI8ocIv3dAt1VduDtcS8COy9QnHzzIrIHwtYhgTpjQMWhxkw+j5IUqu989J5g
R6xOXS6oa2PJ51hzSKPsBe9m7aDB6NDud+Qzma7bzqNy12NIGDTZkmtGEtjxWJniIj4eA0PXfnX3
Z88Irdf8gV6fwTu8zGXDy+TMOGuxYPaKd2FuUrAVBxWr5Yeg2uVbfDeKKaGQS3DevcQiB2qlLUSG
W62XZLRrh/tWCoEj8lVQTY2S13fUaY9WDeNqDtxXR2VfVle3EKwnzpVsONNOJZV3PRZ9rueuk/Ec
JWHoV6aBtAR9CIqFYuj4kaqsIFrTL1E52erEc0BgC9DK/AaMga08UHSRy4+dVh0D/+Lg3oqK4jbh
70MXPeIjXEXrz9cOGNhUbVldsWp8VcDjeKQL8jC4Cq88KbBah+g7QjZDMwAvMCkpLDU17uXceCOV
yQRUXOzeImQoIJBfyd3ULhgK0i1l9/28/FVc5JwgZtHWDQ6U4lxUGAoOTaUhHcqDe7A+jUn4wbjv
G4EUoEAYeTivsuSeznLbphd2KwArLUY1VKxpZykjDwWPGuZHGbSFK2H9AqcAnrZIXHXCcUd2UT9m
UhCRTnqtQq2My//tIVFYGFtzUinnbJaCrZsZi6TuU6BhO4D1qjZQPW78TV27FosgAduQhaomT56B
hd9shlnV6+dV4Ek0X6fGFOjRnlbw4AJe/sOpyvNAD5xP/xieXoMhp3+fjbkbr/HfzoPXJciA8Mn8
2cYxoCT4+KgobYH8XUWNPbI0nrfEx+siUvdUJcuJq3rq/rhr8/Ioiaw9t+9sSB87e7h9rJNEeXuc
3YQZMHvekUtJu7xOTMvESi6GWYoQfRJVaHeWAv2il/tZsXCbR/Tm5v5z6SeHF7mzLjwCYJ/RnpO7
1glGU810/PynqWlCZxCFsBTJ+6sMsOhlbSs3tC2ZhT2VHAJ556HYGZe/zcN5t9lzhkWREQo7euog
entMACryWIMNHsYGHFuKuOy5IpWsABrLso6AGiJmf9L0fWIcHBEpab+OAI379aOx9Vs5/Y8C/6JW
ZfakDNu94felZJ4MfyfP6KN4Ijmyo7YxECIIoBai+K6BJKInG8u7VkKV52UbX1t2TbzGU7QWICcs
3gjDwRMxrVrew6EOSyRAoYNzYFAM7ce95xekLzz/nQGQsOZtpsxwo+YFDRiRiWzYBoRG0IKj+qGX
3iCcVPBZvDgi0fZ5H/J/KyUsKS8qrt+B1/UjPRuiX9byOOtXiWOAVON8QLTNB1fkpjZlOc6u07jo
4QMANNsjREWtn5qgNczv32Ygj5hG59p4y+V+khWMlPFGm7Yhgsni/lcqXo2MBpL+Gq8JXg3QE9b9
cG0QP9cCuKO7NBSXHjvnxFTKLVExRCkNQJnAJNLAiR4l6+Tg/yx1JZ2MD+OvEMJYOtmRpwsPeusc
hgYEaQTwRT29VP5SdSO23DQQ6UZUx0gHU9KlwyhUGP5Bhxpbd/JQozn1HQyKdxnhkImaq7DtZR7F
ByR2adeGyxVzmtXIA4E9E6E/u8t9Yosqcy7qrldTLeRnoZqKRYmsF55n93ayqNEuj3jCWI/C5H9m
Ot6yg821D1jZ4iuq6L21G8PnUpoE6NwMMaqOub0P9j1rP9kgBKf/h6+TCn7WAuwje4v0LbVM9vdz
XbuMGeTwGLfD0ry2oexK2zYV1ZIDgWzklI5oYtG/mGOcVnE0+obZSEL8D53cadFZCM7RgjotcUG6
Ql7/5raCRBOwI1yl5PzVMJ3TQig4FzW0r/2lfqlzcjZ+a42CeZ1FdOF7GI2iOMSRNeqDvPOjW1V3
k3sMDU7t8atLWtUZ1TEZbYEWMAEhoIkHN69Cf5gfTAOuT47lcwrIxPt00kRozLqhg+M4vYXNI+6o
y3IqhoTP1fGBInOXsLTYgqe2k5Ql5qn/hunsybKikDCNew2rLj8SzYAJAh+TSGAj5vWNJLrLtTUF
kF8PcwdA2kIrMmXxt6IXTu+7vIFmzwMQDccWPqydno82JQAPLpiJnO3t/HkudQkkr8V8/c615mrL
hcivC7PDkZmogdFCjKbCrGAnvGokFpULvC0YzFA2wgt8sRz74VTuLdGfjc/oyFD9wM9hXoqIdyMS
mCzxywHzqcNtfoV68DDquY8cHz/FM73CBfMFpBNLXElh7MUPSrxDZxaMQsfegEj/qIbyHQmFE8W3
A2gnviSrLX33vWppLPwF71yOlqZOhrEBqQ/AYV8aM8NC5A91BVQTqvFGTOlTZa8qbTOPvwNr8+Xg
8MvS5xqylwIiHx01VySg1hy+/jj3gLAU9z8+qqpShDIu6ok95lT2NCgZR+x1Lr6XzH+XtQt6fknW
GMlrkPUNJaUy5VTwyNtGFbGUOF4U42vyGE1Akbe7sB5/xFvrln+zXJZmGLuNFU6qZVgk1SA63Pvy
PHcZyP2gLWEuC+MbIOQuSOCRrdcLZNiOF3x2TFLRvMzrhiP8mJGsb9c4Hk4ngvt7GtppB5+wcu7w
k190Q/v1NYmJGO3vSjyg2I6amsXHJSoALYbS6oq5LPxXtYMpMXYh7sJYA2UzpDQIlHCHHesdy+HE
2HQVP75Tx9ZEeurV2kZ6ma1JW0+ISl2s37iSu5GdWGPZZ7X34U09V+MHv7lB8MTxK9Ur6UKYAqPc
u8hAOvq9R5MUiwJROiqU/Fii2gGn5nv/UKfIBp+1yuP6x9zZ2aZDyI3pKtLK1fSvABV322Z8EDf0
vKk49fmJdazwkgz+5Ks9pa9y2ioTR/v3G17MPOb010hxQjz49fzpcC6xSIrLZacJwR8Mz9/pwC2i
l4a9usWF2oooYQGdz/xFhRV+fvK7QPZm4JYS6AERaX5VDkwApPgxsScPftAlUYtr1T2ng/rxE23T
c8xi2jQ49kvG+JVN9uOY2kQJ6WVgIyxN+YLQvgicwhVq7RyrH+2pNc181EpP5CXskf3EKZ3+KG3F
IwdgeL9D11Jo2mLh+eb2E/7F395KzDPt8EIC9016HCeWNPbkmqCR7K+1aVvqp3madRIM6T2rkG3+
phs+leoYLUdwWwhSPrKZODT9UWiMDC7pSe+47QetwqJt+U6VG/bq8c5KfmbWJMIHjnrnJiHBHvpc
8WNHHPReDqKVpARybBclzjf1Kz9MVCMzEA3V7jei0y7Lz1J6QW6D6lwtfj/ER+S4xRjYnDyA0yMb
EvZ0KE3E2GUZqYhkffs2/t79/nXG7o4HAlrzibfuoFU7htj6xKfu3ogxwzkh7Td+I+0gfOAXw2j8
zYVga4ocJ8qIDMhFt3VD1GH9fzL5WaDw5e7a+F/n9149W5y54Usl9cr59Tx1HEdYpxKdxmtsW83x
lE37Mn3wxYOH5IMnJRq0vCZCme9arbwqcTrbiDgYPyMwX9nGZAAkT5K0QWXc5eJGXMNkbueNKgAD
rASBbYnUcmhgmfZF1afhU/+hW5KncbDPXpSWC2T7sqVjYbZZRVFSo3do5EJWiMjJIez67oFi+2/M
Q/OyE5sIVdE4oYnniBdqKgiwZtUhw0CBYKPp7myUYFiR9SJ0uWvzfGtbeedF7kUeev8tIjrM4Du1
ob/svQTwpTw7582F52QVqz9y4t5642UMFmbBLs4fNtzT9nimhaySH+ZiJaothZRbecCiGc708Enr
Uc32LyuNjCYEznASYklm+DYQVyIF9Sjgq/IUgkVNOb2OUR9UYs8cjzH9x94qjk586kgHsYivGDd7
Xd8I0wwWzTdftvWcO72biacyk3vurv+ASXHdADDCKs45OIOGCmfRmYyvxsPZJ/88AqmPSz415F7r
9gHK4x6uj+eROGJJPdltQq39nSOfNzrdZQ1mlw84cKmRRch8NWStP51NuHGeohcXOVnMq19n0Agi
9Mpg1Ak+Qu88AEQ3X2ig9hmNpMor4GwEfYdPllMb111LsDcBrDVFRHx5PYphukzgeluv+A45bdse
uwbdDvquWuXHbzIWdzCDYowZMmA13IZbL0jO3xLJ8+MrqZULl+p3hSgAIoFeFadgdy8AkjjkIjKi
2C6yOhOtyIr8LHMLl61avFkGADcVdXsH3d+c1MqcooJcx/9rvQeW2tATdLZySEOQkX6frmV+BYBP
pjCHl3iDqsyqbT2mXG3xWGz6Z6N7Id/ONzyOi0ZocfLyWbXejimFT7Opb0ZJTkw+hSt0Yo/qGVV9
jgxGqucGrLGSsNCGb0nmdislVF2PwFrEsQEOnPYZpJV69K6rJPbJsasvdZ3bJ1iAPcvhFehgPrnQ
ep4rGJ//XAIHde+7+oF/hBC8gvWJYSbXnCiOxzn4o0zeav27j93AkJ45ErkY5XpCch+nNvkWC/G4
M2bUByRxaVzdbdANHlWj7LOMVW4WcoGLsbPZB3tHxnrQpaswlPXxcVb7nhWHdrLd4MNOlNRcLBaQ
8wthpUaIC5ZSMvzOGLtpxHrpSIjt/j5oIRBKW6jvW3fSnAEmulc2riA6kSRV6bMvtUM2wdu/FR/C
nnFl7NCvE8atXK77kIBkzlwJ5ho/IxWRbpkhqvlIETUvLLGJvS0uCUS/9CCUSISpg3A59PcAYSEp
BCOtvPsDdLynzHtr5f98lXhQRLzxgZToQzrLGQeTYIGtzzNr3U3Sqpez+kI+R3LgRkEOEvc0gpgs
dZL2XmiCHWLkJ+Z/z9DH+KFtlJ579LO/27qVY0XLSnswBLGrQtu94zuMrMaftSZTrVTeeFQDHKnj
FQ6Do/79QUWJWrCWqgAcgZrWxTyoaaLkt7GzEeG6yHfKLWGADkWaVIvRpYvc/g4cHjEIobVVlbrm
krugeqDiSTthJ8kP5zSXxY+QKMbNQqgkzy6/9YGXhzqz1X65E8y75xQcFp9sZesEIm8TruRjIIYj
JiddPSDtmC4Il0khpY/w+mUdt3ccRLVKgf+lvm+PCQYrsVjd6uqEj729u/ACSK4C4ECc2USLX2Kn
W7wKasCteUzCicwIwCW/VSCj6Z8hsbyIvP2H21PV9oV9Vhq6Ix3o2DwaqMEKIat4zHhinXvIiUot
88O8IYcmxkvHt3N8HOrXGIWd6RwUE0ttidhgkH0tC0wjH2KRMN0ArBlUDEpSXSn0p4Frn4O9uIsO
Ml7UTwRUru38DEVNK1s9b1TWrQMdt3F/wWA3GWG9HydxTv9YnaC8RBr415mh3mSdPpxtxre1qN8Q
HrkiOvvGQJGJEc+0QKpJ6TnkDIXH9JVbxAuaU3cFfh1J92rlQEu1WRNQrNzgRSeMLQWz+jg1nXiV
AQqtAjzz17gZkFO2k5yV4WK8/U0y4VT/wylTBXMY10HvuKhByMix4OusfytLA6QUnMmISkidMuiQ
TDDszgPuWJXNIgYTQ4U+qoFkGIkQlFBFifu5XQNH74u2/mye/b3P7zOfQg5snsQ1Z4AHMIpIcpKs
9U2gpY2YsOTV+vToD5Q4woHQWNvjY4Tranmc9aqeaF3me0TcUhbGwb28Yn08lfoPj/Hau5MvlTpx
IaCHqPSHgMrN4tGTQoFOUrRQm8d7o/DfSP1M+jKJGBVAY8ao2egv05BvfO4hp2eosBXc7hTClz3S
7ZvORy0GY04M7fhwnRdE1cHqXcuXB/ShcUr4iz6aH6izF8sdc9//N5q0rg7rXa+ZjH5VqcX9fi+F
qJaYnzXgBs3Ax4uQ6ZH0Ah6HtSJTGRfXG6sA/MqDrffOaNGEvbekcfv7z1KgbOizK/VGtMp5PqHu
PERoZww1p9zpDXNswa/PJgGpDBNGnIjRZUs+iQq++aRHIonJJb9gzluUsITdtdM/QcboVHq3J57/
EUOP+tGwC8LQn9IpQgb7FeBem8sgbL77k02e3pawyYrvP3F6hiNEpZCgxx/bvpj7oWDWQGs8Ag7Q
cTuEcgmVsmkDozmXNIo/bPqEDaZ6kwwR6M9hS5hLfgkd2Z75Z7o3STHWpQFDT0w+hwz98Oi41LpB
+iJwUKZlbM/MQfo8lUkPP/oIdOSWi11C5QJ/xeUReyjOWx2sXMoQwOv9X0hDZgPgyF+dghl7ixJU
QDByIXqRzNMasJ5xcz1gf1cukFDG6l3U3B+4Xw2DaaLO7t75aLh4YKvRfLskuIKX0/EFkC//d205
/hSo+Q/1I3t614P53exNrYEqNkbFzPOkix9zduiw49oN+H7FuwPVMFi+7wTaOTHDDDGIGOsI7WBT
Lr7EefgWq9VmpTG/pK+81gifuw4JOv6qMH3AnUmyF7+hYFj+MrS4ahTldh9622puIYkXoGAiWi+A
I2dwiikTXObcetDKG1U4S4jIGxx3tK3tUVRtIwqUhP2dox03lcwhPdZWXn0FeW2XK4RREwptTFug
MSF+ytP8V0SDWHHOVwI8GOISXHb0tshhtfmVuuibx4JZJgx4QmPcv3oukqQVC1GsxgjivTyWC6lH
5p9UQWpR6HWJASLCxv9ErDahgc1HzssmO9LGKOKJxwXh46evJpFJRhN7xzqllakjHRYzx12/tym3
YfknsrakzKMaBFntSQWIgEd0UiuMyYXzUFnG6WvhbgqkrSelWXkwqRir2RTJgTuA9BkHqhX5LXtN
o62Y59A4vkNgHxOxJTXkth1bYPE4lLbJhwbfdfaPTWY0u2O+VdtzC+NYnHUWL3ejDDT2cARdvtts
W5d3xf+oUTGS+VXjzVNMBXa3eMVnTMzCCn/Dowm9eYfvSldCRbC+mg3KHYp1gWoCp64eGEqLGdgt
hRzrxwVFUT4p7tF1ABfcnLY/UrLhG8yoAGXUkwABNnmPJ3nTbRZuQCbww8eUdT/Dz8fVth7GrUph
pF0HDpA+R009dLraw7sRVBtOpbT5evAoWMzmxshAEp9qSdBV2W0s2xzXBtrPX87IXXFEFpG2JtnV
FWxJr1HXDqAe7eF1vYguvH9MOCXADMRz3nhgLLeZ4pH5CtH9kFYSdvPLsFdQKkpxz3p/I2S6umo7
Tke9SdCI9HmLc/PHD90rfhJC/fr6rVHy/yklEN+677aCMZtihCzFGITohJ0rcNrly4S4NeKN1phK
yQ26Eg7O6lgRLUu6gPpfPyVUYCXaTOhyXzemXrudpn5mTfQKfxEvsrjozGGyw/SBhdOwgTu/ne67
Fol/wzY0CqoAQaDdSOKTLmuvhPCqeFGltTT05Pke1k8Xpz9g17hmnaW+OXbcnb3ZiGxbMaXkDcUm
P3cYWhM/Yf5majiohrscU7HAsp9XG/kzduRZ2dlB+RIWmX8GCE9Xwh5TT1gLwiDZg0oh0qTzFZ1+
/aHRuu4bUd0f9U6fVkmlDa+15j4Sfa1Pjd8Y59v3S0tW1AskYb3EsTYt0NA8tP+kUUsLcYLqLYab
PCSVIJzqqdYFoI84Nnxh6+Txq+3rRa8LW3N0cvZJ+qRW/GAkjDrJ5nXmQaV4/xXsr6i+6vMndW7w
oY6bkz40dnOZsmWtUIMMaoZnrBP1uln+OeNp9wlLnoiTLrZP1IfoJ48L7+Wi6AUl7BJeAo/H+cJ/
NP92A50C1tEsslWQ8QSLU0tBfiG/u7hzG76AUB5fy/1mtWbmnZUaX3CPf/HeL7jrx+nvzNt6o3LT
xMAZ14ESFlqKTeC1sqxxhyPriGpTybGf1o22gDqdlRLASQcN22sT3guLichW9Hkj32/rFlltoS9j
CgxmMIn6cDUDZdCTLC2Z9pNGg/7ZGqcXbG87rkwmJQIm5NMKJaPnAk/GFWlKQmSJmgM0n0/1aTjw
928+PhxunK4NfqW14R2QVCeAJxerLbfHA2LoCN3PZtN3WFlPOHxrRb9R4gGX1/7++zK5WVbDJhOr
qnkEXa7XA0tadD72yW1x4JpRDcdzTnQBP3WW3QE22oOmbjcxfMBAjJvGrmKRpWC8Y/mqa92KPLHf
tnZDwV5kvqFVx2QVRZgYtfCwpt1JdYCrenD7cCpYxhF8ubQqCGU2Lfh2ND++EuZrqiNpRYr2HGht
ohKwcY41vSwqsptKEhKKWVr4THx9Nhs0suYFRwY7UzkX86Wkycdjc4204LOLDENAejnJqlQYe+BO
/0TmHL8Aqm9eJfOYydAr4wWIVD207ur2CRLkulP1VQNX1uHVfG7Ig344K8PBzmQBmejsts3SgsgC
HSaQL0+fjJrWBwylBHxieKtqlIfVzmV7TQ7Ak2D+0f4RYYjs3qo4TnHN8e8Mxea4QiLumGPyb+3W
igczAtYAJJi29hqdA4z+FrF2eB7EAKht6CwiwCkr73fmDAXzuUhTymCn/sjBV2SkDw/OqAxh4YhX
VJDM6BTv7ZGRgnS2GFHAZN05uy1K1cjF30J2eGWA/12mUSpBTrxc0vT9sKvN+3vcs2FtWl44XuFS
S+3cmSneH17ZuuR8Ohj0FrIHQVvrSafXyctdxJuMg8dbz9wKYBjtd+3JFq8BaxBF7XB/Oq6FQ+V9
7EFC33cpk5o4KPj9uqe6f6NQCVGKSm0WfIfrG5eEMg5qfZ9Ze8sNoog0aGLPbMDM6xT9bbDbcLhL
6vULzEs/+RJkA1NZT07LkVfJzIEiBCFH+xLIlOp1W9W+3pIZeeB3edgf1AmLzGq7VFiYeIiSnf6N
U+OIALx1ax0kqBUR4Q2+LqqjpdETrM5J6HijbHWPabDOiPKBULKKjoZO2BwU5zdPeRc5Q2OQiz5K
/phuMXnXlWJmJfmY2yV/cOMf6rs8PVbTlzFPWoMKHdCS3PsDHwU36ELJylBi6S/J89LHs9NBrz7I
XH3rpfbT+bTm6+sNWmKS+AQRGrJ5j7tIqPiFCwgibF1zx+wP5nnriAJDBHOFoRo3+lF7l7rBhHa8
VTClWsM2NM4g3zLWhGgiWFMRSJSSCepRndeixKMxF+zK58fl4gY9vtco/ByRn5aBr611fqhDD+Hg
n4hDIy5l6+FUtcJqV/VZ7/Y9jynqmuMYon/gVKXglcH6I6GJwhSi1lVFcWsxofeIjX79TLqyHOPt
YW/L43ZuV5/CtcZI3Ue5mst6a1zC7vDZQ5aboahceP/CsQDkALgHITxzl9FfEPXnuhs64JHr6jiz
y9KN/7+9kCspG2H2ZGraTH1ac+DzSK3/eA66qn+rBq1oylokFKHlWJCM6A8nDuTLrPvK7soXfAas
UgATq2vy6VAVvxPSywjFrx8Jm0qiA2USAZf5RoNTleN9GitQXc6AyRjvro+m/nLg0kmiqRiYs4Sw
kDKskgPf/iZtVitf+qNALRLAwKC+rI9l92asWvteirBlpyN8VfuyhIawKaJsnVjdfCxIXCoVKQa3
iN69xIz2dBJhwv0wGcvhNann0EvtTWmVTqm9IWg8q6B4AHrwoWpCIUC9UDt+K/SRsQyB6Bw0fYSq
DC6Bo4zSTitBzcQV3jQZe+KsUWKQ1IJDh7TIvIqeQAxtUL8bK0mCa5VoMWchMppao6jjELm6k+i3
O576+FmG6Zkl7/L/oKGvPseDt5ACSLv2dVW2Xj8AmmK8aSev6KHwyHhGPfessoSS3PnMJWoA5OaO
FpCsoBOrMB1NwkxRBixgGzw1U9eH51XP0quVfvsAWDzjGZF7CzkuR3GPIKMLZW0IVya8dMXOILQ+
enN8sJZziy2kWX9S4/1uwWEiCnWdADtGKNpA4K2axXg0qwBe1Yg0JMMVT5G8jof5jPTNBaU3IAz9
Pem8rmvN+GFRY7dQeRMP+8W475l9cspgTRoOHa5QjGriynPM3BfTpgB2MAoTFOJKUlkh1b3H9ph7
CkFBz2VcNJbwh/3naJNTck33TEzxyFf9Aed0Z8J4GMx5dzuL6KuqQjdFsHpKsZwOsLpWExUi8TKh
2c4wiKnp2CNX3kw3QAsMjJZNxapNakQtKOwi0Aiw+l+grSkjoYv4AEWUPxkgWYdtzCwgmCrLAQVE
UGSDOhGauG/MaSsp49t6K3mo9s2oop/EcCnoXJV+KEyg8uoIpKisk/38DZ6mmOHNkdrcYwCYLjT9
tCDRgNBsNfWxT8kQpGRywoC3MKADy7E+fLacrJUZ2TvneEyCFFvPSROmuJy8DcdewvaaJ1k16gj5
IFWRChw6yNoXFrm20uTwQ7wBAdjshWuUHyizPmXXYNUP8Phnpk6fxQMbzKdEEkog8WpgsvMp5z6P
lUSWNjCiLVx1HrSR6rEZ3SOyPCPWOjnpgI1OyzucF91lKYfd5bGTPe9hNPEBjSPpTWo0IFcUmilc
ms93s/3uxRPPmgcvZQO6r+Njx2OTZu3NilZ9ca38ahKOR3lLiSCCZX9/7yyl1Bij1IEhRXEi0ZRj
FrfA+cgg+pOV54iETMN7UK7rN/jZEOY5e8la2nTdns2PicPu3M3IyDgvsEZra7AMELSdiwkcbZhE
7njejZFFNuwlqwZM6CJOsbmxy1SXz6DvxmOv6WO7ASWwefO/k1q0JTPM4cipAyg3hsu54lZy3QpI
zl16TxXncB1XoMFruLJs8ueXh2OFV3dqaflqjfejCwMc+/ku6IGYRmla0Fq2E3x+TarxPbUq90L8
mOpckYplTr7BzKJ9QVodRJWwXH+ZS6DG4vKkVZuoDZrNFQhKiSRAtSIaxFTLCWwAPZgbd0emBwdY
2tmz4QcSjoH5VUluf11ya2K+70KqMcO7qnIsgVRguLbkgFw+FBrEOTiqSwYtOix1x7dA09t2q/RR
bB8f6hx7Xdg3bFpKFpH6a1RykSUQWuEp+TScBBzdyORaq52zzYE6JNzkl3S5z+r0bH4DqbyhtIi+
V8RwDTZOr9fbjhW1VRD4DTP6Z1TkLEGhJtLLQnqMVV5e79XYPvnXhXhh6lULf7icXuf0gsGK2Dfv
20ofXKFge63YnqCSj9kgZ3OHmpnMLojI3/ai/flmAPCbPMcG/V2Ww5KkikXqVMPP8q8HIWgDn1Z/
8j2c46aaIs+i+RrNorS2uX+UybjPUULjs5jhXj9Pqbdtjepb8rqXAhyetwxKzR1n1m+COMoY76Jq
VGbq0jvoIRcZebrXCjIAUpq+YgyDCyDubjnhaX34tLrmiVfff5gUab3mfU/IcKBxBI39vM1u6mq6
NeLS8OJzPt38w4ANO7fHeE+f/dmiuwnvrA/ghnOItIQy594baYMiS4dUj7CnFD6D98MyO9pt6yZn
Cm5cF2KYA5GTaQvb5AfQ9od1W23q3VhX7jh2nnlTTo6Sq/tPBv2Jt84mrVR1viWWwdsc8Qxi9XwU
fUtqH6Tu2c/LQaNgaJoDjnD+KyBSLGDjmqLcM+L+cvbWcpO2D00MyUkRZgy2dgbm3Nq2epHFd1Wi
f3SPvNUY9wc/Wp7lxwprPV0pgY3HwU21Gl8OkyEHwGoqd3RlypjGUwt7YIS7oVXKjMKGUgH/aIiK
GQVuC/2ki8tOUVfeCdU+53U9UkVAs6oS3VzXDnwczqD0tCb5MZjeRSzDFp5fvIwqTudAyQtTwOCE
Wc7loToRGamf+gx4oBXGvI3k5Y1Q74XGZF+AfR6sRyjzbK9GQvvIANyI5S6i4Zp77cTeiGycpS9C
wNE4F2Jk92aRN7LPLUkwQ0nKTYk/fIP5bX/RLVOTcOtP9XJH6UXxjhkmOtHvCGKg2LNQXUowQnIK
+MqN6e14V+8bxJuWLUsV2OPHTo6IPGcIzTgBZvPT09g/FUoDy1EWIbc0aAWDf6/s7zBoGPe5zo0s
JCWdrSYEUiwRrwnuSM9xqqZyzlpaC68M/oa8CxdD6eGrxTAVAK8AUROOFFlKwoqcTXQxAEev6inN
9UTDugHByTmpJT9Fg2zVm/ldigGdqeux9WCGyUwtxqFWIMAfmqtWqapVwwRiy9MpHhX0yJx0NPlb
TjoQ7MnuhheZ+UgY0x8+y9cxLm8wUO02vIKvnWhDalExZLJ2p+JvQ+r8c010xFo8/1fpb3wYjOur
9WzAwuwmH8ESrYIpBywcgFwC4KEkrzw6X3BYDWfEvMRx4nK40Pqljqc8lEtgeBoiOK6w+skqsTRO
A2uUNeEaAwk36cDawQMkRrJbLoe/vz4XhoDXhNKdRbUjUFjySQbxads68om9V20tUK4CECx03wJM
9sJe1zx0t75e6iSZVZpHBtLi97nq0FgCbQ1BoPWc/ls0Sy+ys7UNOLudSsH33wbMIuOV6wUDXjfw
gBwvla7qX00MQ1ueUVAXtKJDTaEWgp3JSQrqaOf2KOoFFTg/74HmVbRXLKHuGGSlGraAyQyeAz76
C7wW13XGRFcIU7eZ2fW3aTF4vfKxZH619F2L6VeH4UFd3rUzfMFL6vEhMWvE60Zwu/ZJCipHDLuF
imC7qE15OFnjr+VLVsJJkWHBLjYgSTYQu8ly/JQcRPiXjd8sVWP3iNOLoYIFhO9/AHGjqghPT+pj
wq9HL79qO8GYNsX02DoMt3yiSjtAXv40kXZB4XumyyYB4UIJfhDu+qUkWf0pj8NW7TI+79T9QEKj
ZKHzkF/HH2ydT2z01x5XoNthv2mteQRvYjPjOX9IZsQ775dYkiTv9g+sooIefvbUD88MmA4m19Ea
9D8IvrrMqoxQV4icsgAUlHRdVSOMjBUPqAdi2QPJDwZA0cGGgDNTxtmXc6SW5aPSxc+dYqGdH/d1
cgn0jiDnBLGK7hCClyvJKjY11oPfJRBhJGJBYMYNu8bWQ9hCrjVhTCm3/1H1rsllqRMVLfEDpHmW
qIoRxXCxNYxvJO1+g68WP08Ux4JfESJH49SgWmeHrQmGwhOmKx7HJ9btZTbKi0o0zktgUOSfsTQj
WOdMvN752PNezmmfWt6qPIZGWZ0Vaw3JUZSBoEZjUOS3+kqmXiIXQPqfFs8CqK/RKqPrYe8m8prg
gq8PEA9HZKlgHJLk1eBB1f6AfX6AVzbo0aMmo/FNFhZBU51HHGr+llwI2Lw7vzWSoAFs5TMVu1aZ
MZEL0/lQq5pdlPdzfPL4pT05yQJsgBJjM3ZixiVZzh94c+6YOqdSPGwhp7DxWVRHrArCbBVvbke9
hLinL/dROrbpLwBOPmxaDfSV9RJBWYol6GherMOU2MVK9CjbjGSeZ3Av93Zb0AKIcF1CLuqVeM2C
UJUl2LMFamWaXPT3w5H4c4UX/7ENVj8EXg2wz+m3/3axR2trtC5dMnNyYjrUWgjZL3+25i0+Et8V
Sz/ZIMT9xoPMKrlCkqqL+Gen1YD2sPMck3hl5/K3cN1AglJdW4mZWs9uVdNtd0X347RsGrz00G2c
C2oP9rd+e6v2YClrdJwP1oAUazmVEgid9FhspdrDOviMhin0R3B+6zGTzQDcW6sxHXjXqkFpa/Lk
TEvn4fcDleKc1hHzqL1hMK6tn52n8yslbJty9MY86+Zs/7GDlSS3oZeVuwlcQutcV7MChQnAy019
b19DeTKkj6r39EURvPNtPLsAgUEl5wCuUjgblZFF0r9mep0iwtp58ncZmivmX7x1P3NS00eYDZGq
a40YokUbVQyYsHeSDs5yTm72IfdEBKvEDVfLdZcVWNzKqYHEUoIPpHJQBcupGihOUTjOqsoaSQel
+fevFMS8cq2sViZMDZJpYpa1e8sP7jfgyB4uip2PnpoqS9fY9DfOfhdr78hq76wMnRU8dt9SqNuk
Wk+A6Mf+AStyutHlOcktZoHmvtHgxHundNQoh5mHdvuOdd6841ob/8TMC9HcWGN09UY18HFv51yk
3ZNCJlc52USOUrz4EpwMjXRozkd20fqooD3G7h1Wz6/ML+vSwJHYtF8p6bOa5YHGB5r2jVdRDMKg
5kAKNwaPl/HjV87qPNb6TzQ1rxubVMX6xjdDfNRC9UQGjCq+ULAGaXv3nrMWQdhh/aiSoD78ISJ+
YhNQ/2nS0Wl5MjzcXzyJO7JbDujVLqStmeXXJw7GMrPik/cYf6IiUzfX7DSE/XfVqwEjB9i1svCs
bkSUqC5xS79/jXxn8VHEiAAUuRXNnSxnAOzxQhrFN1KvHqsfl4g9rzDyK2LeO7wgyOXjTPyGvc2P
Bu475rWf6fT8GaEjXq+4e7EEURGDLCcOf2o56tq3TuTIkKIfWZ8sXH3vstXREqsrAzSTQw6Cp/d3
Oz0btFbmDBB3/pP3MYkwcBiZd4YwvxAslGM7xbC2MkKZGugHunel47lbK28xQ8IkaYUALoSObzyX
UY8Kv2QhLqPnJcRJq6L6onYTzcJv07dsvyBwsbptY0ewPIJCoErFNix22q3JoE2aZzKCcCjGkFmg
pfsD95bmFmJZxLF0pK0mRiobT7+1GODh/Y4U3ZcPHoc/6yPr54+f9qdaZqRdc8y68jnkH0WkImx3
TBh0h8b3/tl22BoBjm0jf9N7kGNJfPqBsEMu70Ru0x3kbGTMGNtxOFOxAIKSExMAPTZUpn60tyFu
G6+u/q1zjplz/hsk5OJOGBGu6LQswFtbTpinpNNpggV4KPPGG3sX5V9nJZFYoAGK1hutcCXuY3mK
LVx2a3d5XalG+Xozlou9iXlUw/ArKoTrkABqZeUwsXDsEidCPcAdoRzBM9TmKARmiIT6kuGeOICD
HCNT0dHG9LKJRM85JqaZnV3gRn8y804FXHO/SCNkXO8x8nYyRp26j14KHIWEzHR/ZXGl3yQKy5QR
p2Zhhdq68OlcY6PySoSdxLpbxNUw+5JGFBdjEPJj1v9mqaQPP1Lsh4KEdHfxwKdEjvbnEPD0Qrkt
vEIIM3aE1hULa0CeCEb1oWODlwpORekzgdcB5ZJKsfmE7JIPXVUru341zQw0qvxytnypOkk07WyU
DOkA22B4aNKuTlwCx9ehMSqHITfKHu1g0Uu4hktCwLv5BIeUFILAgYpJ1ht5iX8ct/DT/wRoCWLZ
dRrmAUhLmzM1MGm2t4fFmyHndS8dqOZZAv/KCSv6z1h1lWEqrRYayClZlxRea/R4nWAyjVGp97ih
nujS4OfSPLhNzS8fupUz0ouT2IMTlB9azMujnyjUyRaCXiemMdYQwCBlS8MpZ5xikNah045kzSJ+
GUmAPfnJatVlGzGdsUOurkdgGrWjurtUgvR5aAED3wAwDd/fYB53J5ObF7zYXArTIPf4m3+uxxEO
5VaaEx4wQ6IOR+sZkTKyB3rCh0+PzgSd1CROKajlF4m4jcoiHR5Mjo3u1yQFURJR+MQUU+s/ggiR
oDaoZ3uLT4FVfbQdJhID766T3CePZHo3uvF9wSFM5SEFrNDgEoT9BOGkYbma3/0pLu/aTBKnGTa9
LDnZmhX9K/QMYKVf93tWBYAyf+/DPLG+Od/T8vmcz7YgfsjjipciE7QdPBAW/bVXxUywYwPN1HSk
6raaRMMJR5LF6y+RaEhAYu25VAJ10wBBmtqwYgPAw4YHPtfkMbdSmJyD+PAcYslqnBe+V2hhcPdm
rkwA6ksau6iJZW2na15fuZYRVopwkBjOOWAY775mwHsGFFAGYQ+/ehP1dDnJ027PSYqbaSOg3KG6
+WkozLFsS0So2oHeTn6jV9KjROi9CNLHjrDrbKQXWkO0VWDtIlRKk9lGd76O31gJCOzqi98YNIn4
THp1Zaro/YpOy2I0PrrabZ5s3qvadS4ShmJbX54hXPv66o8tqzV9ksskHgH5fJPB0swwwasRayxU
wfhZWZQTG33C+7stMPfonZIPr+FPlLPkrjRJWYKVQ71Ze7Q+CvEugS11BAF7HYdthPIHkzYRds0D
SpGo5rrvF3AcmmWK+ywUfZi6hXrGVAyreeAKyoVVS1uB7WcJXe12okuq8Dw7ZSbonhJEZ8zK4ONV
Rpgyse7JyaBAikwwGQzC16/sbQ/Yi01c4vvv9NLrr1lQXXHq0YKmD5WXySPX+wUVk0zBGzSjsN6h
eNhvMGKqcPL1zMKVoINVB2BCN0Fxu4XYP4fbQshVH5aVzRw6h5CrFCvRtSJotTXbQAQZwu2SHHRv
in4rVI0WRasG7HUvEqmaLaNt85WKJZ6pdWCG/35TPUN6aflHKGJvlK4CMxnPICgHwH+5hh6yJvwh
VDccEHDhGby6CcMQ/B6Z20hWdfJFCSgaN1gvlF8Uspp4PkxGZmYdVo/ct0YJiKLJHlVbjx3OnxGI
MLUpQybKcni4qTSXqwRPft5utgasF3VBU+heAHWg3v/F0fGo7REPgN+N2k2ZEvSCfSbnDqx9rpRw
K//ChMGSXYCQwSGRdGcEbH1mNG0IHnAK5YyYrymoZ8HsN9KDSIW2+oG6Rt17fcXm1lNMixlU8MaT
3rpZBF2faucyvHZ4VjacUsix+wAuh4A81vmNQu6Lr7jiQgDirTCVwNFKk+uJFTOybdZkySHeAMS4
jHCXkH0Sb3ZiStP7ZZx1Dkklu5+gXYgwp/OVijK8KIQTT5TIRApoQ2uvFmWKU7sqGY0AXIyar8J6
ADwYQpahS7+B8+urLNNlAK1UJ9gEBG1TxfqT4chvbx4awwADh9wVbnZ//ciddOoqoxBfVcvcp/vN
xrqBjWYLFUfkG2J1XYK4D9nEsb4UqHvVhj/qt8GnWgZnk9W1ZDmlA2pO/4HbYcLF5OrVgUpjzdG6
IygguN9HJw3mx3/d/hbZCr8HdhwmQ6nfi0JD4morpCpxhqPzsGF/2MIFNnjeo4mvWgoO5ByeKyUY
d2DRptOu5FgPGq7SGFhEcyCdn1LYBshdMsw+Vmc4gr4PDb1CCOqAU3R2rpeTtFggCg78ybiPFPtw
gJ6sTF0qZHEXPprK+x3ihCqEZiF+CnWjmEdUqLY6Wu2ID2rRLaDw3Imp5B4vRVvihof8wqYfK/tu
C2hmZ+tHrkexTcZ4OUYYhOkWEWrwEgIRxQwAPER7tlxBaD8O1SKSCJ+3GzKBXwAkaQHCh2WgEeLb
yz00IAAaFjFM4MlmS58jKBAR9ocG3STvsT3zc2oIgOQ+u5bZqBsNOfi1iQcFTUcWzUXAzP2voXoI
498wRtf4NDxiGBq3B8IMh8aoK4tB14jRmYjvpvqVHJy9YOJFxv6/dYqcjlVBzBvIO2SiS0FhtdQE
7VnsiN+/S0g6fRESB4RB+emsYt9r+SRHv1NXkBtMPtbLIkPhB9HP1DElAflKMbY4goneLaaFEd+I
LMTHViOjjxt9uD6xKmdJVlvz8ZIBUqWUYhX76pYVgFppa6mWyYwEBYTJBS5OmIJfB4f8g8/dmqgc
ElOdUzK2lCMarMYCA6yWnYJBLnDQkxzlELGW451YwacFH6e9JV5ae6ophX1g40u74pLPUA+baxaz
7G0QvPuGdtsaV+2IpWN6RiP82Cl66uFreii6APtWAYuzp4vGkf2Ozv3mi5ZuONSEWjJX
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 4 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "10010";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "10010";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 5;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "10010";
  attribute c_has_load of i_synth : label is 0;
  attribute c_has_thresh0 of i_synth : label is 1;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 1;
  attribute c_thresh0_value of i_synth : label is "10010";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv
     port map (
      CE => '0',
      CLK => CLK,
      L(4 downto 0) => B"00000",
      LOAD => '0',
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0',
      THRESH0 => THRESH0,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_counter_binary_19,c_counter_binary_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_counter_binary_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "10010";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 0;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 1;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 1;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "10010";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of THRESH0 : signal is "xilinx.com:signal:data:1.0 thresh0_intf DATA";
  attribute x_interface_parameter of THRESH0 : signal is "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
     port map (
      CE => '1',
      CLK => CLK,
      L(4 downto 0) => B"00000",
      LOAD => '0',
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0',
      THRESH0 => THRESH0,
      UP => '1'
    );
end STRUCTURE;
