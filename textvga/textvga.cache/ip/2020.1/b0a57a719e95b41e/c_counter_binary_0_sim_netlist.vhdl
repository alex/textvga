-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Thu Sep  3 22:49:12 2020
-- Host        : T460p running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_0_sim_netlist.vhdl
-- Design      : c_counter_binary_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
CZ0kO6dsQYeG4Je8bTGTPA1Igk1336LuZ940Pqu9BKlDozBtCT3vF8hDgQp3seWQOee1KNu5Ya0B
5zdJgnokeLuf8OplCcm0csyaB7rkjBpdiW23PcvOz9MakXbk5lJLIHl+l+hpBnPJyYiYdwjnFSpm
stHd+oYqd4dJ/9PiUEDSER1SJLXdcGxdyCiB9jVyhsvtyanOvMhu/tRznjZmmUmAMKoMZR76tSvp
5kAfsugQALA+ckQcsYoaOGtXhuxGfZvVlC2ekjFEjLyAmP6v/AQE+sZ5uyky+1XW5XQeV6lQiuVu
KhQTnA4gNjuzfTHfvJVPw72+pE99MOu7j575Uw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TCgDAYoptOGeocf2QqnNlCH43pGAF6PhBQnGOekoBMF2tLAjJxd5ECcdVwF+rkGDEjHDZ4Ru5o1s
KEJ7Jkh6sAnqLPFr614MqbXhrF28w0YoyN9vo/8bz/TegWKhUJhrNkmF0IA60Uw2yC2rPvOSXiuw
+mEXDW/IDCRmzhroclmZLEJ3oaRXEFXzn2GsRV+tQYfNKoiAXETSC5vcFo7GoDzvxt3XESeuiJuH
8LJjDr7G3Zu6OAUMRVQqD4brHHDPCiWYTE4imhReXXFQaVJaZpwubLPqliO038e+Y7RfaSuJ7D7A
vTvxWHAt04xr2DcyCI2Afc+siJCJUAdHM+uq+Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13120)
`protect data_block
03Wjkjrc01Bx0+ciBVb6GnFksDhdVINaMQCw9l5CqMuVos2lH5TStv2CdBqKuN2qC23l5kJxWxqb
td1oK3q6Um6ZG6F6wLoKGws7a1Cl8GOgsrgDsMoMykKfHhCJEVJ4e2ae3PtJVqLpfb6InVCy2wFn
IYOMoHxCX7QitTPhT5LtiOZngz8pMe4m/ZDPQ2HmO5DjkwVe8Yun5/o+YHTUwaljyHyyUEye8UE1
y3kt2FDIUN1Ug//zZvYyW4hLwJtG++VKVKU+xVY7KkBPLnGbW4xT8DcPJdyyfmBOmIyIdyFLDI/E
GwJu/HYsEaHpbUpE4rWX3ctJiZQbkUnK5TTOUMSJl+P3SSkk97BAtQ8Xy7DPRkoubLF8+aRnHfZ6
jlPDxjwgmLQf/PST0Ye+5mYlNAb0LKvzyUMsOb+59ZRTIdjBeXmGg38l4jhuB6zk0FcA/9NPWA+c
J+tYAXQkzEqImZzkS9t8nXBDfGdOVjJeR7DPWa7fTGcAimTe9xMj7/L4D3JoHNM2ZY4piZWMah0l
znu81t7tA81sKieHo/gRIBXNR6EwpCUbi/DsSkdz7aBAE/t+6v7xFdYlFG2jX3S+wCX5xNCNwyX+
eGLBV0nXqZWKD8Tcet+JvKJpaV0HSIPsklPDHQNIuMx15xcOEi49Y1resiInE+KV3u9xhJMs9elZ
8z29HVBFFGzTdR/S/iR4IQAFjivEaeIT7M+KRtmLxLTyr0MN6f8zKVd4Wbt6C0sb4wufcajwTml3
sKocYAD5Stsp4O4zalgOT55g3SS/kuQDz7hzCr1gKV9wBAdPXX3vZ6NZaKge/TyTmDZx2ZMxhzOy
J5zKsT3O6LZ5qTqmr9Hr0AYMm6YLN/Iv1Khki9enBISAFE8q1V0QLaLSSUIu/dm9OUBYWEl3bjEW
oNzbQbbV7BD+MlfUnovx3xI46ekBJcfp2jdckKz1hKYwN8wvViuFfgqjGN0mJrPjK8sWBW2aldTX
/kK+DwAmzWHUQoroqQrAfhOrvTgwfVhRatRJMrmXWjHepZ0R3sCFcZ0VWJayp+olpGWaIM8Q1uCs
ObcOrdtfhJ4TIdwxAOghyKpQLqNR+g4KtK8xVMwI1fbGxGohJ3zE40ct/hFha6MMoIFlOJvckOmp
uBMLZmQxJLaowHKTnCDG5UlILssdlesrXm3sNzDn6ADW6c7FLgqlur4tGXalSymmwonXi5haoUos
YSebHPPcUfjq5iYepKkXVd2TneBC4QqtoJXUNBoPsi/8dNmikERdP9GWVPfTEOPWQSUW1lqZx2og
QBOgCCR3CZgSoDMIy0RWOB3zMrB6Pjx3B0hprgjEeOrjGcJ8+fuAujn6JKX7jCdCmukLFzYsNxKc
NXsE3doczbaA+WxpskEQaORRD05bfCK9+H8DN9W/0rawuifAGSH8H2dSmLO2+DB6V+K43HwMAL0i
DQx/nJ/aPQ1zl3NxOfw99gxu9S4YLk+QSznv4BhooK8f2J5BZ7y6Kh6BY/JTZoyd3H+ZoO/TIxx8
jI7G7kv3xuGQKe8qGvh75agcPRX91AgGFudjqb8vmY3uPJDFEdoXwXiZeHCdrNaHmZLI+k6wnEQu
uVgw/NoaAC8I2CEKjDyCGVoeZStVkoSn34685gdLl6bAEozjkpbpEADVL4YqkRGP26nVoSbQGAYH
Yg1/9lSfMWbk+lRFfFHaYhx9ne0IONZxLuc5CSH+fS1kng23cwoQmW4vAf1YrQuYA8qCz0OyscSL
B8u9uUDScjCr6oKc9/zm6/u6IEWQ5pbqyx9h3h25vYCZzaomxDWBjxLOOxsRVvGeCy6NOFutCwcr
FjvGr/LsdjjE2qAD8mf0ViFL6Bldi75/j5n6/sEFiIKfMJiqoam6ZUdv1YQJZD5si9q7EynbkWfO
STpjiDIPtmPN0R9+Pjx3GUa3X3cMKcyYy245UADyadTgBpDqV1kc3T16aRVgmRNE4R+xi1cenYik
DLHOXgHZyIlcYqrqGMB4xPoVtwV34MczLhSAn8uhOjPi9ZmddiMj+WSHrpKbdIw0GGURrwkrJ+qe
zRzrpTK/8Ip5pDFvMieyNQqu448HGmo/bpJASCqvsWmQDfov9BBodD0BCwkhu34lIwTxoek0Uk4u
Kdxs6QmcA/aIjzjWj6g7lFMUINfJlGoD4nAqCmL5Qqqm3wZdT7oT+faDaObwS9gJfprkIZdQ4VFZ
oMmZjO2v5qiqiIRhJVR3cfv4oE2kxUDep7Cm5uvdjKJmDIWvHSyDh3eGqfRyEEXeygn00k0QkQzn
8/iSkhSuHa/9PnDRa0WTdX88Pw/j6Z51vjb0U2l5C0vq4rLk3quacaNIOGLt1hN1TqB7ywbFWVWj
TB3HmGnknu88dqJk99j6tFWPi2CrFHYm/egesjyMfh/Kxccbq2/60QbXnNHDJ1C8k9+jecvsTvo0
9AHh7a+7j0FeBD9oVKUCC6tISKUEvSE9xZmMcNxzYXH6Umyw0xifClP1kdHob5sbrlAWAV9oWdPH
l8EceGWPA7rUVksGE1Z0QMclL+dUoPEZ834UG1DpMPGWmKiWqQIf5cfJ2xvJiLs+slFhXxrj9D7t
Q+Tb75a51Wa9TkZbTiZh1By3GxXQ8aXPbHG8xV0VR4BvvSZA24UG50uv9PYHavaEnp648fLdgg0/
jX7yIJkOB06FgCisXIHl0jhm+CqpFCi0p0ypX7OFraa3WzNlGmn9KfYErgqFUup1s5yio4Ramdgl
VCppmmOoMvbGsONL7q9ayySUQmUADYBeKi3hLrXjjruj3KTwQDp0Zgeem3vb5o+ieFPiKyPCMOYa
WYlHh2L1UjIYRiTk7ZN1uiBLFggQU++fT8wcF/hZutXU7kQ65rw471w3yeqB/DH+4LkttPPRqXwy
FWOTnYff23u4MbXGRvW2940bJhCqq3kG2NjAzwcaTb7S6PHsJ8MSqP7J4SVhEoNP/fZ3Se0rAehW
IvZcs0tl5IzDEY9HQccHV4V08bItPv4zglMUfFnOIvs+g0/FvMjWHVefmj1dHZe68TUOhdpbWSk7
zZTqFIHE2Ddz7dcOLAD3jlhv6rlDNfXhFMZ53id7TKPEePQjm99RxHKtuSoh8p3KcT7xk0s52Yah
hulYnJPnCangYcwC7WJvsQnFnNKqvAxnuSm9eb12wyOsw38/EO4QPN7HTknC6yw3PHPZKp5oWpFk
4DKsSywGFsJCv4v5A6ZBrkiclcuxRXR8IlJs8/izq7aDXmsc4PQKDKItgXgJYRcjnDg/AF1Gv9Lf
yLxT6S3GkcBXkoNnG0D7I4byAUl9IzXX3m1b6t0SiU/ka9Fjamh5RUD7FVCJsLIFRfkPcgE8TrxH
O3Beh9VrHGRNsbi9i4FgEQgSgFPrbwdvGuSHP7lUtlSRJdl/FX6dtlrlnE7C9eJpGofFQzSbYq1K
8zn3piaSfekmzbFUd+del9RJxQ7HN/11sIJSOiFx6a+1NEMrQ79acqfkvlEEp2Im3nMEZfb7HiBc
m6n1hSokf4aBByXySHXZ00YHNYp1yKI/sqA/xNeFivPcZ4UMfXmp7gORQvjXAuWh4dpGySTbNg1k
TMa55sLXDb/xaGMZgW5nTXv7qsXy0/7qj2LVqxVNuJjC5XTUlwzUXb2/eK5ggZDnz/W8MrMFffgS
RcjUp9/1PO+KNJ35XgvRp99X4jXGIU9PA0a9/yDRsMLq/dO3bbwv4nJxS7sEDFZmd2BeYKZ37GcN
T2UnXZy/ehZUowu72dnH1sIjGBlSrNAWYzH/3mmQIuNNmDg8z8Hrqm2rmd4xtE4UfEjKiUC9cLWu
+pBwk/M2JsFC3o8xyw4EUKHqH7KIMkjA5K3NSsGYPYDm/B+RFG9YaFFMQLdnRYjgOXRYbjY9JAs0
LUw06WktbvtYuE4+RJpE8H7qmSy6DWqHomE2GSLdwDoXv0QtwQR+aZ7lgv0O2DURKzW57U+6A/Fb
oj/qLNqRrIHAOMFRn30UHhjJVGG/WG/85pApq4vJfxAwVbt3/RSXn2C3trdmcXhXQP+uuQ7HR8XN
uDLXQi44f4+3Td4T995esscqcr0GCXac4jJXD5oIkquRSaLosHivsVZZ5Vy1/F9h/hlpAQ/GFqdJ
nkjttZkH0mok35S+sM5GGMlzTma23BAEFa9sbf4y8fanpyKQnvxOrZqhYqnIpELzj95HnyCnrSli
jV22qBiTA7vlJTOTBEyX7odSojl/Y1EeCYY/EDN4mmPA8+vdjH2iJuDh+j1ROILyMy412dTu5DdV
66ccZEgkK9uTdVslC2J9OmIOjBBXqToUpKjzcLsC9Wc9TADHbjW/lcuFE1icZHiONx0eh3EoJzme
Oe/JIDBNUyCp7XCc/9PJ6HUrBv5Oa/FpCE4Nu7nXe5nCJQuINa2Jg05paFU51cvmFvVGfLqnQ75S
A5rTpmttwrXhsEl3ROGb/fVgJ5hibSW5jai23vjzG7meYGddgbixt8gz4Ke7zHlPtah87995f5Rk
ly5DbKiluT9d+Ar4qpKsYNI390D5a6rCamn2fyRYZUyp9ssno8FUTDjsU/WADv85Gire1WWbov2y
lz3LJEWKfRNxnT4HravIbrQYqXmUhjLa3P2A6+jPOdNN7dUFIPlPv9+R3pUs5F42vs1FMnBK84No
eqQmwBus+KTzR9AY9sGsGS/njCrjuHiPD4nMRPQbssPpRRNJsz8QMATcxWlVAqVqwISEe7w2MVcG
T2/K0fShwkyPPAV5cnlG4GKBv2x4YEUGjc/P2aaZYnic0ZukLXUTUlpV1wceAgDDwJmRd1FjIR2A
8NyWznqwJE/rnllyJ28Yh+9Dzr1Lt/ovnrrpYGl+pfeil68BN7Z+spW2VtqTaEsgP9R3KNV8WPtc
lIkAMxmzHOHU4LJh/CLFWaTD+0ZUC0w7iD0f7QUhtXmZ1qoTwkPTyZXE4vrGPNlI/tzV9/9yqpzH
Cs8NeKLew5D6vlhB8zrXWJNOlcJlnj91D6M79VgIttSoqtR+ujO27nmas4rZ+S3cyzihEdL+Xcf0
M7AdFUZqP2kbb9hj4HME0pJmg8TjgptNXqOat8TakSvh9KoDCPWQkdHBL02Lu247FzgHVsqXGtw8
Rq+lphP8eXk1JwacrGyQCnH0eDBzZWOtd9G25R1uTDQL+38Pr7CG83g0nRop7/F7C8NhLeNqVwrn
GHYkJeeVEtrrXdIVFOba1k4Eq1ynkZQBKy+eEP6g+p31BW7finzRK7/uo+rAaR6lIdbSNreDXxrS
OP1fiU+U12OyW3qvcrzYtbqMLB9X57Bw5MC+J3PG7Kx8do+Fi3gqHX6Z0uZzbvxxeiYjfuiU/mMf
qWh5HGS3p6cb27PkhgQg64uWdNyQGOb1F/ZfosbzRAlBid9AnnzgloJXpQnrdqZTJ2I2tOKzlT5d
Y7rqcsxSpUq2CSiR3LNivkTAflDsRbL7aGTzNtc9Mpj+3F4wyX5R1AmmKeFO16ABcgkZzmxXDsQh
gYq3rl9QzTEO5Fn9kjsfF1qG4q7UDbxaCn2JRl3w5eF1F489m6VE+cP4Zll+5RNiQYqFdaxJGVbY
lG/P4mzG1yaLIvu10kiOCpijnlNuQfhoQlfzsUsp5ZWOywM98bTvk3eh0uXgZdUCwitWqGqmYRTP
m6c8jT32UZO2ACY5CCu7ycv/ebnXHvYGyUzCA0UQOhxp4+QVjRxwXQKgq7XBU/q0aUaGWLO8X+TM
+ayzC+sK7It9HkGC7ZvAeRxaEcG61qdKjER8zzH5zyB8y16AxB07ga3LYNLSscwHO+Y5OqU6/IX5
6iB9dBv1dzdmHayUfefy9Y/VfY5QAS//4VyLBaKA3mprlyPm/btVneqzk7YCrIZjgbn/Nr/nzBCW
7qk0ehRXgQ8yHwMwR09M6PFKGC25P6QwNWda0kLhbJQNqysVUSzyj8P6CvAmCI/b/pYfHpZnfLJC
Lee8GtV3Yb7ATvgUsJYzOjaU6byRRlcD3RrYz0Ky8q4pvgbcf5xvDs0FHysTKHNpWq68tJCGMixu
SU5pSjVItG4umhmM1rfPaRh2CTK63SX8BQIo+CIWjQGL0U5mqjpx8LhLzfenEz+XptevXPgRfPYH
HHusMemLaYDTtHDdxQ2gEk2oUhDD6irW1I5xUaaI9WIiX2j/WdjmKU3ZLCyWVJ8PnAkmcuLg0XvL
YIRNoIDuEMr+0rqopz93DDtQkbB4sZkTuo4XsQ6Eqz56K/9EQA16hzJYvl4I9HfzqNU0OCv7p8V9
2veWS3DTBiRkSFqGMF0cqT+0qUoG4HRLw2FjCpCDg+dVU/eA1Gd841lOuyvewSJr0/7ycTg6nSzB
4aTrYY+RkK4MjQ5jGndF3ZbmKmhs2bGE4HPyViQrfTAZ08JQzRGZ8VfcT2/FKVrmb0ip90PUllQL
VU2RZIbQaRAKUxiduE2xcyby4K85SOYpMERhPymlpaeIUuM/K8B1Cnm6wu9hCKrIMMKV9DxE/xFQ
Qf+zkWKQx/mi/+MDFSBWWQAlUe7l/Gvb/oKaNbtNR3g6GWxY9ghCL5/NCsMfDb0Lfzsh7lf5QEcO
lh9Xcgej9c1snFTXkNRQHxU6W21IylmE4duBoGVPrsLpGeQwYEyxpuewxZC7k8tE41Dq0ZPVDJik
0j2eJ85mEUAbDTtSarJNX1hy62ZiMn3lwJqDHug3JS4ao7vGQ6FwCxsS4dLr8O4z69ja+55cAuRv
OmO4DTj9taAwP0SWPfcHgiibszg/LfMhf9iTg55NVbz24XpiTH8CA6JsigVPjpBym8ic6IfJ7nig
M4x7rt9fjixF0mj+69ah5GyLFe5bRSXCj8Qc2sDcPzLp1tT2Ez4qLkCsK92thLjXNP/22wR/CdXC
ZzUeUQZa5U2FnOU4i6DI1GFg77k+M+Ic4NMewuLZjrPPNILL03ZmTBvzHBx9ailbmxPtqBj46lQ7
Js8McjL9P9sle88eBnvcDqvE9R+vFLNv9w0TyO7HGTp5oBn+P192+XYinRXtPrAH8duU//9uvHmn
iI11emcmCwEJpmRSh+ShJhPViCZhlezUrCDjF2nZJbD6YQdGOsSzI+ZIHSnuc9GbGHaFsZy5Nh7k
rDBmrJRtboUORw3u8eudaIp/8qTXlI2hCiuYrIEmmXSiwjqi26da9lZFV3MOJhtqeFLNI6TfJay/
YfOaY4o+ay4KtnpRK+m7f9lPCBW1d/gB8EJrKE/CYIo7AqeDyyvWOGhBtDG09O5p5ldQTWztpDUr
lSw/SzNXGJR3u7xHPyOwGiprdmskD+XW4KQnFAJ67KUA1+3TwPHpL2wfnC4IqU/SqD66q199FHqz
HvcqMJN0rxJj2abHuQMizhoXeMB+CtJ3DCeNxwzo6SsszsBZkxvbKq/DhzlJKB40G+KsomSTf/Td
i/JY5T7Yo9s4YWFVQuEbGwkn0sU0Q/kHL+7vA3VXvzTu1WTxbZ035Nh10WbPeckIe4owcKT04ir8
Fz3ngBcBqLA0NrlK0vl2W/CJ0ZfeWZrKEX3aJ5lN3xQF8W0qjUUnvXz1tQoY87raGh4lROGJqRSd
ei/lGnAkVC+TwIoRcsxZ6X0CfmWKJnYxngmDfL/VtOPwNculeyznNC4uCbYm6F6xjOQfUE7jbcaO
ayZbT9iQI5lrQtxajJepsESwprjydZcy7NNn6RvsJ6wQjXCfmI/teuV9nLS/ILNKSBEIcVyrlDnM
TP49kERml4YlV7Z02eGkRJIuTUpG43CJg0e7fj+UTqpCX92bygdmA4fDvcmGfpxDUhM2p674NcYI
tUdWiQLSWMDGFywAowlMjbmrb+60iahkXMg5dnOmLIsa5lIx3hSS6+K4kOCCo5+igVXVJxPz8nU3
0CQOKZmTbmdNPPqoorZzZodOA74GiK59zWvBgaHDEaTG3gRKI19Cd2cVNX5aE2BkbTGNFhSh2MRY
8iiiVsipejH0vmBOK/GUVJ/FNeQyEYrPYEA29DyVW+t7KH90fT1quMHlv4AMgycALBbTQIG3OgZa
gWWTrmi8crJh21LMBUka+LCTsBBUYBCyt6vQeixvK5vykv2VhF9Tl860Rft3o1TYFki7+NquBM2g
v8CUd6YCTRG8CVXsZe7zqbylJefEw5dxkibaWVDEL5N258IXMk13YB4X4uAEl1QBQzGC26H4pXWI
5FrkRClZR/70wEySlSZorWQvv8FyjYlBcqWpLR99nIbnNifUPKXarhWm/1prII0YAF85pHRAVlnF
IYattVsia1gTd84WZJyjlhjropsx82kHhLBJOS8S3FTRX0/Gjc3yx/S6te06lKnZH1/FfvQCVQ3I
4odukg2LvLPqUfCPpTBwE1ytz4A7iR/vOqS5RhXGLQ/3/pLTR+K+FHB2lvameHEXmShcl0+Uemnv
KkolN9FvoiWwgU5Eo3F3shO3RrxRNaKlB6fb0CmHvILGRniD4xzH2g/MfJNCjhq/wz6pRgCuiAyK
3OYYGbS4bwlBo3wU37x04+JAuEJxe0PlxdlUmrVwnu6rzwOMBnS7n2GMlSfG6dO6HlN6Lvxb+Cdn
PTiQXVJT8OYpeS63rikaQP2AZfQPvGjhRA2Gut8T/iuwOrjcZxRrtn1Z4RfhmYRjRBbNtrUtzPrG
EeLDkIREPRzA1eybjXg7/Z3hWQI1H9W/YGKq0ON7xyD3b4UT//eQXyF0N2Wm13MV9/tONLyishtt
pUjvOxf4VGAwqcXoo+PqyFskBrQAkt/x09vNaoDZdgpvJ3GhtFc3C2slZ705/g9ejByzUvORd/XM
rbkd4el8QVK2o6mp+RnUXPWFZOM9sTXFMKrO6LgGlnHLuhxK4ZfZ7H5X8aYrhcL/Ko5DPYBuBJsS
2l2RcCHSUdG+Lu1+NegHYbD0qa9UTW8Ku4Lhcg8I+UErzKodgS0w8c4U1A153MnIM9qwWmlyKU8d
mMUDlhZZp0Nhic470jEVxWCedl9hlpqK7Cl/kQuJSZGH4A5dK4lSjx+k926phvefCwnPSaT0JOXg
P3hS8Q9Y6lmqqg4Em2yFA49/qo32t+/DpqHzo2LTFYhWnJgb5+N+9pnFAO7oumKj+qu4ca/hoSUe
LU2wF4GxPAgBz7Iy6f1fVzh3dFuT6wBC5JrTanaBX2GqHjnDwvi3M45+pisppjiCck9GtpDban8G
tZes6y7DxcmKX7TOq8aD0Cn1ZqLazD8Pg7SpMq54xZf3+064kKJWItAw6t94t+T+EylwsbFm9E05
GIP9oG89HlDlIJExWuoedDiX0n0BhGGUqgTLLHXdv/B9YwFUZJgXqfYzkZ3V4Yr6I+8BaOIkoOOT
6K29SYCz02R2ssRIcFYwWZUu6eblM3T23fS7+G6ALZSxc5oJK1tWXRZJNn6fBTjTXg6RW252pR3w
dM6mvvgc3rs+Ie7Gy60zs8HZcCQZ2iHiTDK9F+7ws8TBf+SlpflfEHXdxkXwsmSrCzSy7DBjMcsE
HLgwC/Y81j5z4uLTV5gJvzQSa/hus8hkiEigMVY0d4yJ+WZBQj68UwjS3F6pHT/FcJnD3GeudCJH
JjkoUvMLUcJMtWVaxJvme+KLuxyURC7OSpgOw9w7b/EdGCDVmWDEkGRkDyeNFrY1agwKPb8iy7Sn
8Sf7/qnYsB+l5KKZI1JkHAG7rPrrVQD9hJpQqsOHNe9412MpNTZ8WDksZfgnD9oKk/+bdGeqF3XH
fFheFSHmGsCperEEVk+MxscIjAbTYYYyC1NUS6cgyyJWPLxMTJGkpUhhHUBUfs11LNur2rgOKcwj
YYt3IF7/JC9r7SSjrQGgXIPiC8ifZAx6gLbfoohg1Wnl9qEBakSizjv83FdqrvULHeoQxz1P0b6b
tzVygghuvwK/EG0dZU9HxR0siXhMx4Q5QzGXOUVO70t9oK6duLvBsEpGWKGBIiuRNonytQ6W6efv
jW5hQjxStySlvizOx43Nh1IZWu+dIDWbbzysYrgwdmwMRaozLglGvZylwHFTl7EibStHv/qxTaai
OL/cbBr4bRDnNJGQfihGZrtgE+w4etdhqi7RjWXX9HO7UcLobhk9SXB7+G4cVgk2HdPIOWaY+N54
OMS8PQ7aDeJhvy0UuzFLXVEn9jz7W2Sw2OajC3ZbzVjC3LOJOtkFKNPVlJTQSMa46uFK6Va+bL/1
cFTtzXVDyZMe88hINSDyaqhq+8A6MJbi32aZ/F1yoi2XDL+7ApkjalRO2Mepk0c8sgy6slfW66fZ
SSixayOcd4MluPhsXtYnCWWr6Um8kEbxsTnEBjfx9j285wdcgORAY1haXlSynZ7NYjeJv7r4tH7N
OS8CCLdf2Pq+mTAJ6VWaZvaku70ByS4vmoqdwuE2XJEb8LkhIJzCt/BasgSlx0Aoc46LSPj73KPR
GR6wCWSu551McYct2WphfOnK1kr6SKt55DeKEz56NmNMNxujwXjR/oOMLbR0iBp6vMN3CNxLBE5a
qhEra5/C8Mfqh2zUg2Np7sxSdRR9dQdBZIjzWrsjXvJdvHF4FmLJO7XrGLFl0X1xU17PMluyz603
AGkmcRviajwm8wdC1zwTnQxmk3ead5zOeORJlFEKYGgmS+ctsJulCOLTeGQFklSQXsIdn1Gcqs1L
rgSj3mgcrfmWKcw5K9FaWHpUxq2Sth8hr5cgy/FdBpww4Glr/ofbGwCkVnXlqGGyUmDD4hy/7P0M
OPJLcKP76EFuAC8ngxfGhGrd1hAKwGo8OcKPhsaAsSQYElMbeMj54BaAMqgtrYWzEiciMxmW7ptM
rReGcPZfbanGI7wfq8TxKajFn7mybNHrHpek+PABqnRLY7xNAZ3Qp99jGVyM5CqM3hhXlwKGWM8T
rfOEVucIkXxSX7n67Sd3P4zzJeDiOEEYXFKw29Ce9+kpjzV8V0z5NXVs4XX2Qc73hhYHWGJKScct
gL+RZ/swwjUy8Qa7pNacFvlmWWoGm5Ifqjw57H1m4hcXeYIJspvE1x2VCGsRvXcsjqsuiBPgcUuM
m6RK4KUjGB20J7WdqA46xtBPjUNDk8HUM2vdLfyCJz+BMowmckwQIle+etZIlfAEPQ8XJbeewcp/
EAqy6DBBs6Kh10eCXEDUaYSpjkEuyeUWyXwXR6D42m/W0qVNIjWdKhxKEqjk3KVtaCwXBlZACAiW
nLDzr0+qmpzoqrRGzztOLfnJZE2qBiZMRV1AC0boA5QNJ9HfvTNV8c/rrUxJay2tMV0v5GSc6K3l
PyYTqWhbAnCFsZ8VMS8IfYr3p5MMlAleEnRlGHePumo6dHg7KIYiHw1fLMgdAdRXpJe3HeeEFmG+
4zAeS7ql/x6J1bmE1av1r32icbxiEw7gNsSJb0vM1JgIJ/btgZfAHxsb5MePSdp2S1cMb50+e2hd
IggZanJceY+OA7Moy2yUOyYQ/KiVT8z4zzuaqDnE+vV3t+9VYYqhSYQixhrRIfpHpFEpc1jq5CTa
QdihSy8JUijZEtsdemoAe8LobQfQe2GcAeFdcA8ZGsNipl8hqgXxwa5EVaIosC5sPU6luMPZ8qd5
OYEPnPq8HbwCh/09m9N6sl33LRviRq5Z4CR+6K6fzNJ4uf+YlUG7MR6FUGDpdRCCzWZT1brjSumQ
mGyqYDM1jSed15+EZFWRYRZCr0YKLkbfyKrloXPD5PUKdaslo+5ytHXUixrFOAQynKVe89BaC33O
HFBFPbghIL8AeKc0ktFel0otJRvrgkkaV1cyekv6JYX45ibLtLybkem2Vd2yAz+mUcboWVzN2hIU
S84RCMqe4FVsJX55C8vqpF0pKA41ypNCCKWh50Z9u5oz+lyyd50T2rUZXmgv4+8WBEqmgHh9KOEb
fKarvcCHrUitmcdHIwEuXeXmOZHC0kHyb8hte9arEtLQg8OE47ImcTb0cPyoCOjZ8IlfdzaWZ9W6
hz1wWcIMR7hboATCJTJlY0Nfh5ilM/ZFLeuiVCno4X/DyduFLGxgjG8cTKeQ9Y2XyVKyr9yFu8Lp
BWxAq4quLldZb8kwufzfbIW0BHUpTkO7TB8afNmhk/rEvEE0n9XexvNa340x49D3sDMVy1/yO6E2
IRBFoGoRelFdZlI+LyYqELsvobUupb++lrF2YPHpQgVlzF5AwCVr3IXrL+V1tsUJLIK9AnCT47Jd
1JF1ldLe1P0d4XMFurzHM3VsOHOn7nAXtIVxpcU9cIO8fZSpp1mFkeaT5KQsuaAFrxXQlDHXsUgy
BOV/Lwt9n+poAqA7g9pYG8D1pF1gGL/k19lSSSXQ27DDaAMWdoIAk8obdAGazTJJBIv0i0PKh9iI
9qT1LkqT/roVCPeACNvKfKymlRBBqjK/tIOYX3p8oN3A4TaWsuhJ8kZBCapvp5Gsn45Czla6y4MQ
j8WM7Er6CRRbEvcxnX1/SLyj7QZK2oDADoROeOlWVfLrWvChwWa07sXXGgwb0ePjS2Qx3k823TA4
+OCYQCUg1D3kLM6HoqmOI7ocA3/tou2gBernB600lJ3GhV0K/8ICVsdZ1Wv5wbAmovbBDaoX7mhU
FKNOT0xIa15NSW5i5MvtB8dXyWgGOTrkwt91spvMAqFhR14vJ0ug1Y3Nk6NQcZKPXUD6jvcU01kv
7oA9IJbEqMJDe/k3IGMPVrn3W0Cd0cWkE4h4VT5vay5ZEkPWx+kWqG2OtNHe5hRQqirw1la+V070
aV0ooziYAkhOO67cT15GprRVzjX1/4CWM2e0Nc/YFr9E5WI8SlCXLyVIIZVBm1meGMSIoGaHT0wW
fzUDG4smSCeQsUMpg2VvnjpYIGwlIm54ClxlGDIc1HM6p17t3IhT4h3u6WUYXCECVUygwnFb8lvr
NrWocdWbrcogP9QXlKbe1Sf09pgTWP3MB+b9meYz8gUKWZOdBtjxtaG/VLYF/aBvP9J0iSE1MpOV
DUKX+fBUOye1tPRegyzsvfgH1/qxvsWnCJASbDvGpFooYkq1yy+q3yoq5K87PR45a4e9PditS7W7
ogEyM5+YobE8cDuPlHtbjXvLnaPbvTtxcsopOlp8RSSi9mz/dYT86bjNsU3U74D71Na8gzxl2Ol2
YKTg22DflGPMQhsIt+1GEUgzU5AheVH6ZppfMcyWIIAZpDSo4UlVOT0IpNtj7CC8dqfi8B6187MQ
85jtpz9si5WmBGX9GFi4mr5TiDGwo+jKVU34Ecs0VyDRntJ6u92FSPVSwgXyUV9mVPM/PcQ3/2eR
f4jaP1QmUFHA9zL1UOYBQ2Ljim1McUzuMbj2s6nthpjGhTjGQSlEo3spZhPdIBz1J0f9fi/IqWUV
WQTsw+mV+9JPmVT/mUvC8HOewbiqGDSShsHdJ4OqMIF3IzRxCcsXPmQCxYn7UFv026va4Q6G/yZh
AN4faArzCv+SogIhb/aDgEC3bqYr50pDwwQ8lw+mS0fL54TbDx1kryStfoN0Ya6RJ5EBgrxcmvHE
LpFpqVPpS9QBbxNLzc36/Lcne4tzjSJFYuHnVxLUnEeChcFRS5oRWprFaBesdKpogwKQJij7ulnx
T2e8KHt25QLPej8KCx3f8yXcVoRNKQllyf/p7iOMkoSzjhYo2XP7ccnD0j0wrhUkFzRhyFaVKIhI
dwNPe0GEdo6r8iOqTloluUtIapPGWexq7EKlzPtE5L2bTZWuKYlSjMWoEupTdiczfHWSKAlIfjSF
59MjQe+VbYkpV1fT0Hhm1w81wxHUwH9wciEoesvJYAIaChL/nkU2oLjsgph9jhy8JaT/j8WeZebK
uhrVTmf9V89s1u7dGhKm47as6VG+ra06i8uGesIKj0IM/9JtpM5qtMW0VjwW+XUi5EBkDGx/ywBA
JQaqDLMRsTkI38kRkivupYa+xUEFyW21UuPyqTx/A/fzlkKhA4R8pd6sGR2zbhgXNtFHFmhgjetA
kRLXLfnzxeHubVZgnEEUvXWqHvrkubD6jRhv0NFO3KdfomN1OQ4LEXisVPpSQPgiYLYNou2uOwof
sxRdv2ND0gb5xJTTNMANJ2l1xN7we+t/oWWjJyJTSYvUUMQXDVg+anDpCK/vTFfhdG63r/JtaOKP
Zhlzm1fDx7UnWrG9vdBPEpOdKAe/3k6Bj0jZSt/0bL477kUEakJmORwfv9NvvnOn8aAhGO5xB8qQ
GBDqLE6xJgssfp7EUPd0pmrEn/lcrr6xdMjK/Ugw4wRV6gdHvvIJG2irYN+lqjkKAYlksiFhHEXU
PybbC92Mkk6gvWomZ3WpKNhpqrA18uzGjs6AopQLcFM6gwjv7nrLehbHC2fIuMr8nkwbdqVV4saE
ACX2qi2RTiW6hnLOnBhXfVbhMfrFjUZXNhHHKYEmC9FJh1lzG+7zdm6/iH+DC+sCBM6o+4ewN+/t
zIIE9uIig37da5cWBMG67kuYE7336s6J3eRR/svwW5/aXCJHasc0NrlYCRV+tIT5SOc8Pt5qJZlp
QS/AXnD5mpG4/ccXMOc+pM12OCwslIM5Orm4W5AUaSpDat5SSQm4p30aDj9xQ/IeS8KtEPzSwEpI
Pi/l9ewQsfac+w9+6hQ8QGc6bnEuJPyIaftcPGz4IvJZf5Pl1lURg02PDK4BA/hw+0/tZztnf7FG
O2HumEQuJ567ys1zQhOSo0nMgyPPVcYBrabqNY8uJV80Iu6oHGh358MAYdTBx1ZbB6pgmRzSprV4
TQrx6VHkFBkrU49Zh32HuaY75gyW76NdysDnWsDZvUHS6gEYS5dgjsRvb8pomo6ldOgaaZTEmoAb
bZyZV+dCEDV/eL0PLnyT1YbmXnD7NQSrnCJXJxU/tZSdCEKfq1vXvMYa+GhEKtNKYhZEuh1ScN70
TMz54HUllCb/EZ6vTskz83HLi3zUX0gag0DD97aErmK9obwVQonFTqlhrn90sFPxy3STiryRGkdM
cXZM0zmy22hIP9X4BDZ183O8M1aHyAeYJEgGH/sQHMu/rSTMnXR08weA811S8+EyMX437Isjukt6
fEVOkdmxtwJAyQjjljFjNUgmpwskDMjz2YMwfCUklmszTLNBilJ27nM4ewjGKJMAh3BUQD3UbJBh
3bWo4S4I4zYoKTK6zWp/i3P30txvj13zEQUhKi0oq7lSe9gIpiMUfCpL/1jDNtXb5Wk9H4upANYP
xTdFjPx4duXUuxMhLnqeTmNdsDkshk3ECLJFVPudm83xt+Ab1tCZ5zONjElz0V3vHzNB1mzASpg+
5CMiAX1HZZH1YYlYQBj15CMXKJQBWayZeAPv+C7i60TJQiVZo3LT8qhOQuOnlms9VJHJEVjHxJDB
pu9cNCSfSTgWKko0+BZn64UWs8xD/Pt4pdrCfcNZIRfNIB8xUbunrKMk2BT4sT46dBHEQzrzje8g
lHyCWUj6kW7l6clOg5fZS7pydocm+v9lGLJOV9sI95oj6s7uvSlaPF51Fv20naVjf7kFUnb2VBFH
Db4lW2vUG1hG6y9waprjxK4Ulz8LKrwKgw+Buu9n8S3nlj9wxbZqRk93Z674mobIwBjB495whCiC
H/bnCvDZVo4viZZ4GyYA6zXJYWylD4RNBOrO1OMbN7qbU5EfkiGZRCCcswqGbBY6ddDgSxZXku2l
e3Ue8gBFtEeUMidwGfJ70SY/uJdCPuqM6tirbQiinIWXb68zbrSCS4JM30SeI/HEbonmv9iLw/KB
whShDNN1bfvJhpE3Jf0HoapwmfOIKW3LPzodAQk5vPCj2JVI75cOqy7/+Reo49KlGrrIRx3N0Ulr
fpUyM7ByheHQy+/O79GY2TnCvqbCOT8iW4IdjNF+75y8sSbAa2RuBUOyANJAghKkp+uIT5nheFMc
fnfS73xeyLy2uJ4ed945ckzn+dj9Ms9WtGy0YodX31R9rJ+7XDbLEFpN8yXIaGEsCgWQNPOeHUI4
QR8SRjCJ7vNx4B0ZhsPbUaqSysCe2Fb1jkBIklL5CsUUoPIERG7DjR03vMPMCH2dyGKZTpf47rHg
R/LhCv1VJ9W+TmxFXt0B3N98EKk2D1SLz7DBOU9okPawpfTmvwZEI0B7cYH7DtlAYa7BkIM2manR
U4OGaNv25v3bAQcFBW0xxY26zF+I2o+DDjLo88hQ0jv9PYlYOBm2B5rTDAXgH26/NnbeoFuUvXXD
N9O5LT5+GuzoxE5kvEh6iAp1bCiKGhTPqRfIgNx/OVZOWSA7U0M4E1DJv9CO57cgVEBcYCX2tbpr
HQ4g65qdEqf/zqpByJx3i3WMxhqjNs3TcY2jwsYcD2y5ulmIUMjbgwjmMxNV3vcg5iM64eMH052E
IxPJqOBo0di+w9j8JW5pYIN4uNj99SCDRYvivmi8d8YWt/Gp89Pwpm4fATsSNH9GK661liFZwpm4
aALq6CJUfUA84PUOre7bXTSZsre8VZbN0Vr9a8Ur2d7AaYsyP0vfZjMdflvvSFBtpmy4wFiVfVx1
JNV6UAU1+0OEXM8D4TC8RemQWKq5IPqyRzvuq7fvH9YR95KfpmH/ZOzC5WANSJc+VqZVzgLeeR4d
amb072NtE1kyecQDSEi6tWRA0yy1azjTFvH4Ey/wWz6SQvZXzqZuIToX48rQzNNszavnmp6l2pbQ
W4tVDjDsgPTSP+3eICc/x7oDZEUltcvw/yOj2TWi+EW80yFB08+k4+badJpKjuALVXyDANhqYBF/
VGsMYaaYzJwSIsfymFfQQASObAKU+6CYLSx71aQ+Vt+btS8Bklh18pRE6XADhV5O5jVglSuBcgrZ
/75k2hDhvck9/70gK8lt0py+/wvvbVrQ8hPur6W1MpD1hHI4VrNv2aWCfBxU0RwPfeVJ4rkN/Byr
HyvrvoWCDcDt1va4EVG1i4Sh+BlhC3FvIbrKvZQQqSUpXlnjaluLZSgi0q7X0Uk80Bb/hZOmDCNl
OlJWuA5bojshwq3TgyC2XqRIGHEcWqr6wwum/8kgN4upXwlBxV3fwzkISrfFhOXEV8cIB80OEYQj
k06GcXwvix5HEyvktt3P0sLcNXvBKRNwEkB389fIPnYosfXQrDWVrifwPnCRztJKAvtn8vNSo9Pe
AQqFuQ/WPnggpv8TG8BX7WpKzq5cl7ncBg9t8r6aOqNaPue2rlbC9g8+kmGjosM8LDdiraBgnH4E
ehrBIBpIoJoZZSQO5OQZ+Acb8uKRLCLWHOQDgwEFzpJ0r1fkv1txdkfp0R66nbB3bhiP/9M76HWv
QwyFYMyRmbXVZJai+nZibO7WPigdtgdoMntDOlorwg6u2PmryegeibKL8vh+NhlDss2AvoqouW/j
n/mu/GpeV+vfnqwkdJ0z9h60sdQVlQUSbrqUV2236PHG5/sHyLrvW0+4b0X0lxbKEbmSaIj3sI2n
PJwo/TqPMxGLgn0cXYKCX8kBC8Yk5JbavJdUdpdRM1clB03JBp6lsRIVRhlt7GoFZ5eN5f3jtSjS
XnwqRWSy2q8selkaPQj9F1Vp3SffNZRYJwgxIh51xt7jwyKWDclVOxEJnS6aiyIBycKKMsqTZiEw
7f7pNPLO5KCGkhF75b1kGYgOcOLfY0Q8dSMIcihCkjfDQzpbw3frCKe9ecXYiOvls7p6AdPuoAfV
BGKAMT5MTmX3hA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 4 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "10011";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "1";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 5;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "10011";
  attribute c_has_load of i_synth : label is 0;
  attribute c_has_thresh0 of i_synth : label is 1;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 1;
  attribute c_thresh0_value of i_synth : label is "1";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv
     port map (
      CE => '0',
      CLK => CLK,
      L(4 downto 0) => B"00000",
      LOAD => '0',
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0',
      THRESH0 => THRESH0,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_counter_binary_0,c_counter_binary_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_counter_binary_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "10011";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 0;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 1;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 1;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "1";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of THRESH0 : signal is "xilinx.com:signal:data:1.0 thresh0_intf DATA";
  attribute x_interface_parameter of THRESH0 : signal is "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
     port map (
      CE => '1',
      CLK => CLK,
      L(4 downto 0) => B"00000",
      LOAD => '0',
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0',
      THRESH0 => THRESH0,
      UP => '1'
    );
end STRUCTURE;
