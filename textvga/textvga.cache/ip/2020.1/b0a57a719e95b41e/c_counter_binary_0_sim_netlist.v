// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Thu Sep  3 22:49:12 2020
// Host        : T460p running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_0_sim_netlist.v
// Design      : c_counter_binary_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_0,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [4:0]Q;

  wire CLK;
  wire [4:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "5" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "10011" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "10011" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [4:0]L;
  output THRESH0;
  output [4:0]Q;

  wire CLK;
  wire [4:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "5" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "10011" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TkvbMyuvv+0cEfcKBIdKkinUMac94jxkk854HD/rObI02DBkPwXe3PGsPB4FI0AVgvMnvgWu7zOf
fxMn2KL40AQLb9edAn2COAbBiZpX623uYcZtXxIGVj5D8PE//hXlP4GXe9V3lkDSfhQPGIUmASkB
n9JxsbWz0/vskoEhjbB8zceJFqgacpxLLvZBshgz6KyiOgwP4dM/hMCmGhIgeSdGVUKFOSmy8hHU
we0ZUv5ypo/f5TSl2p/ctUBKo9uSNO0Sur+hiKsNTok/0NymRfaHlHhbz4R7kt6b6g+k/7EOz8Ak
LL2fULrUkf8neMBQqiaMHB1qizVQ0y+hIThiqQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
0N7N4meWwP6MFDXu7yrzwMT9d8xqXmoBDaM3gkyCdgbRiImnNY8G6xsiWX8PtNgk48xhMN0QYPKu
/JXGDc38NhlELP0qx+cadgHjcnurtqNa1OZG067PfGnuIs7wfg2/mm7H/TJl+xV+NkJ9Qg4+x++v
Zze5Mati4xR+WvmYq4Jg5Aw/FvXgNxnNdbfBRJooXNqQ3RhtvSwAcvf2voL6qGKxm0ztPGdA9VtK
HmRNfn59lKSEneEJUdzydszV6IPK/YUn0vall9fyuYVmIRwVDaXbGo9xDDsPJmd8TSV6B04RLwyB
/VevWW0QbU4uWMFo4ZNG1m1I30dHTW2Fx84YWA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 6192)
`pragma protect data_block
7rgKvyB/GDJAh62BiGMKAvzKGpTY352KMShvcEkGrnUyi+KhNbS7dIHDDsO4q38nCL6IQIIxuATR
FxSatllact3Tu+/NvbtzZcN/IlrYJblmw+uSj8923Q4kMtPqAVdBHOcZF4JEh/KDelWp8KraUUQb
Omond8bJLRhT8S/mLavS+vqUWDhaFqa0lSyb1tT6Tf8ZrlNMF6WGULjKSIlxrPphflM5tLSL/nCP
MMe0dVBVkhtmFVoHeVwl1Dembj6YI7zrcF9sy/0dCF4NvFyTXZTNse5cGzEaJO7AgkEykzgBKUZz
P65zlJ34SgkvlYFP+eJi6kaB0fI8g0hvnNtPJD3gRIotWQN8wzhUPMm9zhs2+1vk6dIE4NKDGYZn
Fy/veLldfrk9aZm9UWbyiiVNVQgunxuEBPyNjSvNdqI6OAXPZK/72WaOo+dxNMcZ4oGtxsDcGLLl
Rw8/ukLOzl8FuMpevltAmia4TEBhkwOlQUPS47GE0Y8t4tI2Fl16cNtktvr5TaKHDzCGiPEJ/z8q
av2R6DDS0+EgbJMPbbxUhkHeCNQw5A4tM/19uBW6xMSR4LvaNkEPadS4sm+9wQQ4xt+eyMI5eZLD
RdOAmo8woZ90Gp7t9LZPb8MyaAYIRj894bz1nV/RSJattt7aaeVtVq5OnWrXJ5zeHAeUwmF6BuJo
K4dCi23ELy2SO/YLNZA8G0zyMvVY/XQnld4TEIvjkkoGXm9Z2xVbdjhsOv3fU/ZUy/EopLWXfaWn
ri0QFftFlIejBHQlWl7O+kYz/7tiGNhlJcaXO3ZVQ3/VRcGFul+07thqDYCYptK/5eS7Tv9w/3dM
4Oh/YIGntJpM7bcP1jgyjINAz2RpQy7JAUVz3phwS9FyK2xBEllVz9tyuxNFaEK8pDNzbdhvE/7F
UDB48c7prXvYHdkTtysW8NourfrLGhk4MAEYc6KeecLDutMYfgyzk3b/dxzQl8SR1Emtf8l5+f0u
1Ty9s9k9tCxZFz2OTOnxqx9SL8+enR50PsbHiecTBlaVs6TpbLMOOBfh9mayBDxgp3f1z8QTDJI5
eA0Yf9PKA0G1AK2oZhDGUJROtqRugH9QJpiVpv2ksUcH54fXPxgAmXYRkDDwB0qmJWqZRAbBZTwR
EWP2ezKkYIcXXJK8N6MEtSADNk7R31UWL5jbpFPwMWiYwQfaV2Dy3mgZmq0hHN33Ft6w01oSzBJL
rZifoLRF5eRybbDttY9mbL3C0pKkmmT9CghG6GRCQaa9yfFYr5UhItPI08aIeasoPfv2Xle5zIIm
gb/AlVjF34P0E8Wq496U0gYCSdtp2YHsDo9ej87+FAYXNycVUw3sK75Om9idmwWqq7KXqRZAfRYY
IIgTedWOJ37P26BwrrSJdXHHIYbyN4nVumXFgBCXBy16ciM1qQpMHQs6Bj3OyrFtx+54UttK1pZL
8gPRLtwYgsjqLuijyLGdsg23B8b1WxXF8ZKxceJkipyYH5VZD1catJi27jgDzbdjzEpFimRAl41j
bhCiE7qQBBV0vJslbAqZO9XFvvJjmiB3qDWh/zFM1tf8LZBxWXdSAaXcOp3Q0s5m7evHnH43O3ql
eYQIwP7mmtw5YtsceOxAUpFWL0Vf737Ao4exYOP32+Kv2t3p0PZr3uBN+A7URmvADTXl3TFYQP2A
MOQuSIVBdm3vUAjeNAQ9FJEAnlWYycFxInkXsYGXrtGOpaA/RefhENiCbqQLLPKBk0um5om+H+SA
cL2CJ+h7PCrzDm1EloEf2ZDDiaHbM2tWs1Csg2oyNgfDcByKnBaZ7G8KdtOLhwS7tu3G6KOLeaYe
hzt4oKYIDd8i6tKBIC3XeTutR8db6AWKW5P4ibzUzx3p9tlxhAeSkwuLAVWsJPVhW9me0dc6T0yn
4aqsYRfPlhtQ5T3Wfaf3qIZnQhoV1RZDKdOksMMbHyP2xWJjVc371/L/pDxSkJBmL4ml3DcKskHA
HywEt6ZLiysErkx9hY7VP8XfqVTEyPb6JvVe1NK69IBvnQVymkKPvvBe8lQaleuHcZ22gM4JgU3g
xw/fj3UU47btIzNX94XsJfdL+9EUV/+TEmmBATEWsIZ7sWRTJelPdCr5DZI2Jv21Hl9t5BCOCkNi
OK/loMm7D8maibMz3Gt/t+Y1K6moqF0PCLaGRzQMZTuvDXWCwXuXosILI4Hkdty3evICr/8fXl3K
706pdkH8W2M67SQoOHiU1pc2YpqfyqBqN+7xRELl0qAywCqNoGmL1oXxXMWlLDbVtVZBpE5g4a4q
ZMKcKgCEMdv4ZBl99skP3T33HYy84qMrNL8fTg6b2firti5u3hdc7XjqlShSq5fWAkNyHns/CbJy
z9ebTgNxIrrD5Kl1a3cqdg3IFIj0qKh5L/QzRhRJLz0ic/ZTUUlEnBTnRo9QYRoY8pz0F5JODqps
mCqSE/P7b+0yHJ6M9zK0j+ztctkuX+d8tktQVdUNr5GlzVduRRCSmSYF9j9aYywTaDJ5Ea3ZUDRT
U+rn74c4UN5AVWHuAo/u1i92TzVdKAFuHbFhxK2uSoQYIB5XmwKhFbQKL9DZ7vg8Ng0T+/37QvGu
irbgzJFaKQyRsa91MyChHRbuo33ufJrFC1c4VgLstLLoGobATtN6TXP0copU1z6yDgaJjhxyaXF2
b/rYjAV/f0+XSSrqFI7RT7JWrVzkWfpBWOk+HjiVpusGj2HIQ1L4d0GAUkxMYMOYIOXCm9E16JBt
YDYT3khSZGH61AbjS0o3RMU8GA3s+JOL9aPfVxiT79OnzW7O1mCu3Kd1VQUTUf1IQASnI7GnvCjG
JEkFufCjw/fpRvb+wLBurbtKXGXN6SZe+E6Xa8oO7SeEZB98udYJpU/ALMc1ynFfRnfPY94IqxBH
Qdybko09ihAui8R3c9Grw4XgjL2wDrY782021LFCIAYUluZ0K9lhZXXfq3ArwUaJm5Ce1cEn9yum
ajkEy3fOWMkqyV2OO0v3vnRCRoJDHx6Y3b2AInqb8BFrMvn/feZNGrl9FQFoBeBJsQcQR6CQmmfY
ezsLus2DQ2++rvtbiEXDHe1qGAKL2qoSzfml4t2rrUqrzUmeVdn+yGFnYYIiHD7aVL4o7UPLokh9
NUNAIfUuBWwiq/n01ARHPoZETwWiOAc4YNdABwy9IqvYK+L2EDEzlkzGwDhFlhj1afUo2S52Tis8
O1VgPzjdPkHA725yjudENvP7p3lX5Hlu1LaiRWgP92trsIKYWi2skHD+KlRh+vqXhddft9vqOWus
+aLBYoDGEFrjB2M678JsRIwlyd5yAThkmsfOgmSiAqFQuf3WLWHoDn6oX4yJKDZF1Hh+zMVADule
U42nZadP9xJ95BWKwK/nKvcFn+vKfVikBfh67PriUomo18Q2XZFXCOFCHRVfs5JDx2sKY+N91zMm
+UvBzQiRKqA5BzKFRi8AKN52TyMvIsw4rLVzGUYqBJD0Yj5RGQuykDTzqfTDajDeVCp1ObwvbVsl
A4UDR2q4DSmKcDgydlADo7uq8ptNCld2qMoxipvLnY3FkfFXYbAX1L1f7te+3ouA0cKYXfja4YYi
MiCxmroBKK2KIheW7c8BTzeM7da6YLNZLbTSwJ0b1+/tD3qVy3Tj8cOgIJAtZkI1bE6tRwke6xIs
DU4K4/mgCXL7T0E7uhIgeLJdigYJtmcZw/GfuWstLAF2t8Kr2m/7ggGpODuqq/Nr66DEowZhHhfM
qFeIdr3+HgJXcbGj3CUr2aUPHUuCjXtAbHD19VIPiJ5XO8+Wzyi0JJ5P4ljZDwh/f2TIrgqNXr80
nCnpEVOlymZ7PJwDM7a1EVaxWzia/sDZGevht5ir98nQ18bAcXyF0D9uBRtOrX+spE3OmNJVdCzt
bYCNnnU+5oTUBb7r4zLwzqTZ9VK5T3oIaDF1gJT4UIRnC4sy1Bx5nlFqu6uT8nJsNRXF06cTOmWV
Z5M2/H2/3UDpwsq9QlWuIGpiddOZf106oE8nzmpqZ5aQ0M0q3erTKGFDH40+0GFanPftW1j/P6hq
3Bq6JDDrL7Ct1iZFjhNNm/MH4Ga18PAwXlPGEkyzSH95jSuZlOG3m9kt722r6mFNhC9yoRO4d5bp
3XaEezbAEjAKYKO9GEdJDVw1y01wAbB+ldLWiCp5Xn1fwgH7wi5wEqBrV/MZkdC6DypLesa8FV3Y
NsTfLGQYf7UtKUIKZCbTT/TL6mvOjF+TN7o1j2uIeqcFafap8mOwLrcizCk6sBZMLadjOX7lUKEj
AHH7bUqKG/D42xZS1E+dKWizpWpiomUgX92N/sOxRheIll0jPmkKVb5fFJVRGfcosB6mV/rQJ17e
x2SGsRTwW5r13X5xFbD/+TGpMiJhhR34pZQmG7TXeYZwjHaCYFrguTgFtN+ZDq7DgPbejcwp/Z7u
vxsyxCaYme5OY1ywJUkRXqKTAB23zHDpSSZQ8ca8O3Mda0URUEzZRNDPfQpRWe/krv/HWHIYKzFC
+0U2iSajomJKcY+XnQ2YJiXU3lxDac1uHQ5xBKVUgYO4wU7uorxqq8Hs8VKdo+ghy/+cwoN4wNye
UEhuI7zT9/owP6C3/tzigc2wCelKiQw68HlV88JYrd4IlTz3vtxADIqNUjnJlc+585Gvq4WoDqOp
YHUfSevFOpOQqZjnDXqcyiH+zUO3WPv0Wl7RmrsgGp9hNvA6ow9dE8uYj0kKtt0d8D2nc0wdYt4/
5X/ZtY6GicVNOntDVEYCK6A+23cj0N/tvyY2XYcMyWdozGyZo4K7AFhjfn0Xcn4XqyST1QOnJohv
ks1w3ojzm6HujYY5EnAgAAF0ezqg19ao2ZS4WvClDOUHxblJKU77MJjlUQP2PtHh8qilU9K8IKlO
oOdR0EMM+r8cbquodTSEwuLN/E0unNVW0CHlU8HuP/ThTisnAC2fXwPzIXGjodCISYc4ci2Tipj5
DOOH+Q0w54h11cvhThaAClgawwi9/F2TtTgsoxODBk0CLPpFlTR6uW39RlryugB7SnK54aLkNquY
oGLcgtD7W96MdE2SKJ8I+pGfkA5WydNerX+no/Rqw3JiKVK7RHA6Yi7QySC5a1iV9nOIV7Ys1AOw
dOMl2DjOwrlSyUTol7yeQ5A8LHzhGKX06sTL0V1PQYNIpuQ/2OHTOKY4S4LaTNV9G9xXEnys1AUq
s0xNqE0dbXEO+FV5tQaR8jiVl7Xqg2f67dJR3WfPfRi91XckXm3yMXxU5zE/F4LpOnw38Qz/wS9m
wO8NNMzR72UJ3lN4jOiQFdgyFMjnbI8lF8kHQc80FfxmazU3aIr+jnQIMLV9E/dYUnY62PIQr5az
Tcf7yj3lj1mH4v3hADzAmitbFGY6Ar/kgYgCrIYf4QsFcufqLDaeX5xaRrQmucw1Nr1s5IvMz6E0
7fZ6PuodDslib8S8y1Bx2mXOROLxkSI7S/bRpp7zKZOvfR7Y5Ah2QHeUSCazYfaJk46LgpdDM9jl
IIcpOWgV0EI6qTVYQNZ8JgRq+ie1G8j/6Xu+MGkHrGKgF3wKmA+nvsuAQ/fRWR4O82+j7iPWk8qy
KNS2lSbMnZetluoraVf37YwN9zIT6vWMgyPbIb1pNFiM3ClZX89jEqvQl6OQmq13fKpBDdUaUY98
qBVz0RCmCLy7btCt9KMmaplbJ1a46zIAy9hbzQpLeDldN1JK8JDxD8q7BIu0rOK2vxa4uOuVASk9
/jyDwpdlI+6woGytoxQmixgwLHwcya/KATCUZixAHNA0sAnRPD6GmTb1PINSsBg1NEsX+DdOA6cs
mvZXvCHa8g+z84/7b223BFTuopOtDxdNOIt45eMxVpCbvtcFdbVVb/A0M1assvxx9/BZAf0W4UNj
Sr/CQcghwTK/gAvLtc7c6TYWU8lIpjoXoThTVpzDg3YkdEjZmW9B0wxVPbbTpkvnj+kX0k56Tm60
p3cnV6SxtbKrO94s4GisZwnmdFZ1XqWaa+EWh0Z56mZ6F3SIpfnhObaGYOoScQtSLvvP0N9Fz7mP
E6pXdyiwAKYDah7y1VEEPOjaTB0DGAALJmnzRXLiBaAmOHQflvQhJziW/vdrnj0PZzWq9qT/318A
+fXao1X/iLDzyoC4QJkL8yq9qhhFDPja7bYQm6ztS4lLJfwN5KQfVTV3WnLjNjZsy1CAeoCBRaxl
t2N0R5AxduqF51KS6PZ4j9CDSQCqgMDhXfAxEEPZAZ+oklNRBNeC/CwaCNdgE2e68urYT0NiefWB
3u4KJbP2pkLKDI4uUmFq3dQs9FaG+DmHtbotKqfUGcgQh44G+ddPAJXfunPvIZ+Q024i7iCo4QYW
iZXJ7TUFPPi+GltE6Y6k9r/Um2gjyx/x3fcPQSoSMcf+Q5kt5QmGkAejZtbCjXlUn0vSBW/X782m
at8xXeto5Vw574lBqA6sYtO1zM4aLETI3CBwoG/cXl4NuIdG1SEIFjNIob44ypPUyAmOTHNhyu1b
6Z/sf5ugSG0cv9SBRYtc34dvVOeLTY4tCNv1Zw71i0gu7HtVv3roRxVt+F0PRPeOmd7Q+z+B/u5N
NXcLVc6Not2/0vi3IOeMKffNdxLmYaY3exBRYoPf9RP+iiSN11IEzoeREecn99xefLjj7/g4WYoA
wfJr8M26S8urK12nM3rWNjV/zZ2CBRRQpqmBIRR9Uv4IgxXvRXTFoEuY8PebefvvpDWIxCDHfjEU
4Rvo3duS34eWR7Vt/m0mgYz1BAf/JnmQOvKKP5+VKuJQBnRmQTUoq0kMKcyV8/PdhAlkiBt6hKt/
tq/0TDOYdCf9XZvg1N2qV3xVl6X/0mJ5jwjmsCpe5kgYgzR9dUXTwi+uwsAikITMfA/CN9CxevLV
Dr0Ziz8IwwWpMva1rP9vO5uNHUDE5xRuhE/bT6xCD6+YNAmVSyoNtTkfk20UtXIgOdCTBSg+PMum
JKZc1NmEBWW0bN+IyzMICpOtDqnIDqZ+K6Wncg88w2TIZ3GVLInoEuBjEpVP+hiImNyJ7Hoxi19w
FgAQGJ83phtuG6oKEX/x6qPL9Lk8UpystbiUm5XOiSQj7RzGNdTYl3MoLbvvWQ8FkoaU0Voypu49
lQ5qVZltEsRJ2wDILw7cUaB2hiG1Ag9mdCWd9ozSeu6VZh6IWiW2Y0u5x81CFz/fXBYhZEANiOMk
5QBivRPwOYyxVqKbAZPlFez3zQTuSCK9eRGlLRLHHv2bnhIm9eHgml0i/v8KzTr4ugSD30lGLfDC
RoJJmd/TF49wNNy0FMomLLvUiClCPPzGfiGsetMY+FuKlgjzLmDKSZ/N3fBhtW7tZV1HZHNM3JvI
EWD/P8I/yiOQvp1K2cwWHM2QlER65Bn/mRSgyAxQ52ICZZw4zdAjwrNoxvF2aHJ48HomD8UclOb7
0/kbgsFGlMPOY+ulAPBRQ2GTOW5OnM3vXsy7USNMsYSIE7Gc5rnK5nQERkzKzeGMnZig56vvikiQ
rF1Xe8EQHgvnCuVeWTjYiwk+0ctT1rwQx81wUqC2wBMyklAzFzRFt3S7q8KteOaFNa7OXzls/P9h
P8us1/Vqlg6aH+mjrozMx7q8LwILlcU5g7UoDX5lJe93pxsPMOR90x5ygPgXVdnQ1Tx9RNP6OZsL
FmLZBydphCH4XRBGSeSxVzmc8CRPiqhExTF8lCUet9z429Xj7p5uSbqQmgMPEr05lB7wBw9MZiCi
SmidnQn/mWWSBbgyAPEQAe9orgALHBM3d+4ZzVLXGzA6XGM6qz3Apuu1t55Qc/edGeuWnpxonJYd
iIeOtLTtm0imciE/hfLjY6vAKHoPAET10TGHG8DZmLBcwuYZUy4cJBiWMuyDHhxa11r6Gn/1nY/Z
bWI71/QrMpMDS7S1X49OytUnXK2NcZB6mn+d6ioYd330bEXbyBFMmwmcZIvI5D4Fa2ifBgFWZt4l
L63eyf64DCcWqkkpK7ta53setX21BDLdwZ3Qtb3rF+H6/lH1/rd7Ds+Nn5aXBmTUHZQVURFoBF0p
ROEvSUIt5fprKiG+epiKokE8z77112+yrHTqiySUJVC1OiOqlmUtOKxrjo7jAqn7j+aTP3ytWSYV
rGf0wdTXP5j+gY/7Uzl3I2cXFz2jckPWCEMfILmXCQiA8DG4F0wJAs2OuLycM2bxfBAu9fVc7K4A
zc+hpkB2B/0KNssIXD0DHZwdDVzfIB8B2nKRPakFC8nQWgl/623t3cWa+ke7aZRiPOGZD3Ge29eZ
ARs2VBOkEcK8nYCRTQ3/4/IZFj6x/yTbojBT5A2XC+NJ6sFJ
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
