// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Thu Sep  3 23:52:56 2020
// Host        : T460p running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_800_sim_netlist.v
// Design      : c_counter_binary_800
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_800,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [9:0]Q;

  wire CLK;
  wire [9:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1100011111" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1100011111" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1100011111" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1100011111" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "10" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [9:0]L;
  output THRESH0;
  output [9:0]Q;

  wire CLK;
  wire [9:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1100011111" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1100011111" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BBEPTt/N3uzdT/V0BlyPmwVFAF+rzrcnd0soK1RyfN9bAd94r5zHXB3Kn7tBZrjdO96SMajBFnFe
qwJW6O6qPDuPX9sBisME/p5tEJhUbDSalFJ9EtPHR+hboK8/BGgWp/ru5lEeHEo9gLZioz8drCq+
3i4/Ps+SaVWzXrhaAqA+g96OM8sGVXl3pgBqTWfKMV+DlRqT8THcc6Ud0TWw7tSDpglPBOY1cZ5X
gAC/c9PAuwKOyZftnK7QMUHSWCcIaLnUfS2MT9aATI2dOwHGITnvUw3zaz8sKW1REAM4lUOyefIb
O+i5zeLRtLTEons+0gT4PKC9AERm/e64/BkNkA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
x4hotiLL66UhZBXArq/JyWiK3MkWjftGsBG9c6k8k1fD2XEiVrhDFhZWfDkZKDFtcAXJPv3Y2f03
IEDW6QlFsieCxHgnzbfZfc/2ea5jaJzH36U4ZHO0T1ruIkDxtnwXzywSJS9B2YHerlCdubftH/LK
MEl11j58NFfHLi0bfTgE8EhstXVC9ld6d76nfZfPeNoiiPMRVJWiJiAvml7Vt/Q3UpbvjQDQ4zgd
HjlEcY07NF1OYR7SVlHtclmktHISfrRemVmkb/CHG4yV8hQnvTBAe/Y590u/c8jFmBUEa4QPtwE3
0b5JqZYtIQdMEasYgt5/jNClHReqBvW3daOLuQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 8224)
`pragma protect data_block
1cADbafyyb8aKZizbGPy+7mMbOLXxBeltjdHJKAIpJCw7F9VfeQseqF7gn4L6CuPoMCeVQmP0Vjo
oO9hz5VRM9lPAIPqgqVyVgwCR6hKeGNpJj7Nek9hVWtwlmuL+CebB7WCYES7I6/xonrS4oCX3kEv
GcRGZxKrHf4oeNai+5RnfbQoyIS5GgXQEtIPfHJxJVSuNRNlpkZ+r8um80cuxO1xDQ8tl+zqDEvS
J9XPYwhP5OT7qu1XmPbOSHljtCgsUiP54b53oBlexJRTGUxN+IcH7MXTfZKiXAuQbnBKzUkCd6Qw
GBjrsJxDmsUB1C4k6LuRRGjALedopMBsatN14uvq94B5vlpDK+4ItsXKrgOZ7IioGEo4mx0UnK0d
L0dj0B+yVX+nIT9NpHzhtPSzugPxUb+mMRXg7Qwds5ItnbondFSfmeuAzRG04RTC7TV4E7g6iXzT
1xcHkOiQIUGGq4mmVC2ScwMv6cszPBHhvrWmuoDeKerSpTLo3V9xj765h1mr1Op8A7zzrlt+xe+z
E+u1D10+ls5xxmxFn/VsNvh13dkdfZTLeUzxG0l9AnostFe1ImdKdMrjQ/eM68jwphNS64eDx/gk
gPswgcRCJ/9Hj17+6nfnsJfhZxl5cT1Hgp8BYKShRRhLUrBte9PmJFvcm7gGBXp7V0sJ/kC56mZ+
rDOHHw/YLVdYJAU/znu+MklGOb9ItJ0PekN9xJoMDDIrhP39p2U1+267B2MzuOsCOGvQPHdNfT5h
YpBb8FePoZMdVBNzEAFK9s6KPfJ78J37EBjJaYR5QDTvBPzn1uXCmq1znI/goXbHSv75jJjCS43D
WZwVCKz7JDKk86Ac1q29cnC0fxtYK17n6DDDSeYf2CD1KNILpjqIeqkn80Eo+g9JDKpn6sOx6RJt
2YJEsRflrL0ZSmzuXJrR+mDwgYuu3AFyMS5lUbE3zgA0nJn6PRjoaLY1icS+xFhzsYbpgnYMM5nq
QFJBtF4DkI+7y1JseSWlm6FD0/qoMPdf3Vqy1J1PN9WBSxnr479n2crxaw3AJo0wB5tfUDBRTg1o
jZtn7P2oVdKxkquhsUPLT+Po/RSVQ2i0RQF8Wd4ZCzsiD7DHrFHgFMUmgvQaYRCHCxru46UmH+hD
WkxXpxH2LdLO6QYCpicrsK7yfeU8IF5ETYz+ALVc6J2683saijxu/ipT7u/htZp/G6Ccy+bAxGYk
czPn7UH+MZ7RbPOo78MLy1Au/ePIy8G1PcWH/g4tKCw/3/xATl2i92IQjEqryRafRCV7Y5w1BGD0
HGiHQnsW1v84XkdvxzoTb35pI+hP9RuD/W0Rl0wEDfah9ArOLJiAbRP21vdCOQPHiTd5jTAbBrcy
EuQILIgmBs7GlhGQT8A6YP1e7r5A9ZCDSS+5883ucKZV3cid83nbOOfx/DaFAsaO6hpatW9tfB+z
lzj0/CbM8gZud3+ZBycPk93R2vPJigUploUVE3MMpdq3iYPtYGyEQVu/+YujbaGbw5aPeeTeoiwG
2csBf0F9kbG2eDxrDIXglB8RilJ0sc7TdyOu8gTQ1WiOMHqejWmUjzuzUpjTU3aSTvPG22k1TXAU
aEWDUadQCDZDIHrk8yu060RpggMHM+kOboETI/LHZZ0PMbP0L/lejxJLEmrhXB1wFc1oOqKlHBYb
A4w54SMMT0PhfqY7QcWkkMaPi8qhGcLXuAX3ZxnJl30NTHIXyxk6QjsUdpSAH/ofcNWE4SnpO8Kb
S6DDIYojckSDBANxbQ6pwpImXqyYN9GIHNJj172th96SYQ5kPJwdCMnru15nZzO+btbQdEzs5yhm
tsyRjQmud3Sg8fkrkBYHArqWB+uDoNJy9Ad08cwUadUG11w6XKyVz04O5pPoedcik5h6PHcdFj5f
1GYfQv+PB9uS+Hs+SuIrqXnSyoR0YYIkNL5ZRxo813b+C1FlvIKfqBzE4NhOgD1R97GkMJXS71ra
0dIgcrZqgVB9sLRsiFwl1xc8hxqxy8ofbYIyIAgpCcl8FdL+umnCPEuXJhJUc6VU4WTl+QuUFMFs
VC3yRF0TLg3hJ98u3pFAuc0EZkNWzg7YjjFmGuDURKd4ffthadsrpgFgD9mSxx+edUByMMh+M7nr
eIE+pMZyCq3pxYR1bLTEISGG7GC5fEtJrSl89bkWYUDtchSWVvXvULSLQsWfAhuR90SByfkFyG33
qZnwCUPrEV5Ag0VKyB6nmAkB+CgXBmUGg1RUuIiIxu4wpnF26+adM98K2dt7qxje3BW3jWr5v4YR
vK75XOvx7+28upFefc67YDCpODVWlFdgjfiTlFgjq1pS7cXb7/ts5ucHR/jr0Tmk13VfnhaZGqy9
orLQLU+K0oxC+4/4c2iVhCVshNaKVLHe5fu6sk/v63K8RIej9t/E+LF73/02Bm7vWIAQH/4bp/AT
ehoTlsftssubURQcAsx/ILtbrIkUmpppdq8ua4gG6WJmMmIgCoFy1MD/QGmNUHbjIqQrus1Cet9V
yTi9LDcWBIbF9qdDK7dvb3Bp5XOZ5sz4xi8R1uk8cuby+u2K9qn3fbGSYU+GOIu9fUDUX0Iawc93
IxU2pnGi96xuwYqOjDARaKnngVaO8n9xkljJDimDXV2CD6Ly+PeEFwZUFHGuYHcZrb67opqoreJ9
VrEJSK3zrsLElHdK4BjqwKKRTdu6gHv9GDM6qZM53di45rkjV7j5P8/ZDSd2oFQ55eR79UWc0z5A
OcrgUAvMUvN8xKMyj+PfM9tQIWe39lWfR7Wuua/BaATI0JOYhhzyBp1KAgdYEVKNh1GH8qsfGpMm
rmq0NOk1FBoMM9QZwEraO1UXKoEreiMv7j15hejhRc6lFIITsir7j1kPSIPIIztljiEDEc+yS39P
x41rjdbHObJh+LdoTKCXQ3y3FC9u3QKQCDpcqTEXix7eiy5pEDjkbtCGE/JQOC0NXlpTdYbF5y4G
8z7Y5Sva4JxQ+q0+3qTBlFGkclgOxO9SAS7ZJRPYqSJ5yQof+6rJp7jxUYeziEEd9aRL75kSQOGX
TdcfhjUjgzmAD4vcKG6EACTOhtxMRFFCASalEm5YREMQhHXkCDmFNOAh2U3pTbdTPaD63c/BZMah
PIcBIuiegoehP5UbiIjuHYIc3TLhaMLX2WoNgKaAwRfozMBc/GRHzxRwCKMjEhoEu9hsZW+qdihx
SUFT7L/OPvFfhn7J1aEy1OginabsUtdnpXaSwIeFIGWJGlx5l2xGgPAtuGN4IcSjNRHLzBZBHOoE
5Z3/1BszS9qSsIS3chdbEIZHgV42EDRf4ZZscG0XaHu5uWJhkQcsfiFcPy8WxmGyodFEZPIwMkKd
n28igrRdmxS7OCbVN5Hy4XGMSSvswHv80fYDQO1NxuvB+FfsZFzze0HSq5hFy6pIt1brWAtu604G
+pvbkf1Esd78kgLTCCthEP1JDAL2gJ4RF5r7cErkE+TsdoDy9SI8aXEy9s6YgqN0WM+dzqO1feeU
QxmlqfDqq++DJZZtqXWWjSYVocQSGRa2ZmMScCx0z83UU9LftYWutx2sldlBwX5v7p4dj85Sl8Ey
c0BKNsQRYEqb30Q1McRxo97tj0/5VJTdz2duBhbHd04PWIqZIHEfQnSBDIAWbLBIr+NuqpT71bXL
YeLT/Q5GO0i6afMDRHlbG+eYV4JIjID24sK5TS6g8h3vQx4sYrmk0P+gnrpyxJDYD1v/WrNkQzDC
QdRC3cjVqXzPo/fZwjYZDdsX+qlFSCpqoZ4e4TMrmR9yq7Chh8XVWpWY4HCfIg38MvXHu/V3E0W3
IVCeEvfvTiPZUb8bpUjOQiwZdClDWaMwtPTpVQpC42Vq2kVYO/Ijt9XUXfZsLgNbwe8KWZ+agTxU
F5ii0pIM4BxGn6QvYhFC/dNj0A/WJo2q/Y4zvmMkY4esFSZAi4Zgyw4TLUPqjQxTksZYmARU/ncL
skc+re/t3URdeonhY/ovi/HXUrRfPiDD5UwwAmJmmmnpgncVQN3sEBpLlNyR5MVhio4dewanGwu+
tJYDQldFa6t7e7Z3NYlD7I0VjWTr5xkTuUDdiLx+fyQ/4KIp08c+QQ/vNRguVpxI0wpnEJvvncpY
lCumwhMiOEc0Sny6QsjqhYrs7dHug+qLwrcdxEUhtoDoedL6/LD2soupM6MMGpWu7lzXcDPKRU7c
M0NFWxzu2yc8n2Mp9HdALTlyk8Ep8RTc+zBU5AgeS9sWm/RoBFFqxyzBFoP/hrfneYwrxywPr/PK
7sfLDA0tN/qzKHvTiGswHGLj+0DryzVJLz+ORVK20xz41iqtI/blKDBY32KW3C8O8+fqCBXxQZN/
jcmIDZ3be0LzDUfLZSHrfJ6EE4d6W/laTPq+1p5FllRDqQwx8go+nCEKXlKThozwXOk8hEfejHyN
XbTAVFtsTwWvNDtzXdeq5oPKO+AlKNp8lNipi9b+2mZvDKK/sPjA/NYtqAL/LFgMobE0ryFh+lyV
labiYL3S0BvtVItbSQQwFUcbbdD6cnu43fwxc49ImwMgl/7KA6ufo+IvffHs+Iq2Jdar+3RSIGri
bXO6igz+pLSTputMItwm2R+YQfg+5cZvmw89g+GltPn7rCy37RV/2OIyplqxyr2BG7d190/J2CcC
eh9WspbDOEdUFcJJ0CjZTiTBCMTWwOqR/Wu4XJz1Oi/HxpId6niW+6ljwGVNVkmf30H5g6SRFsR8
w/t7eZ2fjFZpRn83Lwky8S356YSI8a6bxVWdx5GpGyzxF+wZIdh6cMzk/a6xbOoCAnPamLXJIKGo
tB/TqDcovng6WsKlXXcKxmkkjNQLt3GgC7IJuaQnF+/My33KDBYRw6k1V2R039B9p4TqQX9fvznh
5XZ7RGfpqvW9x9dXbuoI8bsDfuTim6xu6lcKWghO3d+CsntVU4PUp3XGShYUy/5x/npx/HQJt3ph
M88qm4qsXbaMJjMmlryjCvro680uPU0DauE9z1O3aoh9hDdJc7N/RpAhDsXm0MSdwnjiN7/oK+J8
lFqrI/Ii8AfUQAHSKwzhBYWWF5ea0zCmxlVlPj6w+gXv0a0x60zAE/h2Zj4+06I1DR6PJcX0zc1I
1Jn3ewD1EaTFWe5DmlqmIovCwZW0PHCxt8VLG1gqEEr+pWxTAobZ1+IosvcHIIlKsh1L/DTAUs8X
QkTQ6is0116Z4EfVKszmcafbSQd19j/4UUaSQsCtUIr5eXvWsyUskVYEBscWgWKR4YD2iPKHSzPA
6dFHZiUv6xbqTJd3lGxUKVVTCFBYY9f2Jeu0JAL+UA4zputP2qY6n4IFSTS0Z+rlrnzUOcZowPzB
3KG2HBRfX4qzX1u6FTI7DiWbdsKYcDKXIkNy8YH9fk0vgBmXDSrO6Y9XIUX9yr4Bsma9IehAj+jl
GPAy1fCdoZ5yjug/Qpsf6LJ+m1YRrdOcCVjaZ1O5PipUT5eDPiXi37WtVUJlSMD9TsVfOdpf2EyO
Rqm2C+efC1HsV/63H6iFMbUi6IKo1IGnilTWH5lCmAEPzjtV0kx9Y6YT+92YGJFc8LjnIzqEY37Y
Xx3vXPXsW6+oKAfUCDJq4qiEQ8sjZp3jmYMJUZiGXeWThfeLzVm0DkVCs5X05mh7DI1mX6P6pKZy
3HLg9YaWyTHeycf7rCP67Al+Xw5PoGMiWuSP3hVOC381D31mHFHQHUFOckfjvfyFetZvcM+52Fqc
lcdol4rR+52xTeF1YDOwydxZkVdSfL7ONNm3mDLMGzkDfmJaTvvPvnO05cb4j2nV03WFdp1iEAHP
hL15Sg9O5r/0npTJruB3oc/VexUOHWjWIl1Z0vWRQyLOpHH+DILC6GFn7BnLFy3aQtaHQjnWU53L
sMfOIm5Zwhk6E11vVaWSIpzwWlnKJSMdArpY/Ym4eqPoioMRhjFYS4P28TGoIN0mLTknC59vp6L8
BKeQg2l4JuxZ7kjqQPyb47xCzj4m+uNjoVMjOuBJKsTJfU+2VUaS8Gc3KAVa43aSiM4f+CmgtNBc
4StSCA1AkJj6WKDQ3A7Vud3VioR42KcOILQLYeuKc5SQ6u2KxM53G/IKuxFVl2QWN5nSvMjQLGSi
wwW88flHcLrxN3+GRVlzBgvGeNruxuuryxlmVj8F1Yd7BWeOF7uIz5zENa//2RfwBTAJnMNOw2tT
TlDSnUtdSnRA3CkgSq2JevrnwVWRUeNtxMSDZkXKXr+oZI7zmy0OQvV5T6rZDq0iiM2P2OhmN85E
z5825go9Ubb0HRi0oy6l6eBCD2knv7/59vuFicEHWJIhM7kdm73bDPdXJGFNn2VElxOHpZcCAGgv
fNJ+qOe+GmPPhGQ/QneKAEZm6CBv25j3MvrffeQvchuMu2N4ajRUEmnae+kXNVredRMra9bk8bjx
jRbBduwGH+ufqp77S6QpD6OLj0VmQ2+L4ACUioNV2WCoV9euoQArFWVefZctgqDoFU9Ju/cEHnH4
yHCYoliamIk3sqM8yyx6OHe4EaEQPKKK4D+nDZ6Q9TJEz2zcy/VKEvwI0ENJqOX6SdNpqgak57MW
LUdM+0xCpmb3IcOvJs+uEmyh+Xt/jlNA/RHiSmqk5zK0XLV+xxAqdGFaQj5Z4gFso77gvOG6TwtL
DYZnWgwKlkLd8ww9MYy4/vlX6g0uhoC9l7X92frLE8JqRXPx7qMFhRrmTBVy7XYX0cqnBfJkX/ih
tuuLl00ZttQItvN7BdQXrtRDWKJxiyjmuPoh01RoMrBCg73OBrE6irDhYkl/cRGdmiqEaSbcEa9i
7Ew+lzmfJI9D7VPCtYCiZkz0dZX4dbDOOINAJWL6oLx2YAfIyuqZZTS5+dGvegivaP09b8zypwlY
0zdsfxzo1CZ4HcZz2sxgSiaGJbn8GBKxZScgbz/o6lALraoz/g/xZQ4xlioJz15HjgmvG92u7iot
GXNFspCNizyHWc9cnmJJApwbnVk9FgaqInTm5Q0pumEfdEsV52nImou4OU8xYMi+xBKnOVDPmk8O
CEHnCj5t6vAC4CCBcyiKschyrjwkXoGJuXDmPpuaI6fz4RJzx+YpryQ+c69V701arXWagEbN0Xh1
BDdK0AYd7+tl9UVo1lZH0qB3VJ2vyEj3R4GNzuk7UAgi4H8EPEZgaQ7+R97ivrpjmch3auzsmy79
FqHuPAC31G0EjgRX+FyiJCHELjkYudDCv9xXnNG3rRN9R03k3R+pSzzIYTIz4v77qAmd0tWrgHXE
SVfd9VHycsRg1xT36D9xnZ0RSSanbpLpYfgBRrKOl3UW6B5CU5KSTrfE+CmsO/kgvcr1Qh3A2XF3
dMzxs/IZyQ8JSvRb2HYOtxpKQ2NusivGkFFAW+YM2IpuvvMcx9dD6O79Bd6cVmdraGKUgsSw3fAd
X+/daPqcPZ7Yjr3my0G+BDaIzsaIO8aKrLOacw6QALPieA7mNA5ULpraWex0m/se7WB2dmTfvExO
cO/8KKICvmjh4I4kBi2Wn+PNKpI9UqrgjZkPGcRddoyYiAjY6AZHJZrSBxVIqwSBFHEMTNqqieO4
jMINupVvhPhBT5THZWiU3oJppRK8wAOVMvHzxLu/MDykXrHGNq/8uRZMnNOafIBWY4TNBYy9ghka
hMLeYmDejOmgesKv8oMLEzmMCHbwYfshIKuBmjzA6X2rJJQRTjks8wiFGRer7Rlv1WmH//FKP0Rr
9VYPETUqvKVGzsETWTRB9PyuP1EZaAjEpOTzbOoMjeDeI7ULlBJfq/m+Y5392jIB7GlAjeO/mM1G
r3osYy4MC1U3RK3ACcalbap5q/bh5MAoXb77rjprRDIcF3I1yF3KNWUZZ4ZzyJuW8oIIaBBrCzXn
t0Ud9/x9h3SNsMWjyrbPwEyS5FXXUEF/hW/hh11tgqt7LdzPmpuWpo1cAEfHbdOP/OXSw5yta8qw
x+Uy3lZsFVhuiz9X+llAT1lbHQt5zv7u5H0otds2Ytq18peN+63e4udZzQwN98Jjn83p5dJEwxp5
tzKNY51q1L2CMubRWLJe7FshGUSlAhq9o6lZOlJX0xlPXkJ9/zsZtP9igVkJQXoEHFd5484qMQDL
zkmtuqjPkOQAAgr/3inRU9KyYndi+icxknKciudn7Ct98YuPwn6Za1DQM84GsjXQ0qWJ5W8plaeQ
6lNpK+gbobqIGPqFXbPq7dNVQRgNgoETUOOQwkoDLh6jK86zXLthwNUj9hCZRQ1w51L7I6IcLWz3
fgxS7rmbAO0ufwN/hrI3Yahod+omtxjGquQQLq1/l48CiRffR3dDM9C4KlKNv0rHrjvxqUI0Eh88
ut8xNqMHme15Pk3qp76gxNhfAmKWG0HngpoVde6ba/Rxn95G2rNhZ1lIQ3WC63E5OEhBT424XUKJ
4q7Q0hDOUWr6cEo/fdoqWVeOuTvhUjx7kpRdP8/ni2v7rK/GPpvi68ZJeGK9gzlWGzTuTMAuo4Oh
LUtYTsmB3/MM6N0FKbiPc+t0jhM2MdWy9hOUCyXNmWB1CeVorrMfywhvfKwFQ5sa06bcD3fcDwed
4aQgapaTFifZVstYXzEZxhTbtthr5g0rzaBree3JQ6oJq2fyqfeBcE7tFPX/6+pZPjwe3USgBZMo
JQtk52dx5MJg9GkzhA3KxS7nJvX3xFzlgQGRDsYhMq8eLIC2tlEok7VJU02iGXm309HD93iixLMz
DdeYClQpD9GblJXqMgWANZywOOsTXNH2VCKulEr9YzNovl+cpLGz+UoZ85L87f4LP8smRX/kaXUQ
mQFT6GvMu02V9fjPov3FanEmgKB4EPebGsqYgS+rZXNVIwbIrdWOt/ViJmfTr79JSiOCfPeZE5Kt
I+hNUv9FUhC4pQbA7x63CHR+oSuXnBMw3V4XMj3bdJrhZ3L6ITikdEIKlr0tGw612b7CsJIGDZh/
ABPcYrGeAXS/ywwp3gb0rvANyhHeEogFWokLL/crwdyxmOMe/llTecxizmbnQSxBXB+qFLXFOxfr
Cveo6f+FZoK5hbacfLK5XWNaSiavfD66ptgy9N5fEGN1EgyzsQ7V/pcP4Yntvsu7mTzIBeYYR0UV
4sG9VcljUbKmUNr9p+BqRBzb01w+LQAy8uD9r2qH68hFISAggJgfDwttuv+CdGVjjpR1OMG1vSh5
9K6aMvO7bxv1PcHz3UPistl1CYLmczpAzMap9uoEHei4BbTsuEvbgL4NYDyt0uCQIJgk3GZturCd
bSmT4oETE4XrMf+VtbqCzKrHd+0/DMsAotDWJ2XmaFEaA0vpei60mhKuD05shWGuBIA8JH0A3J0k
If8f9odo9yaoTHA5Xw1IRCp033lLeCjZCdOJmW8qlIQ38IaSYhGSBs+3SNyGE9rTd49ISHKpr0pd
QR3EvKeZjoUiOA5vGEVveI/GbhUE3jEsgNxfZ8OpYdSwMDmOU08lO7LuHYZebPMlQnurxQbJ9hLI
dF3E6LVYeKRHa2JOgmwCO2grLYpm8XSQtP1+PcLjaAQb1hlbHGKX2r9WzbSkntUTbffX3UQBAY5L
SYKLmeAHPu6dJBHkQAemxeb9P2/WZRpJ5xhdw9RSM4/y2K6v9omhgcvsmy7avWX04EHqGPrWhC51
MEvjhUPNsDVfFdXYlOsuesUDnHdxmh+YsWg1fmiZA7GE1Shz9cvtWoQEM2PeuEXzinCGN5+CpVLY
eBgC5bedbHK8Pg3lxlwNY26aVesyg31pU2CREkb9eviAhciCh6qlQ02clOui01WcQmdhcSc47KJ3
EZTD0ulzPFU2suxruGCuIa0Tx2JDd2K+2s0je0sZqPs7o5jCYEJm2xWbSjwTgbkRYrLKTrXm+4zy
IJRYIeq5HMEU2Kg48TthxwwEbe1dY98P9XzDKo1WjFd3R34JjRV2QeSTeRCdAt4eTfASEVXD0USP
iVP7zpS0jbEX+5YCPM81FDewYmSaAIptXx9VSDhkvQJQYixDGFgaG2ZdPf5Lmvr8HpdmPTl0AjMt
AxkmLatxK0Z8tbo0JSEor4b5hWpjvaJrSWYVFHKD7hlktCIO/PfES4Uzgz+Z50ivukH753AYSVie
SJUz02pjX4c4MMtb+KI6NkaI1+BE5lkEsGM9brYBS3okUhKCystF5YGwiwncxySsHS4lXhBmHh7n
HGWVgLjvj88MA7IDkaDUwdYVJsjsnSDldj/gZmXw0G8zz3YyxYBK9qmG+h3I80FT9L4KMCN8mFav
zpP0J4DGpXM1uNmzzbhdTXABKcvlaBy9uO0m3SWD9YBsx2A/w/o4MoBnP0a38dnI24BsxbCWsfo9
xnlxhfkR9ukVv/SziLJrlXtaiG/NuKz4ZXVkzicz2PoqxA0G4Fcwf7sCZfzTAtaZDXl/Qry+L8fI
Ax/B99DQnSnX2WMyQPFE82tpD0792c+F2Dlf95CYVXN1RroM5CDAcZmSNDjApvEXFSDmQIbJNTKl
UkkQ0BxK0nnk2Zu043CMuph+jZGvs7sSgrbzxdMiy7VMTwfUufAqav0wqtqO6G42v/N9xXAMUBWG
C3O4gzYKMmhwUMoAzhYNhoHSxFnfhfDWfSsm2PTnj10VdQpTw95nMpgQXbzhPxmNXmrcBCzlGS8J
HTs/OWjkPWBhdbK9vFjL9A/LcKB8Ee5WFy9vYpoX7bULssemCW8u1E17miqeRJN/tjZ38xHmfvIV
XsOh9PB26NMdyklqqmcgNCK6VYu3NN/e0ggqN5hK8h1ED1d5zgYGKPIUy7XJVWFGfTuAH0M7fndy
sWmSqDGFOFZtdOG1w7fGX2phAqNuLaNvkwsxg6CeP8Yb6yGwAfJrioA0xCyfND8+mbPM6OXnLhC5
0FPvty8Nisd0X/UDYOU/nORULS/Zb5/buaXwY2plTv6NOrJBP5hGHt+NnmxEJeDJ08wFbSUhtXQA
axbivQSroZGBBLvBsy87qIboDT947d/EB1zcEqFqPzdxT1uAnSZI3JIsyMOjpUeP9u34kzyH8PZt
HX/qznqDW5Kf9uSlyMbrjw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
