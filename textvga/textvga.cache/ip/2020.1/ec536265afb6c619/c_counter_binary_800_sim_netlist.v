// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Fri Sep  4 23:45:30 2020
// Host        : T460p running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_800_sim_netlist.v
// Design      : c_counter_binary_800
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_800,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [9:0]Q;

  wire CLK;
  wire [9:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1100011111" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1100011111" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1100011111" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1100011111" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "10" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [9:0]L;
  output THRESH0;
  output [9:0]Q;

  wire CLK;
  wire [9:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1100011111" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1100011111" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Zo2uqvCSzbpAZjNx9Osq95p+6ZO+Of6DyA268PmEMAGKZ1vJa7F+QKD+HcH+mi83l7BohF2XuE2/
D5Un96XNz1x1e1rLFqjot5DCWmJo0MWKxTCIIQHp7R09yryl2D3k6vd0DFpeWbWmBIerLh34nYhU
J3IC4cFeCvog9Ga81jVpuoa5iX0BKgsygDBut587ZlZDm0PNsMviOpOWRZZXTI/wGKUVhkZqSbx5
h+3niyEwf5xTrGHWkD6Y814axn8u4hnsLfTRN1jJJgIH0WjMmKAHMdhtQjq4kWMKateutXSRfhkv
uiWdX58On4i15DbHVfIkSz0hg96h+zvkapO1+g==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cM9DEOvMOzbYdAcrvaEWFahtVCE2dBRQBuLFIvvCU1OHcSIsrwrHfUReY6Q4glLy86FqPb9tZzJf
DwyQwrMlVNfo22QunF4ynzS0+o4meKx2g6gotHxTWN4uEwrlxNODCfdDWS0ew3VhIu68oFKfjwyW
a9TiJQTntuGn0jyoMqiTGZnLrCdOiFRLOr4nXsuA6xJlCYiih35RyUDOykHefMastvPLeQ9IhLqY
MRTkiUbUaBxZI8aXrR7OTie2/IbXxbljzDxJ2CIi/MydWRfTgDRvlOFBsTA0CQImwrXAruZfIP2t
7Iq2i0TaBBjR799oHyLa0w0kjMHZSBUBtcjhXg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 8224)
`pragma protect data_block
oEczpcn2e+JUHWYEfEIDVKMD3IuyeZeaCMMqJutXtR/TLh7HJWZ4eoCQPlFnEh9wi/OlP/7AR6w8
xNX7t7ZFHNjt44wLxucJn53UoUXYIDK2WN5RzsuFFkwYkKNVDJssbUF5KGElb1zFj8rYrzLotWUh
ZeWOEjAwHpA/uEjtF8DXLvQXwn1gMNFMC07Xmd5YnalxqiT45YhKcg1AOpQPAeSFNK2xDmNPouWu
WiOBJBdRBUS8N0sfPuhytq6o318Qgu5OE32+kXJu1QIgH6/a8HPSrBxltCHiP6aSsjI7c0YMvO41
EGwylAPmNSSn9XPqpGdevAjZDQh1g//7eu87U5tN9NoDG6D8hGmgCEpoj9iLyq6oAJbwM3AhM0Ym
6FtbRTbeLNM5PaYb62ZGDpR/USNuItOMprCAq8iJvofbp3C6a/fgfzurjNi9xmDdIcDU+vIkj7vW
iauVmtdrJUlFAAnZN0WDgnKEFRe4N/sEfb01WY1daXuxkgUTkAfEbVm7YYe6cZRWpOU/VrrbD+Ro
tOeRUsaGWCWWcD07USkvvBSQ1LuLhwurZPtSTR6aS3Qgp36/YuklEMqPgHP9KuXMHy6iXFCi45W2
up7ZItYNAwDmJPzypmLHql40Id5Nqs8ingSytG4V8nQRIEDehEmKCvvA51XIP/TWiVFeMvWBv042
1bws13Fg/Wqd9O0VavTSiOAqDybCqYYJrFs44AGOgJU71GzySJXMv/UOQGS5gDMd+VaewTtI3zye
9BGzL+uhO9aIxUH22J4P6ITHHCBDP2fvEQypQEps3LOSK0fq6E0HU+uiDPn9/Iuat1830FQZHK30
iSvfI5N5VPYlcPlwQozsSbbIygDKftmFOqZzrTETNS8B1C6j6G5BOdMhW3dXaPt5sgDY7254cEdX
bl03Cwur44hHZZMD5H9WYzQwapxPxwI/0R6GLyJ2gq1w/Eat+bUIoBHqC83JeZbK0PJo6TaMH64e
f5X5N+qDSWYCREh66tEiXDk2W/xPpfFCwF/+LH+ezUoRnqI6LE9W+bqTd4VCBYErl083h9zZo64m
NHGtjsx1kWr57FdOISiBKEt0N62KNRWz/yYifanSNB8PvD9ekAKRDq1XvdqnXLMoQjHFrP17FTrn
hKZ6r3mUMSju2j+GfsxtEvxRyBKROke5aVHT5MM+YNCN7INUFQWt1lV/x4JVfo8j+6i52pOTFTBE
GnXKIVQYdR7Q33eq9/LTG1xIJZ/fuvE+1x5Hku0ej4wpLda8zWCs4kPTMGEbvBIhqk1DKg0yjKmi
vxEWQ+SlxE51TGE+yHk6AUzumoSXhC+0GeFFWZ5oIEweu+CpQO0r0vQ7WVC3N8Rce8aktMmLpPXU
9PWqiIseq42yrVFFvSC7wNepbjB1dPx5qCm/Vj4UZe8lV2Xnr2Hjduz8b8nxaWsLsx/rQlTpz3kr
ZBIJL9mzzzGhCD9GqHdX4+pcmMp4eeHp7IQLJMmuk/DeFTzePf/w2p2nN+XNBFsy6BNWC4Ycwg86
ZEN+C5hi9Z5VDf+IZpGW8ouUjMY3dOjzD5wKx3RD+TXHDT+i4IFnV/mC0On07MedfJpNHvDEK1AY
+Og2siwcSapui7s3rbc4tVJ0971jV90BGZEY+fVXENf5YBDMN9hwUuB/+bQV55uHRKBrOGGc0cS8
Z2ss/Q2wNeONSrGUY88A5ieC7mN1FpgbX7XxAncoc/lKVInC2HQYqA2J0sdbXKoXMZ2SEYIoANFt
44wqTu47+jSprAKwvPdZte7OZ/DUsZd6IKyuWPM7EghCYn21VVdvjNGOPHet/khjU7NOD6SyUc1D
gatPpUKRanuy3cfykP41tWcXa6JWdao1YDinY8m45BRXuDVwHTyEPYmEuH7gB4ERq2Urdae3sMcq
2JVsi2Cqz3CdUZWLGR26sKskvoMKe41fXta3ARNwhcFBAsTldUNUD2LKFWjilMhLeJMk2BQLGZ6O
vojgDFilX9oeb67y1NedJRQ5WOzTl68MBxv5F5Nl1rOuS4Q9iZClbGjtrDwAbvRcQv/N5pqHR6j/
5Femef03DOETok+08BpeVcqm+0NIt8Y7pkD4NDJiBXbpd8bV8dfoQ8VV750a6XKD7hvDAuAZrqhE
qaaFABFUgVTQQBzLZgfORz4Evq53rekJ7DsrJMH1/KWi++cCrWWfMfApJUOadT594ayigRkC/E59
7glDWkq3GJT77XCPFRsV1u+k6gqidygkEbLb5b1JEHApuaEqkJTj9OL9LkhqyhHv70xsTbJVK2Og
mgIHiD7ngmgTBIAWW7GelOMXPltuy9w0L3PjS/D/4acvKTZWX0/vbxHeFY49VWKsaT8btjbbJP0t
FYCzEJ55bKqNNoeiUm+meTmm60oUKEY+jGYKTaNq0ane9DTgE0hf60NYFd5cWS48b2jjeLxNT382
/hsgNjU0r/Bt9AOFtJSHLF8VPUxtrhpljMia8H45g0N2EEnl4QSBEaM0m4ngYHWGwtqZoWM+0s65
9zaYqytOwj2wkeihnoCIrIA9cJNKOTByLlnNjoOWkomCYvOProVi6TC2dOohKcSLymHGA5KE0wzj
o0Z7jpERM41aDjcmhL/QpvWZ1Z+u3zKBNMnMvEmKSgmHsfYpbhuW1D9Uvfp7w8M3GIGQxfSPqYty
e1/FdW0/E6A2ASwaiAnHkJELXyr/d3JQVsqYeA/vlTh+8Q2FpN3cYhV975wJscSaZFIj3AIwvf6o
N8m/iLFHdlBweeLJvtx7j4kFmf5fg9Rua8ssbgMMBmHy8VcWs75cWtAtdLDxB1U0RULpcJJhS5GY
ki2VSEqnviJ5V8OmlXyzr5FGCDyRHA7CCL5pH1kXJ1OWS7knKlrklUwlXHFvsRT1VYXm5pAm7xr/
3OLxRI6VgxSNKCN3cVxSx7BuSjiKtau1JxOwHbwSYKt4VjIp/CjJaPX/X5K5VPbYqGDDjFkEwgt3
742cTGqp24dzAA3q06NrfOXrRpUOblaV+7uoWTsc2XyGhUn5UymGycI1/Vh1MlqMkggLxuazWndA
ghk46QQGe6KN+nKqeGGwEeDy4gqxpH46i5/3X+E3rklw0p7toqqAAUnNT+MFXGEIx89IRTzjnQR5
Hav+Yxnm5iyqZz+pCidSPAjE1ARiH+y//3g0eu8wlLwSTd06ml64sOAnzgBDUxSRkX2AG5quhE8E
gtJBzgqXSTpEx8vGAQ5WXhkT2yPC+Wl+CBUYUNxi6f26G7d5t3GGTsINX68tIlBQusf9Q3zLmqPk
AmMn4Oqgkmz5v4Fv0CEpIWC+naN0iMMvz0l6MeLUEdK05ughE2W7FLW4j41AJ4VpzEL4uF8LHeqP
eNOEqetVl4ePbuW5+tHG0aVkMCd82AE4mz7BcfCJB0IxRAMBAMWf5BZhslR5dOvcFHTo2qCTBZAB
GIbBh4JIKnNp7MhZhG7ixsz6p7nI+BHMc8ljVNVEsl2u2J9fl+mBk2BcyRZMpkFeBM5IHXopvanN
A4WrQuTjsxKh6Qd/mYMmAKBtyPqOUNbN7UkGBCayLBkbiD+AVtgywdQU/V/lsUMkCsAT7KWUDIPG
4kXr2jqrrUO75PBun2+sIGTO68s50ukWjwgxhVhww7xiIU4JZx8EBVCL3OGrA+wPYTbUE1S7Jvj4
o7KUKFOQ2LIFWrZ29A8M2xtXi4kxF9hjLYSZQRzwGqSWp9XqBsvJKhLk3cVpxD5NDiVo9In8TwG6
Ur1cHz9KXWYLmOEG4iC/NlQJuI6BzT4xH9smOpLOBGYKzjslY6Qr/Kj+clpYE5VpElL61Bn/tX6p
XiqRStnDlAZv9F/lv5eZpRnz9NGY7PUc/0oGk8myTjg9xgDtZFLZ2wkZJvjCJT7DylFYv8/9YUuT
w7MQ2LQ9hvEUEBxszwkYTttOgmnpCu7h+u8DRKWRXTrLblP1L2VSUSIByWAxMQhxtje5LVSJQ+7U
VBIYaYs933MYdcsx5OEh3P5Ny6fhQ81ccCu4jKrbmjZj2hwGU0ZLD/Qr4DjIabXcm3pv25BAxALY
tRa/QATD2iYbmfm0tglBH4HDuUawB4FF7A7X0vfdtg7qqlaJb0Pjj1JuR2dfuOunjHehtoDQeKTU
lPiSN4HHLpgsqPvvmAEXBPCN6uLiqG2MZ4L5jQ2RK3u03l1BjfEZUtLWhjOwvJF2vSFY/xjS7yHT
QKGKdBBo7NQvBXwVviseGExQCfrAtp/O/fvWfr/us3AonxuxTet0eCQbnzB5Q/d4D3XvcxEzl8RB
3qMsTqPQcX9/dN8LCEAAwTeUDD4QMeLVoRCAn8DRV0mM50ep/wsm38UMDLcypCzOW8onLoXaJcrt
Xa6SWlFzxOQfndVOLVQgTW372+DHGYwI+HhdYFS8Cr1e6jQldYy07mFrspIM91S429rNjmieUZh6
llGtn1Awwkvm/29QHIomwwjrbr+ihSeXm5VoDL+mDpeoS2/OwjW3Rt1/NW2bGY9i6OxJIKCoGR7w
GD2Jx7V36n2Vg+s56lOXjIKm9MT0KLDICQfzGhkKT82H6RcLeUoV7Y27M/NFoLtiBg6VsQS0e3vM
+Hj90t9OoQbEt8NkINvvViSJjHKfJvgyk8k61Y4Ws7QFlYggKE83L4Clqh99MvXZX36hlBKQPKUO
BNP9Rc6qvMah2unFwxjE2L2YOBq6VJSDuQB1xmbkcaKriblhhOxpFMS0lEi1ZXZbLR6qKv3Lgho6
yXMReXuk5bna5sivu2n0nNSB8ltT224zE131ZSfaiOc6yHFg9eb8BReCCYEzHVpgly9efc9XUERL
jXf5bSgPUrJdLj6r+HE8Dg6qjwTraICZG8F8adwuCKL81BTTddpdT9cJH8a/0DvaSFaojENKsqHE
ybyzd+xng73nUJ+zuTutFkp4Jy/8xZMR6QRi75Gij+6HTGOoCw5MfH1B372CgGaJpo0sNwj9Uio5
DXf5dnqMMcu+dx35gQlJk82z/WOie0P0QLQHSoz1gNwXAFyv1pQeyK66CUWk+WCOo/JIbN6GikuZ
0Wb2+IQ7bvHwvQtukEEWQkeVzWNLh6WSA8EhJ9d7Er5sxexhzcK9kMWThyaVKNnkAltCGazULHlR
MvSr/1f7eFA2dTMA5vJePFrbJBBAQub8yz3WLaMQVaPYbZ4TxULv9xdkiWDmUxkSiKVMcaUIpR/Q
yG3siUAbgxmUNE+F6/NADz9RCveAmPTllFgU0p0x0C0B4BCqngdamv0WrkX9PoyKfvAM5xg7kjLj
gs9Vjf9DXrvQ6GECWoubPJa1NyOKycmEOxToy75VzyKT4eZzSUFNdDUTtUHW16gdSaKheNX8Xdyl
HusS3QgNp2Gx4eBubB8tqZ2Kbwo66pBM7V36c27YxJ2au4I+t0CS+ewRo2spCQyV/2alLl9BKJf5
IzmzxbVzwMr0T8Bwisrl7JIqQIYJ6hGrGYzYnvhYg5CcMjsooCOxW4npblFNFXnbpUCrw8pzG0pB
Umu7Ea0kizn1kSmxfIdSmNV28mSefo2QUyOO17nlLT50gvpUO6fiZdWEt+FMwWygToDFjwpyj8lZ
0hejmhwCkm/9NZr2gNnYDFPS+sVtPmXdMtF1+7HlWcbI88zSid4R0JGBDbKsf4o5I4Ht7jwgIBLR
G+uGsLqnfcM6ExpGiJKZLWa0vBLDT0JE79XFO5hAufUtSmDyNlkXNYaxSiFcp18spB9gADrNjilx
mezeVtvihsWsl2OKAqC9C/l5myc0Zp5BpsvLkH6X2vsAcYJUxRoYIW1bHo9MRHeQbWmihCrNRrhp
+asr8aGZkSskO7yu+bxBuQA3Z3gjVB3B6j1bMKhusjYQCe87vnQMHZbW8Qfy9F5tM2vQrccMM2BE
XrA4iX6Z7ogYHw2O3xOiZ6R0PU9zf56ysAHfeF7tG3D/lC9B5CGYLg+QAC/hTcarip8VSZxBuZYq
BTTn9MMv3NIsG5NT1T/QSyG+obFsvG/Yq67SPzAjCpHk+BrMRW7o+SJUGpH5imkVLAvgNWYiFPXF
Cvg1YUDanAOyf0uW7yRFYjm4IHzKxqzWFYJeiipA1/Qrx5yXv035qnV5LjDJVQXdA/iCSeDLvwWQ
mUTCXJhSL8W3iezqofgNdFP2PxK8NMld3Xln9RSnRBBFCQm+4gR7lze8+ifO/SYCSJEEQQyM77Hz
Eh7ahDi0jcwJuO8SR694DNlgcGKxaJiiGpSK89sZKg24Iw8uWX3GaxsbGG9xNh1tRJEEnVuUwUgS
J8YNhKlKVfscYfdZ4M9KiXwYmFHqptDrd1HJN7ktJSgSMNyj6AFo+1ySBXpxK55uuGQEZaoJD009
9D6ESWSh+Mp87iGwUDS214ZLdhLHgJ14r7mdwjbp5UeVkD7U5Be2eD7ZngAH7pGeYoAEue/xlln1
rvohRyMJZOAgJxlkYwbdBJW3WArfAmb3Bdm5+LMungCI0uCsl5ea9SQ3znCU/mbC3noqK7tP+tdZ
ywzV9eFa+XynilPrO6xLXy0TGXoVLtjojoyGBF71OBG555GHdKc8ktRsuAKkiuEM3s4KRfeASV6t
Mn2v2TQ9XmmwqZcT6fYsrgt/MzW/kzmEdKPY/3exQgIZkqUvfpL8y5rwgSaWmWhMuxGM1JD8nOkX
TJW+yt3gArvVMYGDJcCMMe0hswAZ0GPvwoIa0U+RAGka/BE1PrxG6rWb007i9YDPWVpjHEuUlIiV
hVBTWwPG0k/VLzP8k43MRP6ygclR91v6dnHpBZ+AtBUSv9HJFtV354PlHSQeXdCZpir81mhSO3af
yXtu6AEFs9FjRvCZn73NTziUQjbSySLFEce1B8G+USDmRHleiP+AM+8aKsCd8Dpn13EsTy4asKtE
x4TtBe5FkAcmsg6lXmUCFaZ5PzTRSU5ahXAVmvwBGItl2pKiL4Pjp5lz/M3Dl+yrf/CS88hhTiXW
y+e8dsaj26rVxFEfYyqibrVbbe60Eyf9MLYFj1sNom7WOoHJSZ0u+OAotTkAu/Ie01rbMEPjGPE3
gOROYQWOjnaImrWhLp/KfmDWuHOJr1Dmxp+LwicJfFAzOwctIHt5mlMxUNYsRXyZAqzUyhhuHxI1
nLOPlOgmnvWNL7WzUtFc1uPPPFxUHG31T9E6ypU2576JPBDLAh76cBiBBoCC4NBGAupFI3/daLpC
Od7yo+YG6q3ZXjSbWvgUVxzsX4tkFXkhMwn4cEqUwBBiC4zg1hKbRi5+3F+VEopDeBmz+mxtlysw
tAmeod0epbuL/Qh6cn8LDf4xrPjSWiMpw9CfLpfx3whr5QxxmyOshPZzLVxcyMq6Yfy/K1AOLZmE
Lfhfec+/cIwQyAgDy3EeNodckA9kbyQ0v2rTEfamZIMdqIqK7oA2si1rh7GM3o/XCJAYYRGY3lH0
UcsMu3of0xAqcQ4KqysSyq+c4MTNGV9p8LGKUhgzWt8dz6cDNKQMVZYvIb8deVqWra0KTjV0KFNV
dTsnT0kMjfgRxWeSmxI1LoD/28dcBNOJZDSNJCwnED2ku07DKqJEHoYQqpeN8Kme7O5sRnTkf4rP
Bt/owe0aDYSqId4L/nDZKh85BAxx9qAUgktfe2DjwELpsjhRppmrWLVweZHlefvjlgA+sI3PG6X0
8+u5Kq0/p9Eq79+aaGEqq8lfA7FzdV5d4cnB1dWwUkwL2GQAljjLqR67nWrYULBvmBRWiE9KGBUA
6jUYn3DFO5BTNtVfvJVo2+AIUFRyTYZmjaSqkdjcwOGH76a5WcztJLfPE4IA+NTcq+RjEQk8E5IJ
RuUUQVrDJeGOXs31i44n8eRadXne4iQdBMC9cRI2BbJivvyqu9eFJ8qnWluF14/f5y8yTPI0Pe/B
K4JMSScVzqIA5may1YLUNXGKbh8kGTfg72CVlCPj52qWaNeloZ3+e/OJwueoK6I04m//3b8iwdHC
u8qPLM8jIfIyvE2ulwhmy7okqlqWYXwnu5YrX/48qBf+ZJms+m8/+m2DuKzGTk39pLYw7LQMdY4R
VA5RKKXtXlUIVOqIlTWcJ+6YV+dNn/h+1ybwvSD3fft1mD0WNuPdt5RinuuiSIoiruoEmHitKl12
370wgKG+/IttD8Cym7U/qAI1I+vkgeGIEBI0C0VyLiNQAvod8fS/DBzfGp3J8OZ1lR6xu+YVsCgt
VGvDMdokEEQXlKFrCKTK2W9jOrKRyak2aObPJCrZDWQWn0MBe4VthLZlbZ2BzSqDn69f8wOlntoc
ecjn/cMciHL3BIW2VnS/Fs3oShFWEBuhSoE8+E82h8QpQXWWib2TtocF6EyjEy2t7UIMtE9qrgZ6
uhALNzTAIYckRrBodN8VKdhg4JrSNG5VmUhh/hGbbVkW5JwwZ5ShtLqxWOsDuW8mmfuPFsfff9PN
KxrSpRJ3SDdUWJepUHMSE5mHrpw69qLvfoLkYGnr4JvmGs6CYyR0t0Ly1BfeZrtkYuDjH7NXgfDB
Eq7b/C+TGjGhILx5ZsW20qoQYKaN0FqjN9RYOFJQhRy67aAF290avJIkQRHAZbyck8KgnoFstYSp
BzfwZJ6iLqLTwH2LcLYbW369saj4wNCMm+MShujVv4gHGByNFB6sE6wDcTzHeEkveNGc67eHbutA
AqsOHO+itEyqEuT6IlN687Xpu0fIzLzSfxMoRk8t8dauEZFBZesdF+BbOctdifruglbermMSs6P/
Rm+zFztKYxX/9coN1e8TBfk91d00gGMpLnSkI9axUamQCcS4sJBF85xRRQ8APT3/UbC7V5IK8nJN
ET9fet6FtYxG5RVTrrVphLoTbqs1c4wNKwxjBuaCoQEYSE5NQSWEXqx6vtJ+vZlipLtCA1tAy6NK
uZkJvMtzqrbi1ZKVOU3sWbv8Qg27DC2JlO4bVRC3gpFzrfdl27/YoeN7sp/49CF3goa6GgvgjMe2
hd2d5L68CEEmL/tGjBlP1cRlwRcwCW6L+NzYOha+KPBmYWJcaX/jj8cznczNflcFfaTE+e+vWvxl
SL3683/OfI6ivFdjZaMH9sBskUVd1k2AgckOO4O3DP+L1vWI6VpmDWc0ByArBS2NrdrRsZNr/XuH
6JlOBAl3zNPDPL1Z7C0oWIucWvWnZ/FkIVCe5XKkNMz7O3fcdAAXIfdiFju4NCXZKXWmUhfNcvQI
FXGwP/SxdolK/CiIajxx6gj3RdbAVeAXCnCvJOoc+lrpi7WcHZGYodIsYG141C6y/O4uSAj8ypRF
bSIVL5CvpKGxTHhLrVLFiD6KoLRnVZkhTFdEXi8UxsirpBHzI1CCNrVQPpO4IoLzm12rw6FHPCY/
SKyD4BvMlhAIT/qdmqu+J42g4tajH9VXBR/rZ6g2X5t2Tw0ocKuHygNJ0EKIwDG3g/rnyqWPHzGY
rA20SuTpwNFWp6XzlRQIVc1juu3uuinNH+S/qcRCIDyZubrlE4mtUdhnLqY9UXjJ4nytu8dI+Ui9
A9HjgjBCljZDlTfT4DEf5goR7awC9POF3ST18J7Co3uzGSM8Ks3XibxpB3sHmCT47MkmM2iCCLtS
Iu0olxO1kP8S5hALjLHY9K+I/PpH8znFTTDnARIIn8xlm1lmCOzBzRg1xd+PgPjTl9i9NaO5w1gh
966T+vDXBV/4EldSTx0EeLvImwY0phRWjD0O1GBRX8cROYyxHMhvbYawGGgJd+Klhpy1GfRhwTgg
TDQ3BjDJi7WY+WpVdn6DcRrcywZ7VadKLmYl0/cEKgWt9uyQm0ACifEmEfIM50NvgznK/M3lFY77
urOJJDmnxPr0jd795n3IiSFPsNaufqGE2PuvQFd0fPSwhyyo2gvoHI3RW5cYaCBGzlriI9+kbwLW
N5lKdk2b3yzwgqcMyjjOUz05BQqp7rULoQiEE/5oqmjIgKa6p0sYTebpe+mtRD1jsikDa4uDdIFI
s+Ido4tcJaqcZgbATpvbDMyv4ambGO3STMlR4rtQkyFw3PPJ/E4A2UGRdz9+hWI66DSlcm29R3db
mivSproWOSqeCxng3iD6tIHZMbzBYzzoQEImTIZak1bG396joCF3B5W5d5mrldKsQrdGUiwbD/ne
gYR5aS7LZL8fdAg56fxYeGQZ9QEkkcgz+6NAZ6UD1Fytgpi723qnYKGr3z8j7/dZ3K41T+NKZmdB
/KjKj/nrvUvRTlC0m4YppA9E3Wq1iZiGHW/8zMuJkhYNctKEKPajeSfcG5clFc6dp3AwUVJr13jn
GeZJx0noLbJOA1HKdVWXQ/SLZgWlktWaffvZ4/hHRRdldzMCZTUcgi4OEq3ToSNsBaXRX7Qhz5be
fqRLRWH0tyPJRwkEyOsgTM14qH12yLdUt1tWCx1bzJHKlZVmAIi2dSSG593tJe62Uc7xCkrmku7h
oF6r1H24NYRA6Bebi9c8yaNTzsfHxICIDmlYoE1oTiJ/tvFkxsBPI7IscqpPaUsjlDjK2EPIYFrv
vgCdhhs31NkAgjN9abwOay2P3CrfMAYCETqVMhQsDIGkze79JgTRDzi3T8WOwkDb8AWIIGvbSH37
lxng1rg6omp1Pk0nuYnTX44ie6wPm8YbYiFG14wT6FhoEUmLk17ErKHXe1/XWjE/q7UivuCsjDlB
jmTP7nKyauSUPkF+qnfpOBJbKoQRH0HxNyWfd3nbNfjPmob1t3eCcxZzw7J7iQjYlvnPzxoCoOLM
4LzzPVzW7My5jUSETsK1D5T0i4Y+llD64vno/2ICsjRKdR8uQQkbg9OzW5Hy7sRNoDdA2GIkZgFy
vRUjldZNXUh3CRGBoBU/bjkYxUzspzxXUr5eQjmqNv77YdbbHxQcAzcWK7eyAVfwu8aEPk96+Vct
zhoDwrB66NNkMFr/FpZ0sI968Sk8Sk3EEV54Sh5hEY4Koe6asVgMaYvo8v4CD7Q5B4Z8/tHBlqGi
/JKTs/deWI1qvjbNFGD+HXjsqv//jnyosLGS2Ct5IPr85qLkjQ3kmwfe54KtRIdAwIhqb0G2EZni
5cpoPwvMf9a7HFXtiPRYXQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
