-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Fri Sep  4 23:45:31 2020
-- Host        : T460p running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_800_sim_netlist.vhdl
-- Design      : c_counter_binary_800
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
PezJj4m/NivhhZtvpPuzn0m51OsYoAUlAq4ASocfhiaIh4w2ICRM0CU89pzAwtg0lc/5jQLo4cYC
mu8WnKTeaFqddXtoY3hZ3RYfySWWGWZl1AYVTFkon/5dxxjSLgZhfaSAahe3nsHP3nZhfBhWZNH6
P2FF55nKAUHQgj5kDUa9NlMMu1v2Tk7pbHjYwDF5vRdrxXMSYZ39uUpSreq6xSfXp7uRq2ctUwbb
adD32CM67iXFz1LKQpOh26ME3Kpdxh9jtC1geJextAaJdufIYiiMJfSV6zZVXPd61/E3zGJtOVes
tNP/eSExrDBtus2OweQD7Osb2xFiD/YOfCft3w==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
fvu2KTe68JsKfOgxJ4Cs/roMGTXuHVOA3ICOGYWOGkbxccbJZdGQ9cdKbBbLWkSTKmP68otcwtkP
8miKw1dPixQpjZBalJd7i5FQ5/EK4UNii0INRu7TwqOWeHMIqOyU6VTGW8eAIC6Bb5si1JVzEJ7m
FL1W7pYvYT9znACR2TPtPwvQy/Pp9jR/iFASDIdNl3GTCAwgfDBoQdzyZtv2cedNvlHl9IVD+uiU
Zouvu0tIJAAo2WWr6GUBTYAR5MU1Fs0rJwdoemIccKg0UV6nHNb/MiI78MHwz70kOPwo7T+81pBy
ITMuuQdiwH5ua7ri0hndOQ2AZYN6iIyMxca6dA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 15952)
`protect data_block
BepzOU7dAfjGTsa85zewe5OPgqhA8JiUvbE3YELyddJJJ8kdx1uKJnGTaNoFl/Ucl4umcvElkBit
gVkNwlRCGpUpbP5m/Dbefzc6zXg7/c8KteVAM60UEAqEQpXQo3ODi63wG/3jfBggwIVxovd+d+49
ww3sNlLR/U3o/VGQATGe23KTtT1yPgRgHoQXXrERhht6VAdHwusOSB1cSfxlKw2jKOeXrKQaFkYS
Y88WzXfZW2n65Za71LHTDdv50qF3u0VVvZuv4e/uexeNIzUwAw+1pmNwsb4YS0wg8saL0vwe6sKj
F9rgniNUvKQZFtCcq4J/kd1bZ/MDifT5C0ARZqKHJj0kFBEdhRuqk6cRPp5kPn97sBDBvhtA8mL1
tm62zSQjxNcIsF0UCh+hE8fW8n7pnDGHaW43riY2HQUNtXEk9/IXTCdG5lcjN+7rpihYXGB/cn3M
U6DhQO1FJdS1vbAI1sKVGVRMv7jBXTnzW5dpCdwpaFb0JC4iNLTVlirFp9HBAxc7jbTmzaWpJTC5
Fi7JplzaFju67k4IFKhvdZFtV+v4mn0c4bvyohGvyiiig0YI2coZNs9ECngy8md0wB8eb2eZz29t
6cFfnLLVPlQksAI/StHU+oXCKNPv003vgkwQSOXq3XG88LFujtqTzvd4BbmgFlXlBAxB4qKrhIuy
uuq8eD6lYQbgsC94AWFHAK7yzHpF25nXKhUR7WL+pdh9uQTbFDeTAQ/aYlW/AV9vKQ2RChv7avON
75LItAcn5Nyge/gooRCdwm591apxuiAZgOm8B0+G0io8N9ZvyFKcxfDk3SHfZZSlcCKZlJc8as66
08sST9kIlDEdJFywBtKjxEKVKgJwIhAGM9PIH0zc5pktWZdM33B1LW/kW/I1HsDg4gFVa57uE07g
yHOYGK4EM8Xk2V6O3csqK+kBh5Aq2VXIMhf3UbR4dSQj/K+cIW4ZGVypU7AiBEjbIjPmLZ0xu9y2
guCA1C0diS+cWFjdkJD9upqaHiATT1y5Ex2uMZZzAfYfyDPmJGA30k2Yk5dkPbS7rMxQpxIl7rOE
2Uzb4VfBIUPm7Fly0eE1ZkDWU3ktcM866R+kzdpksw7lmMQetDf9ilUwnPXkUCaSkQIBAgtm/lDz
Wx0ARohPHIo3xYs0uOR9T74hyPLiRkeUU1iEnlULC+aajvoTzsnGus48wKlOnQDkAsihpRrQBzHl
ZqMtGZ6zqnjAgVDuYDVzwz14PhlAy9UYV7i5PaO3+0A8aBcifsGTn4gUBYR+p1/QUR/PQjOPkjE8
O//W4yvGXFLFRZfQcBtgthe/qK9t7EjoW6dqxDq6QHBGcTT6GkXzfVJ+5bly9Pr4Sn2c5gFlg47e
toUEIvfrp6VbYBqXJSDTg/xKbTqsDrNGMpiUK4hevOPTk2AzFnTfrQhG5Eu24srPpIn1kJmWQh4t
ulnjzfYA9RV9W/O1ZxeVsKct00zFyIFSUYhKDNOU0uZRrP6fOcWD/dq0U5Rw+mqucQHypdrYiQG/
5ClgQ+nPIp+7T4iLQKJ8wS3urZZZaOxjcFn3DvArrk3Q/jVwKmgcsUOV2kjp2wdmoBGCLPJFfu3P
CB+NuMc/n1H+fRraAfrN99Uq3FCHJIRXqErqoWxP828h0MYlS18tAhNqGrdubwpdAzxmkm3sIHmZ
cWPigpboyLKcHh1ATtF4uvoBCvspA1fFyLi8kJPTR9RHx0krVQeTgH8/Yv9VngVY+QOZUshn4VZH
ATROHMLiOQcpEcJMAnQaz33gTEb8zUzEo76nddQt05xEVaB/6JSVtHJfUyfxjrIkQmK8mA5oDdtd
qHOQZ0Rpb6kYa4cGnm1n1aDduW1vfsi4qNggJyHr1IP64KQNSjVfguyeVhwHAEJcR4Vjqz3sYQx6
clH2Q+LSgTGgvxNqL4+FvaQjeVt+AnlKZkszpNny8NJT8VyBzu0UlMvtNf73Z0CjGpOZa9hz60ny
COPBtj3C3FOmE1FjRDUQ+GSJrDeKUIUpDzs6pOfeXAtecNXRPccMD8+MuyIL9vOJNIcMYUsJnENX
eGh0G0n7xplAkdse219rHCTf44N7cNFcM89vUb+lz3k6JxMdBAQpSnZ93CiZvJHRo9MPmsvUvDpS
UfsFkkAxRHEPJLD7MSCaUCsFAZHcZKopIoQ/1pfVCn/A5LSwTiAP9u+kkHCEdR2qCei/yEtnUV6h
7P85v0QTWrwtc+M6ejC1QUdRqp1YvHbja1kY0kI8SKSSUnsnodXy3u85SxuBXtAm66tKQ6pGKaWE
7p3ce9W9R5xJqIUZYHJrJm3QFT+GrL8fdS7XPOS1hw57YZBqn3K0+pfas/hzw6svCvadVbyegmU4
bx4YVxxAHCrL7ni04Mm+uA2Oz9JE7rXQ2O/sldBffdTShRrFx/o1q70ifuMHks31XlXWQhWabPLr
Lrjk6FYZq2Un+8xSRHhJldtSsloREReYAQJWUziNZ+4gWLYXg4r9s559UrKhJSLmq2MFCQJVudDP
Id4ZjNLXooxo6+kiYIowogs/B6ufhXB8k3y47/8q4MyBiUlHRvhKH/n5oKR37CNX2aE1NJjWXsqd
nlB9DYrs8HOx7+5sGwYcKbI2d2V3CvhDEPXnoeRh1jzQxpMlZ8IfR+/rZ0uvqfJhUEDDyG6PyWqM
ZA4txBw/1hpWaz+bfIhwmiWPWQYWGMwsJ/ihy5WZWYQKEk+8sihI6roJFSl7Efa1n8PCCKrdXgBC
2hoUcPgxPSvaV4haSY44v9io19ptmQ3Bh9nFP4pKygblcLXrLSD5+RDGmWci/+rHuzbXawvbQ7+D
h10viIKFrekE0rKdvy16hptxSP/lJitLpZL5I5i9TOGhO7jsYnBvhpNiAimr4t/fHJqJ9CxtkmkJ
9S3UhFTdGy3/TwkoLx3S5+2swakgUFCnjOoWMEpMayf3sGFkvrxxCzHAFT6MJq9IrEu4dZjgPJQj
CVcjvJrSw6vKkHshgb0eNW/yH14D7jF9oY2N/DgpnDzyzb5j3Q1DFVDTRkLRWrfkzkKU/J5gdFye
2O5dsbWNyNsHMljJCH0EtxFHQ/vrRnY/8oRF4mfiz2Y02sxp6kJ94wjlyEoos2EghLXnLK0l4j1+
cqOPgx6vVcWNKN8S6PzvD0iPM+OUe52EzC/ondoFsIpD5TGqGI0d0TTKPXI4glA403Uq4t/aILt/
C1AhqGWtHDtRYYjDuR8Cb5Ro4KgNnwsQ6zQ9DnBEBSLi1w77Bw6UqWLn7NePq6gta1k83hk4qva9
ejxyBDKYlZwJi2V+lmwIP91nIE5alq7bQaSBxpQkxWpImk5Aw6IjpbJuz8QiQih/of0h1wdymeI/
cTF91IrhQ7AH1SyUms+TMfsHTxx+kRIvfWFj4wPo8gqLJD1WEw2jzNGAmRzVe84t/k6/D/Bxdyx2
6ztsQS4lI7vQtve6vMQOWFXxKPUmdbRjt+c7GwReFqK64EJJn6uZGFcAR6o0L9yEpOzX2zaTSE35
4cOv0iKqNYGV2iy0K6d46QUJmhlxWorfafLxHHKceCNFUOlK2lfFPOai92rzybinhQHJu373ArTE
c9/at069zOX3wmlGOVHjXizFB0Zg6E4NKft7RzFgarkIp5VDjy0VxTYrP/FxGg03ZFlpWaObge7C
WdzSXfqroI5ZeE/drGsz2DjkJDEfheCA192CXBYshy0lLJJqMnOqSrK++ZB81g8pLvX+snW6vfxG
TJyam8rNQgfh2F53j8ZMvODzRaa1WiAwgI8XePSn9Z19Msj57DoGrHSCmQC3qYnw8puNAT1fiKG1
QUR1VPxNEyC8/hfaXPZ2rjc84Cf0jtCVMJswsUjTVS5VtzVliEuBZS/Ji/6EL0yQDvdmFh9VrLzP
vQQyfKm37ySyN1gdeF1L794pICHrFTkKZQmmMnEL1f8ZLWk5w3rT/8QIfUjcpaXBSfNsfNRAhIux
M9kflpuLM5TD5Bi+V87+SZcHT7SKZte6EDCmvH2l97Q+ehKRtgbu4iLgZks4oKO1GiB9aj89xN9a
GOkkMg/jU+mrRPu9D4fiEUBTQm3bJ04TtxPsgd+kpsg8AiosUUJMgEVAyKYFLBv1zlvAsspN5PC3
XADeZqbPGccKO/vOm7CceWUXhE07gUmjlUW8jrHg5zH37OLPrOOEdZsWLwfqXeV8EjMD40eppX0A
LMDZAcIL5LYtzxPXuY3RZwkiG+/OjDfxs2zvndki2zRj1KUqIVld9Fwn+MW1myb6F9Bz+U1W0GYm
8AIYEhkm85P+1HXPIXIVnI3rdMm2LnNc+ZEovRzj5rEKzqkhdmEv9vuEQMTuC6B4gzxa7YAhOixh
qku1sW9ndPFbh/doqg4i/gf0YXDW5YUVmaleDPIXF3EZzHEzY60Iz/iBNQCHYSfo4EV+RwDqmJta
NmBld3lYdqHUm5DxgoXt/sS5Us042ZCG6VZwuGNZKM4QpM12Gp64RZk4Wv7RLTSYRm7ScRD9GsjM
sQWtlww/i9rHHAOiPKlh7XCFfLoWeBE9fisJcqfNmoD7LmzDksG2/8CPH+5WCXb0KIe7obz3knhs
jhBTo/k7ET//A55DH7aeRdbzeUNqHggvFrG+2uiivsdvSXb/K4FdIMOOuSgbU/ZPRDyz0etxswmv
oE88lrRNitHnvV24gljLQ6dnIBUvIjLmZbiGA5rXsR4rLf5rUf92ts+x1p4OEtSTEgtVdg1MmHLp
EUEFrnbJXiOmR70+DaSOYQxIL3WD2be2bmVk6moNbBS61rVOWJTwgsST/4eov9rCamsq7snb63Wt
DJAJkNxNCjyu2vemoxXLPBGGMwAay2MatU1fCCZiPYZ/uGDGBDcd6SnEOQ+2qIFZuAkmSzA6lrRD
FXNDVqt7i8X7vDkv5NQ99JZV92+i5taJ5Iccoj2j+09yETCAUyktqX/hjkNqeMpVYeXrC+4kkn+q
Byu128hp1/bhbiqz7NF18MTY8azz2RBO26iRQVqWKxkcwzbaxRx+8gfnOZn4poaR0FRDZdpN66+7
C4ItYZnz/BnVjYc/QQtinTk43hjBuIInEFHBT9sPb6bkB8zLQbZUjC8FSANu+8ULMZ4ELF7qZxun
OHl5RGaup0dkv5CGb6TBv5ZNWyVA7PGxLf5U3ual8GD+rCcIyfJm5wpUQBF59i9pfPUMuEcUHAyY
smwx6V14QSKsHp2sFslcPY6bsp82UplGQthXRMlEE8lCS63NXzhmY6BK7hs/GprXO0nOcdxxoTAw
xP75zcvaO0Aw/22KLVAYbEK3gTXTQeWe8KD5Ohu8NjBCsdcY27ZPb2ja/iXr/c57q3Of48M/0D/W
BX7L7XK0INuDjPD+vgQXYp6zJtmdO8nNuqVZubi+7BVeoiFZayh7f11C1Rw30c586YzIumtugy4p
dgNZdvsslGQOfqETE9MlXl/6MjUlBeU9IKFo2Ez0wBqjok41iHT3HTuYSe8OAeVZQoBEaIZvhNYP
2AIvasTHjB8362RGOh0N2p32Ww9sKB8ftPR8wn6VONXq7ljJkV/fJ+XEE6QOv0WpElkDtgT7Hwih
vV3I+LeVKKsLzHAYV5BrjqsXwGb7KKobUrCwf7Jvas2vnm5FT9amS2/kAtrdPdDOAY4knKH+aH8k
xUJIhAhJaY9GInpufNwe9IW/k/nYZHBwC/B+XNbY78g/Z9YESgmP+m4Dyuu1Di6VlfMuG/EmG4oc
BpjDzl3W0zN0qz84BTSOqrYZhyzx/51nKEbHFoAojKTjsLab7s2fO2SXAEThwRkZy8QGf62o4OU2
aw2jAvcsg2K8yD6chCZGwoVzL/56lOTPPExvHyKdB+NOb/F+E0TcE0DaZ/oh5kYnxTZmDn6Y0bf3
RFnWjHatWHCLrP73fOKDHLVFsUEBXUZmVCuihBUqOFp/gixUveELRS1I+EUnX4EBL4xoyVK0TKL+
5azDzYiUt9WNG1eeF1qalb7rl/OgOti/5hNv6LhIl/4Zn9bdybyFMXguqetexWF9Hn+FmZnOD8m7
wY6Sz3mPlPNvEz9aYUXXa3VMVJwF+i36z5ZpvYc38caRi5d0dmFMsTk8L26VbLwM2bMmQE7JGPIi
0jNFqTvPyNsyAMuXWc0AM1kYD0EFVmv5FA1muQVp1rBtxuxhimgsi74XJomVUEd0XtX8r9/B1N3/
Ba+rTd/MIR+o22XWqtqFIck5z5to/2RLr1L+CoDdDlL1UKWAIazYdTN3ul3rLvEGPyEk3blvbq5D
cE2UfIJqjr/jpqMyxZqGkk6dDbWrbA0T7QYwp2D3Ncyfytz1WzvZ5ILocR0Y4/thmsganoeLPR2S
3gbJ84RzCp2vaBlyLQdvmbnEFH+2tyNQfyV4StXn9mMhvy+rsAvi+z1vl3XaSVgWLT0znlvu4iJc
BsGIknGZ9OT5RzBVdnBrXqW7TK4/TId/202K080Lo2RxNXT2IpZxitMtZbQa1rSeD0DAaUxefYOn
rkVzH/y9o6guONX+ubUEzhfrX7jJiIIv2VjfcgUN/sbW+cxzih7MoIFQKRQi7PZtoHRO4FgPW1q5
VFgjYHl2hX+FD8EzZxoif46RH+KXoQo0u4U/0t9SeB135xAC2q0WGC+hf7RD81IjVKLcQZXPlKri
sA9GAwpNgi91bKCnN7P0mdTIzw92P82K4qOVFM16wSZJMGNxk4NrEBSPdpNU1LPg5WUPqOYtotUl
kvUEVzxP7iA9riNK44eqysrRmlbE5pVVX3PjD3vslXRWQSUP7vcsb4PamRxJb9CBU1FJu3gBKfac
sxTo6F3Mai9USN8Fi38rAGQixlaXU5o9rP1RyzlPidIgIDp1tpJiM0RifhNnMVccbncLFqebTZI0
U2yJUFhtTobPvDgvPzSaHix7U91pJy4HXRI3k1TUdfvLxDRd5+XdNKIxhbrXx3QLyMmFH4eglxiw
IqqWcbfb6D8SjHG6ihTbB/7GnpngfrWk2kjvQRUTpWFywP/Qx24XB5mVJ2cYm2vIlMfWyokNDoS+
vTtXE6Ab4jB5hhjCYlSFfjaO+SG/saNQrAjJq43XNAK3EsWYNt3QbP44qZONlJGDvT/IvVzEQd4O
lRL8LqcBX7vm9rjrsmjEtSEqHnMpuMS+ym6yQadACC7x/TdLBXIRrOXTyKfiDnMKpswV0KluOjiD
UbGD6Br7/rqETIhIL1X/+NA3W4EfvQL3OYKsuZD67Fc3eOCKu9UfWP2iGBnube1HGac+cF9D61Ky
0YGchLVdzY9hXGUvorQeYnJkDcjhWboTlXUtOXt0X+rXegsYNQOj8mAFJxfGMICbJqHxgC8Cs/x9
UP0cZHY0VXRVy/G9qU790HLHQZ84SX1d8zBcJ8UN/uCjzPVigNzK4dXBLhAy2DwzXeMMiJ8vmcRL
rPRRd5qToQDAxcLFxigdMWhiAuLreehs6yCWWN8jHDZtt7CFbZoNN/TbffGMqoCW9n5ArP1Y/Bnk
6LSx0hYXqNpN1We2xdi9LQMJsfKRiOClcFwtZu1PDLvYdCtdFwb2EVjofznQpt1Qlp1jY1LLTGLf
V/6RV4DnqZ4ajQ8oH1ZFcntkv6tTRHt0LVa/u1pnrcHd+BavUD7TVP7GbJkoWwQPd5sKfHkbBf3H
yUq0/smYF7jLZ4KsQTnPo9F8u2kx3Y+yQLQIaY2cVoKv2z/txIT9HAiUbyPc2bDtBTH4SrLANvZo
8p+j6JpcGwilKBY9ThyRNEr4PUFbyaBYLd5m4rG0mYIKllcxhiczUjwovGDzxQA1rIvLQ6n4gZmQ
xhYU0pZGKtFEn7aU+FZ317ZsXPsJWrVRi22vztMutH7QNBAzy4BO1DgDmTSfVlfmS3EEt5b0poUc
T9fAiIqEugDRS4+wUcXZTOyE4jqZH2W8JJ8+KEwErJzuT4tcl3Jsc5IVZmT2Pz1FKkT5V3S6wcaq
Zx2LMMJUHtu0F/5OmpYnX68sM2xGVb+7H70YmjhPa3gWbfEyKSX1HW0D/gMXc3hRIyX3JZZyhRxq
/QOhdddcU6mEFiRSlyCk6vwi25aGUSJjY55XwdcHOOt9eAzLfZh6dElkCPzNYe6g01EF/bpIpAf0
NRSU9X3Qoo9F5tRgTkWKqPotgJHRzU/n2qIg6XpcICcVdH9J6BqctipXU9GKveM+q/Pezc9U9At7
/IiYAvFqPqQ9wvMKlkGC5sBKDGHhgzkWL9YMvSywYdmJfTIKHljFJz2L9ou2PYLf28ZqlK5y9vek
0ZAQJ7mTbRbTsi2GhRal7pycAdttbar9P5wnFWhXKy/52hgBWquEU5oWDW16BwgLJU007x9ACeb4
70hWU+GG/VkuZgVf+ODjcQfrmblyo25m46eUrHKce+P90HZeUzPuQAH0LmoCbj8xXvB91uVkI/uY
cPf2wCiLFt0o6tZW0L6ZVGtAis8BczKHaXxrFo129UlTrfZ9/tpUakJMrNQgltE4tdk0xIEYqrHE
S2l0MiacElI0bstfjG4EweSgt2fsHBJcFoC/xOjPXJCXmjtUmxVDhh5W5F6ps7jsrJ14xiDxtdnM
8Th1/iQD23oBVkH0xdqvuw709agcaVvApb1y+QouS3bxhB/nGePt2BmuFAez1d2aJFeR+HOdroRs
wuKfOsL+0PRbmBbf0FrEO1ERr9qymMr0wh+uLIAxma/38pbNFiRsCQN74a6kalIAnb3x0J1LACs0
PRuS9d/bNccrt0smiJ4/mgdA5PqpluSxXVxcSjbVUZ1w36sR46t8I8f79brJJrTnT4LTKzfueAY/
sK0PkCv/q5xdjNyx+3Pw/5kf52ZiecWCsvlnR6uErMQw4RJWNa/aC5CCpuAIFDLepHEP+E1xJtyv
wc4DI0FT/8XLF0eDvqLe8Tm8yn9d/cfd1j6gec3LVy5hwUVCcb4urSuWFu8xyT//302cCq4ggvtn
zc7fpbvUBI8vZNCZiAjGudrDoSrhLXIEJZ17ng6kjyyglriQcJN2rQ/OFRfIvKvBhF+8ew/qNspq
iOIFFIL7j/r/1J/uOiI6pWjZPbaWyAXBIIOm16Swi/8D6XN2unQVPd0qBR0CgQI2YkAl8WRrjqOt
n3w+RgHI53RTcvmAJmsMVpHO9VYft6RlL8Q8WQPk0JoIQgj55vV8iF/oJc193IjYDXCUzuRpIK2/
OtHsxzVvu2m02MKII1JofrmTqL/6Dh1I6Bv/6mc3xQ6RU5jCupx9mPKq7oKP4Ix9u6R/rIobZt3H
bcDq/yVfa07yaoRkxP/X0EymF7yyOYo6CEDXDL6maXjXtGW6LECu/muldM6at1qDnv6Ig9XKkXFr
PlfhydNcn1cEcYV46E3Vn4DVhpzJBj5+Xzgr+d5cnKOucM+uSqiQpMk66pyqpGS7Mwf4UFWo54NR
oR2oZsRFi4HVgzd1tzMgk+qo5cKUfCfjYKrHDfYqDfampOSv1y+2bWfOyHYOv9Q+B+4oXANUn+Vi
OUt9j99KNkLQsfoYjBe81vb1f1rdjr5XBRYSumbG31jjWePzvf2s7H5OdVedraNE4r/nA/nxOHoP
XgF/VkYzGetBQV0vL+f3E2+FO09tC+9g1P7gxsc/28UKOF5oHKE0bkjA7f6cTxphE+KK8JZJGWol
kS4sWenTu2IrZn8+B5x2NhZ16fDmuE6yRJ5UGBAY/iaD91hFaYvKv3imu1yWWOzGMpAV9QeR+h/x
Y5ViBkVSgyUF3OoFpFUH17m1kvpQ7YqfY4TD5E9G5/62gGc1Wn92DKggKuZUpDtwFvtCApLC21hs
Cj+O++rtIDY7EtURm2iGgIsJj5brude60YvI8IfRhx2o9TIfc08M4sL8De9UehzaAPgQZF/E1Mre
euqgiy1JstTh/WyEUOU7MEAgECyuD9sctwUuMtggmMaLY4H3vxhgl2ZO3ZxR1dDx9ZgP1lQs8rQO
KV3VMnf806yk3PmPxZx6ZquIhaC+vmFxXkCCSXwtaZOcdx7Rcbc5CwoQU5Eje32sAE5fc3T0Lmx2
ljkZG0HzIkmu2VwHyfTTNNmAjg70Z2PT21BUY/xeDVLVYjr8oNY+k1+mIFKQ6MGz2IHksV+eG27V
H/4cUg0eD+76S6N/seOFXNelkhpxhxBCH2UXcq5q+5VTSJsv57+f0LF9AMyfxXaDGR4MaWaujYSY
52yJpW+Cd8VnbDV7jJBifaA6vW2yDIj8qx1iTDc+pFrAB1YVMuoMMhT6PqNBEHHux2Nsl9mScqu3
XLZBn/tNGnCMAqNKJJPPOMW28HTThmWNk1+7F3tyYo1gcrL+cJKXjzNaFr4jwAjUOWWW4Q8yXDuv
KS0VPV46IbMMgh9Ofq8xMzBabfDdf4MTs5CtVGC8xQtF5aN/anIkJzszr/QpHFLUjjDzM0ZB6jtU
/Vk2dzx50JIznLm7WWdINBaJ+/GbOgAxPcVsa8QrcufvQ6LGOK2j9KQt0JGwytBdkwPz3H9TY98L
m1a0LHRcNESjNfpjY4hiurQfwKSgJm6ocGJUR/nia3lmFr88+LEEjkvwR2sfdq/IQhZRAz4v7T7t
zaHMuucT1TtfkWHa7le/aBGpszRThE4XD8H4R8tGq8Tu3bKHv7OLMOFC/nciEeco92W5JxiIec0c
SNwj9pvY4vkvntOtDj7UYIzu14UHSbapN6kK8NwJ0N2Zc+xA+zqtAOm2gD2/ewoyYTcWC21U+iH/
d02AcDFuFxjXvioe/5BDSJkJ0Iu63ascrW8chEdMq4w4mUERyz208q4cMSG2tdfgFuI96jqxmdII
HoXbqqq8ekhs7KoCKAvce7mBcbj0oM1JzOK37EF6O6JRCqy1/afm6gQRoqv1Lh7w3E2CQZiV3tAx
rRT9yDrLjey+AQjKzmYbIm5MQe4nxaUKcS6x2IX5TL0q+PgmkO/ElcnyWXpUhatv6pvOFjWcLD0F
fgDpRHEM7idKU0jL/84uVRrs34VTejBdOkO53YfiywMkX8EihwhMAF/s5Hchfc+6QGY3VurBtUId
KJzFElPUBvYlhCaA2NVmW/4wauBsODTOmmvN9p7ZFaZdxoSdjsda2Qg076Mb9+JhfJRadlVk2Wm0
wdCvPHB+C2L0lIYYatn/sxjZnOmwOAe7QNRYrtiKENtTc9Gph8fWLOXxpU6qziw1vwf1sMZkotts
UAm1rA7O73/+3DCsdSLrTozupHHhq4ijyv9gqh/ejwlT2oM46Um2SC3ZwUssG6iPSaXrvz3/lxSu
NDoYqmjq+e/LwpJ1ROYEv386KwBcfAvdmQkFV0gzMdLNDXmpQaLjYTRloQvgJP5HhYyYHTL8nY3h
MWWMu1e0AeedhIsUQBf0hDWljxJaZyGzjzGAgR0CKN0yOCexxYk1ou0s//k6PUzLzNXlqrhkegG5
S0XN75kxemCgdVELldJO2gk5crENrUSH7wDn5Vyy2eeU1+O8m7r9aGdto5NjC4T99L6u3ROhnBn7
yJmPnz+ABSTEADP/tss63mCN6mQ6fOxYYbQkryFOzFjEzsvscK97KefGb081qnUrDd51ECbsOY+Q
ceVVrxyWZtP6+XdV8za4obJL4P5L22baHJAdptiR+73xqS/65tr2ZTmhLFkH+KyeuYQBDXWmhg72
qPCP3+CuJLnz47NFly+CYe1QpqWLccTTlnbGabpSBLtAmEuH+Jt9KRAcHJSkMY+Rc1gar1/fwBzb
gfobht0ADUZDSo686PtOMG1euIrCXtHsMz5w+KtSLbpFLfkTMGKRl38ugwH84PYRGLhMHWVlOe+S
pTiHkFLTtwwgQ6oqXPsrFgFXF5vZ2iSiOXxphxzJ3P1p5TDppMxpYBhmuFsPW7FFIN411UuTk9/J
HciUEsHloZQWwrAjWyP8PWuGmcP1U2hrVlU7FSkjpQagas9/Ha36RXTCD/HxsJ+10S6anyiyzagz
fW78wfwh89TKI5v4dpLROSdu17yjVJHEB6+ugujJqlULpOJ2MwnP3GmS1A2Th8eTiGb/vwiPam8V
956PQVRy9is8RlGvzPQd30GEYogbPBjY3zB2xS7pkrFI8So5Qmy2jVDzA4QuFK+teCxNiYpjr8mX
jkf8EyxWPYp1nBPP0B9Ttr4PDp+LmqJpjj/1wbNwDz3gT2QS9vrNEGGaqobosyAUYO4KgbPuATZv
VqBxFDYU3XLn5JnjDCM52h984LOZAsxTYnP4eFiOlW7F2Ik6E6+oLZEKCrG59VNEcG92Wff+8EdR
XqKNcQOwL9cbDPq0ps3m8P6xnWMooHJKjsiMu03knUmkXHw8E9lcbU/sCIXAzD5Jky0RlOVHupd7
2O9AB8ZUcksAL4ey4aQLKOdeptWkW/TmSZxb2tNYYiOAjk0vrI+qSCdF/3lGVG/ypylST1xNUtl8
V4LKttbsgj/ECPFO8cBWNxWUd8QFVYrozR54p+kVpiIqG1KZTDNekiInPwtbT56xgPJ6TxFbdJc+
qDpWfXoIre2Ld8Wu5mRfTyO1aK5UuUakdF2VvVQTFWvHxCQMq19JEU4qpjmqmIY0miLSJgXL+L2k
7lbM+m3n+vc493g5Rpv3uqlzdSZw/CvjzmKy0AzTQm20hSoZmDNtqA3DqI1Q98LWlJuuZNwwzGR0
kCxyjGe9FLIrmM1xwgbCE9xc4H66UIEMptY4Q1l+Q5sMuicnayEZfGGM5PKx7N5zTGvwdEOEXPo0
S0EV07aBb9A1ZBzlZRDbnh2WVgeh4aaqOx0PogTNU4WbFAb7xWiTRCIgKb8g7hhRwygNFI7Iq5+K
6RrEkbGuH7wgMfkd9k/+0Wam0cn1SAnVE8bm6yMsbXIa9uBeR26d6Wf8uQdSkSXz2uE4mTPHBhul
c5TMscF8JAjt4Jk6sdUZ7t3+p0ajnQwZaEFfQtznGyAuLaBpyK76rpYmbDJh6gAGxltN5MOkydr/
PTrCQSMH8hIprtcGet/4WahDHpaUCbyyE394ks+/6c5Aa/3Q2j2BtruC4GwLHA4uxha5iE8uInRu
MqNBG7ir4aVw1gLQoPXD3bo4E41iQxcalNHry41R9rxyAv4t4Q1DgHhII0mFvm9qtjqCAMxnPSQo
Q2d7gJmf9KB5a6mcCx/NVjoCx4EkjRxiMqdt5+3WB+PJNfQ0QdSOfr/hP6RrBYeZUmng4H5gy+hg
CoEcNBfMEreaz/EpmHcAYsFsDrZUHm8RaL891gqrnEtkQ/FqS+ZVwuYWsVeA7U1+z6jBbsifhnJv
jtcekpYsYeUadlx9CEmkrT9ddnvSV97KCUmEbYHO5JKHRBJQNHw/Icsy2N/xWxnwEZn+DO77AFiW
CfOl+zC79R/vlgVKGiZ8v7+3FsSoHN90/zJteO0kpebW3Gl9UbgZIvz9/6Nm1eyclxuowlUJjcs+
/BnBIi+9nMwho4LyJ701+obPFzdFRuCtVfihrH5MlPptzG5p4DsSQIHiGFiX0Fkr/ryRbvv9r3Ea
8EGwXqVi0G172RChnQ/egfG+HLpd63oJ7OY/NS5R9u4GsTVKCwZLB8zLEfRBeO59bfCX4ygLcZxq
v4dUXHJF4LFrPVsk3RdsNsZU0mnx6gbjewHEqwdKmyXnrQRxmp4esvqAuuLgDfWyRBbjPf/ADPNJ
gHOdL31Ks7PReiZtKQdRyeGGUlPI2r7rqVevT1lKaX97gXI+ZY9tpMc43nLMpn1H89c/yBpSxacD
XSbJjDKQ8cRaZbJdIILSqWqbusof3Zy5zvPowXNZRG9X49bUlr1s8gIYYKx1ag8/lkjicpRdEHxF
3Ab7+Ll/vokc7prFqt5DLkDxQpaW/WUEPZSUPIi86Z5bANjgYnduw9xTlaR/x8wmvehXRF4IVtsc
qVywxyYq5ideejuP95DjMjcsHGfJQ2SM7gWVCUf2tB0mCiyG9rIqd1dxgJTT5fmZalJvxFI0a2YM
xbhQZAaIXHSDy0bcieiFq/qxPLdFttjp/KRKBn8JOiHl6vBLdo3Vw0it9p2AYKsO4trJBimPQbUL
y5HmLpJyxiZ95oy7Y3bUmbDu4I3Yd0vbaUGMrZOWOBJcn2dcA76GO5ED99bm/6Wh6saII1DNd94p
HIc6J+DG9Je+hIPCi64ikgPVlm88ZkzwvjnGNn3Cpgf1hvolPkHExwIOv/+ToYTtSKuqVDs/QDfu
G6w2Dmixw8tcxCZa9fJeUcKJfndLTJAjzLWqSdu2jAMW61vcH/w4uB1I72Fdredw4dvub8YBz+Jr
nKtUhBHx5JXF8+5OXmq9IthqR9PA3RzmLU9ZrzD592OZ1eGWTDzT4P8+aAKY2eUYS0LSQILfMrHC
M39wIZC/HVlv0gfCby9Svj2iCyaLzK4rqf9+7iaQD6l6c3SosGtti+veiOSQDXfEzsfSSeFZXIOo
A6AmC1h+P+a+DTt819nzm8FDp0uvOKpnouemYstdAcYPxtmUzihSLBiV7PL7LfigaVafUo2ga8Oc
b+aoLMz/9GRr92qWkg2lE5xkycq2ne0tD0uYcN1Zxr56u1CYTQp7+M9zLjJukJmiUJG0Ce86fyoX
IMNUUa6gsT0RgqYts6vYeQ/+Y/aYpMuHZAT2CvhlRXdxMOtf+PrexqFoBLeZ587aEd/ikeKSMnWW
3DvBYlJwy+ZqZuw3hm5DIIorCNXO0BkKkeBqrMPVb0Mk5aI1/V50qN/UwD2wyQPDFb6j22otwLuO
5WxQtNXFBIojbx2+I3JapjwFfxyGx6fNCfSfarLEx2i0VamRSCxq9ZMHONU5vrkUkSRKuh6QoSCX
0gB6IUWm2aK38TLZ1OjCkF1NUYQRy+sq1PxhP1UUN9df2RCqWpPaZ12sXnk7PdXjWy48rEEt2O2l
YaJoaqY9OuzkgKpGzb7bChiy/3DjA3y/M1cdX6ciNTQQyqhEGksTGsoW3+xM0hX+C1EefY2WyZ1y
lzLtEoVuI8HG2b5+N3S/ZCxItCMVj+uuuTdCO1JpokOsMGS88dibcEJ89tp5mQ7oprOD6O9jNE5v
y+/jnPrDHkKkK1+SOqOvB8BpFsqsoJiQMFqgYchA1rI1E1OlsMyGw8NMYoBK+RnLyVEPZLPa9+Fg
TIF6aV2wIXgozk8HyWbc5f+hQB9LlWtN9cJx2GVi9V5KGzWFKlCkQQhMH+MYQVE++LUicl6sC40c
WLRiDbAhy513PfurVwQNcogTd6P3wkxB4H5h2abAWsRwX7TOhES5E/YNKX/kGSfPeP2bftp8JvSB
7u9W3GuPSUdmLP3VGqwbX7kI67jZ5e9kl17wRtUfuy/nTMJZNrVRiL/JKqZclw70rJgQADndm4LJ
Q1KlCW3FQ+e5kVr8D3uOQHsj0ji0GtCyfG/dJr0Xiqs1Wy3PSw3a9eIS50TgJRcdC/QmlA0BYqwo
zEYN3raEuwka9TBIEEKwhvUWzvr7l4vu+1dUSEEUvzkLcbbXqkL0NwvMP9b6xePuQSV0KASE2QkS
vE0cH7L78H4F6PSfNHhuZVXb5GHw2mdvLEw11rRisb//i/H4ybNcbHsGeVVC1FmGY4jdl+gKNXh1
pSKKkimbeKZ9j1/pIBr36Vi6/7B6dUeyUbLgut95LDrvNuIRgyEpbdAdMBoqKaL1bya7sfx9yseD
VwNAx41JDV6/9XByG0MHgHIlHbo60FXJ8SJXxcXRPTTa5+GYEXpyJv7zW2eRBUcWS3N2d8EMQs5o
l0Jyx0tvoh1ElOPT66Cl5iXv/EQAfjEzhvjuWLPUGNlBl+YFW0vNUd8Bzl7yYYJ3dPrz5ojEV8BI
lMbmjcKL5bcltUMy1pxCH9TIDKACB5aZU9sBKQLsrDlnzr2dxbVYo6Sb3LQO8BaS81SrQXf0WiDm
xPxHgq5eehqDxA+0zmQFq84AG1ixaC1RexziJ1SpdJecg4pVujrF/+U5aUWC33p+aj6nD931G3ea
54lIqpFojIh/6i2NwrabBuAsKt1A8Z2IIEqQVVveYxebF5IaWB4MuQtUPAtxfoiTD0gJRAaEKDPZ
KZybgOjXV1LL57ae6aks8FQsFrMFJcw+pC10hbMnI2Ym3tGH3xB7cxE9EiWI5n+kKoaoeJlqRI5M
3xXGJcFTP+00QURGdOqXBLAVm3IQGX3G+KJHOf6vBNK+1rd8cQrCSRVb9LG66SyHwFT5Ga51SbuA
LPa3vB/5mU/H57F1iVMejE+6NnZxgkMSJCmOP9b3R7sUG36frhs6XyOWP/zT2B611TyD9YzDTZQv
TQh/rgYGCQzdRL8h1e/kQ8nMe3KQjVfhRABIBwzMRIk23UBG4xu1Ra3WyRPe7KIveSUA0P0+c9Gy
+ZVtlAe8DAXOpbsvHZbaKDv3X8kXc9lwiHAZ994SM2m7TrVE8QQFsjOhrWaR4CdTXYAJlj9nmbTn
TFOiOkEjMopXmElz6bdxVlcix3nxCMPzINodRJ+qc9FDzdhGiz7brbSLAKOMREZnGHElBHdgzaKJ
tXGauFBHgjW3NJEtGZ6hYhn/bqDpczzYSRuh4ne8SCbnZDRKEeIukL6siSSlAdtc+pBGFQ0uqBLo
FpJhjBqPssW9dVQ/Odna0UvdpPSIph/fdZLSaf/9DdSzg7u2sIhIkYE+NyrBjBV65GN2iaydNxu+
DjQSAIVLFHI3+Pwanb3zhnciQ3IjCmmMTZhMXWFzCdJLkr4XxrLkWs5xJkn5qNqXV3DF/YXgnUd7
fNaK11KLkMZ6rNlqrD7MwjbRORtZOgDOY2sh0tU/GQIq31IuxkqClsR9SLikWJ25zo7UJWM3CgyW
vbwjAWogC6yrXc0rPIrn0zeSTwPHfoT4s8LUTexnM2JZIJpyRu4EclUVffqpVvch0hr9cY6jwwQy
jVwz6TONm+7dAYXRp4sPyC77xyFszqFGvE4H4/Z4qE22Kmy5DR5WGnQPrtjDO4/F8aHIN5mKHXyC
/6wvlWlwADcMStU0AaCgvpZJ1+MIwgAgyrSOIJ1cUpO9t32yTOMknX9TECh2J27v4Eqx2LerJPig
siikYA/aNepZFTX7d0g8dfnShSJCIk7nHZx4n2LFazJUoFeZg4q+BqrqkW2doOQOHLWeAqT2r23e
Ej7OkUN5A0ttfLG8aQH7K/C75m4waV06K5yF7beytbKgZkCOPg/Ay7OlUGbLE3A/mw8seMDvGHaq
jJziLOSvnyVvaZIJggBoujH00sEXMdoZYzGasHG5zo8O58Bk98ZVhbi1aoWLbw3Nc+nCEHSYMcVI
mDIQQOEXe5173guf/DLaq/TSkw/p/MuwEQvETIVzkRmRsWMDt3qp3r6LAnUGZr3Ul93QBeBQUbSy
vFFAMJlAWkYmxIuyx0GhQelhgXQWtIdQif4V8qm6GeyVm7P1yy+WijwTqEME0svfUKOfddTqGsni
US4P5NP9l3fdELCb3Z6aJ7rQtYekQa2nDX/2S5RMRb/cpwLGSDo9JWV4nVT/Fymfve6B3vLTPn6W
/c7m9OY2oA7eJ0sdow/erUV6ocgSeyJ6+V3efnRItKbwD50Xep7lBHUDcHffBp8yG3//cKOsNmZ0
KdnK3lfW5Me5VLt9+IthJ1JQdrvZV0g8zLOZ4Z4j1QqxlaYd2zxpMDs+mG/58GK1TDDItnApEhon
K4GokbQQkVnBoB2ohS9UYSKBabUiMFE1eGhQUPiQHif6CCmPEWhOrWwyM9em4Ebaj4fGDO0jauz9
hGoWThHDcyIzMJbtYxoHbcUNZ3daOQnkyuxsJEI4j0v6Cm7xOaHQejnNwuWEgfpezmo5OhM+zxM7
P7+/OHc+zOclZavPTLIHukor8lUuQbkEHVKOcDQ5pR03+TtY/p4UAwsTYOF2AiZ+kRet/78BX5mh
5Nx9hI12HRfnvldRWYX6MkLta6qZ/zUsz/agfIu9KO1UEqKMOfBTt/65Y0R2UDlVCdEhJomRlToQ
42/96bN+kHAb/fCgjriLwtTFbRfRWQL0By6oMiLwB2SHebQ9xp1SwgvbM71DUhpO5hlqPGI38N3W
YngHlS8Zi7ZNNjPM13FBWZrXtseIvcPd/3mUJXBj2RBw+Ip3TZ1rqY8tGgJSzJ+0nTCQ1Z7zX/CD
FZ4qtXvcOjERoGs0ZWIMYux0+mFbOk9nbe4K0xokqBhBN3jlJ9JYO2xCwVZCwFlSSdSRmuhR/HLH
FChS8YF8JKpBrQSAi3ba1aIRLFkR1006xhIRselNPTheDysv46bLimkjIUy2b8HhiCywC1HsowHG
QCPf/IIWSVyeUa4ifYOfprMaLhcV3mwe8Iqpb4SHjq/loEKZCfFhcjQlbYCbwZDE5U9k0crwhWGT
6U0zxpspcMs7yNV1oVSZZnSkScqZK9hQjSJAhiRf+IhSZivfkG09J1U0RwuLYQkPqL4ru4KKw0iK
8wcHKJ/81MXgpzAD6sL85Hx/+bHLLMKHDyZJsd+S6IPXNZ4tqHIvp5B/MnFPVrqnzGAwZIq6ObVI
5cz0Szh3SnThiJcmlj9fqOF0LNkTecT8P29tDnV0QUZVcULLuRTDQt/rGRWAXzj1CE8ukesk5Lp9
4VSEBfjOTl+T76XdItyslwBa2c+1ruWD+e/YunMAtJ3B31X+eAjeIq5osY4QpC0nkpo2iygqmEzh
Bnnf8/W1l1S9wL6Qf4zsAWdgUwI+t0gdN7wNPZgxEExAkiOHu9KMvDfOMWSiXe6t8JNFEhVQZlJ3
A7mGvb7OjCuhBbNrHYCqk3uHeBFjWNwyCozhdECB15MPxziDvsHOzW/Uu8gtrrz/FDX9Hv22G0cj
QM0qolkbv6m5yA0Xs5Se/+Ps+hRsnyWe1NFNaDjOYzyBBU3lKmBrKNvUtZnosk06oXInmB2yihGY
3ETL3tNOQG12hPTkRA2AzO0nAm+fgbOJgYUY1M9wDagDUex+bjGA1obPYWFU+uTUq3G5TwXEkjFt
H6vtuNdgjXT1WNnG9R6uSFqmHBolWwZjPLybYDpXSxAReBfhe9Wl7PETN/iluR6PZ43TBsD0NfjK
CEivUfmhESnF0EzfN4EhQSmDKjTIKvf9kPxXuMPf9DFOMVJhE+y7GMv/08uaavMwnU2ZOOKC7ePD
GnEq6gNKEdlwmeDl06Bl9lLvV/eY4k+WZs7dDJGwhrwmTi1nV8n1Ow0ItGocNUcU0Th4rDyKvkBT
nEqgelChpbhp0uD3ERKuOywiI2YhVmm0W9PAXcPhEjzsPdE00kRYtJeZaW/nDGcQU1ymcB8+OGmf
j8za15ZFCKHCdwrYtkz26c4+nWYeD6qH6WsYtRyG6hoqLpwsD1Uo/LVrtMVbIcAvBAwXH/zhiOWg
30ZotyW6WU7VcdoiWCKuL7lM/tTmXmHSV+yrM+TSp5VTpiItTRIk6GjV0/AGHVzUd+jHAPQ7yyCt
l5sxW9jKFOmcKHQqmEcrzP6mMzDiCnzEK/VdiJrghj4ks5Lc9yzdxU/ovJdxeatm7Br65UJFmLyB
jjRCh6BNNHXgP0BhtVHgZqSo8fHTZ+vjmKQ+GDHvAaTDO+aJsD2jJPWF/XTN5/ZHpakEZ8r3llxo
n3uG9kWnzSnol/ISEJ2Xkr0EaQOjHo68hg++0TWiaMjUX5pGog57snAVMABkgpC6UIFLD/ZJkrLt
1qGR2YysYS7ogmjtSfZKNlpUyLcamoZgn5ogcLmYjuVbzTy2uXgoXKPPam98KrfvssZX/a9+Yb8D
63CW4SI9r965JVMsGfGwzmeJe5b1ioejpBQhLsJH04QQuk7p3WuFOHuH7wx05N91sVQSptpEzIMX
FDuzE/9zh2wYSTQjCP5BuPL8KPMT/3f6MXohsUuECTpsxLE1CQkLYw6zxlArzupPjplo5WyTo4bz
IQ3sphtzPyTj6xmbslInVj2apddpwfQ2lQMt+SOA+cKdRUjjoMyrAVu/jeK4oKrpjDEmfeIaflOM
TNsGwaT2g7q0eKbnfeR89+UCgItdQBFyzfuXeoT+3uKd1CAq8l3VbmtdsDrooMCDAS0jp8tts+Vc
TsAIuxJXWJxyc+c04xK65DpBJBXQRFJxC9zTM7FUKcAHNWtPO9hIk1M2btGH/U3lcPtOW2HBSuEh
pXzGmEB4Wgla96+C05HpAXKrVXM22enCZu7Jkp/VNyhIVtrhVNZtGT8vAudf/qc467v9Khc+W0hC
URAd4+8WTtigkIRZUQruMYv/R33JLgL3d8z0G6dz5PQwVuwmVfG2VfFu7DqTwYll8RE7XMBydP+G
ev86o4EDNvRETna+FAVtzvolFILjlOD0fKGFFSHfdCNqjMjbHCZAuLcIySwW2tqfPwsVzngf83tl
ST2/q47IuzELqunXt0X/1/fnwt2kcNfaGDIYzNI9Yq8NjJipl9LucGUdkfB7n1D8/6OXF+KV8hPH
UtUD+Dfd4FE3u87n0MTzhvI1s2ZZZZj6HkdkCqysXsr2c1Cfs23ptS9X9ZScJ9TsX2nome7ZaBaH
pui1RVQxq4l/HnPSdNM65UPyS7IclqMhsCfkHXoG06unJ/yIcVw60zO5M3NpMZr93Q0OhBli7eRp
BVRYOX1SSLLiwzXanef7wg5+BwQv3SpquEMZQKdpMQ88+X0lFn2tJnnm8+3mO7Q4ZoDff9GF6Oyz
guFdDOo8umzdaqtQBmHZAY6efCPtwpsyfGzYKXOsjlBuUVMuPgUDkrqEy4SY1gKCLEae1DhrrAy8
Ae6wC1jCJ+1LqQxI0164VlNKOQnTNTaVpyuxK84v/lf3Yt1Ihqx8b0XaQFFxt8L9aoT42Nk6tQEh
urLVxwExnf5F5WMMu2bpFHlsiwTSpxn3Gek/1RRELKchVgCdX0MQfA6hipFcY2UnRTJ9c/yJr0ot
laZ7SmDsFoRnoaAt27xuq5pyNnWoTHvlxxk7Tzjqyy4h5LAW9hk/UFpwORFwf6ttJNvX521h/MXd
1Zd23u4yhD80lcZPJvqrZr+b87OmVNpnc6GUmcXCndu9f78c2nqoxRkAQrJTsT/QguT8nQHybNGw
o8EM02yujAwRc/V+D+EuoTz/P2+TDWKuo3DouKnDmqFeTepxKF8HDOHieGdWIRGzntPv4SCkvDWi
kCcFacy9T6hrh3tnV5wmeualLhvrN7tM+woJyVdZB4IQyAlwfDS9j1V7fKuiEBwESSrRrJMsnITH
e94/ZMQqBxbLUU9iSu/nV38+jDMUmL2i8PiCCTx4kKaxqRe/W/NnkUFdy+tvLYbjaP3Ng/oaGxhw
mJireTNsDmsa8me7B8+/qR4orHMK+jLUqENJDdbNUL4PCJXqwAhxD6hRfUdFx8HiDpBRhCb8cxyy
ff3SQJdAeLjw/15nhWLZvgGcK/KCd+3ekGVgQGdXOf+cqPIYS0+m1vBUyWl1y1DpnXuGhB8Q4BBO
KYFEwMLWfvIIpjWKqcmHESUt8n5Au6+EfqkB29RafJTrICbFuEftvhYyuX6qWEn/Jg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 9 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "1100011111";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "1100011111";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 10;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 10;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "1100011111";
  attribute c_has_load of i_synth : label is 0;
  attribute c_has_thresh0 of i_synth : label is 1;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 1;
  attribute c_thresh0_value of i_synth : label is "1100011111";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv
     port map (
      CE => '0',
      CLK => CLK,
      L(9 downto 0) => B"0000000000",
      LOAD => '0',
      Q(9 downto 0) => Q(9 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0',
      THRESH0 => THRESH0,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_counter_binary_800,c_counter_binary_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_counter_binary_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 10;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "1100011111";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 0;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 1;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 1;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "1100011111";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of THRESH0 : signal is "xilinx.com:signal:data:1.0 thresh0_intf DATA";
  attribute x_interface_parameter of THRESH0 : signal is "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
     port map (
      CE => '1',
      CLK => CLK,
      L(9 downto 0) => B"0000000000",
      LOAD => '0',
      Q(9 downto 0) => Q(9 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0',
      THRESH0 => THRESH0,
      UP => '1'
    );
end STRUCTURE;
