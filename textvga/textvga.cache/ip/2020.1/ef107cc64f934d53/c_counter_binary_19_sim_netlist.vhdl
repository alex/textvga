-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Thu Sep  3 22:53:24 2020
-- Host        : T460p running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_19_sim_netlist.vhdl
-- Design      : c_counter_binary_19
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bF+9ngpEaYFI4o3tuKnFxRemkWkitWNlEMrlEB6FHGhOeCQTettD4RGyRm5i1aEygxdhi8k/5gcz
wFOaDPhSYNI5LCQ5siUyfvOtycjnCX9XWrMxa7hW6JN9P7PmeG0jsufC7VHfgOqWjtIQIuWUEAV2
lle9RVZtLxvyXH7R61BNMcEEUrxVldguFOtqlMSAlxn/oXEiC4OJz5RBTqtf3+v+XwmAjiDXqGgZ
F40MdXODosbVTnwIThq7XUE4CCgRcUcAQ4Z3zIH5f/5n2qXb6gsSfqGc/QU4jSbO9M7M7MLluncv
uidKk9bth1SrC4FbDqMmlXpoKwB+ZIcsGEBKnQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
0ddEa2vqQMsPixhY6gn9Y540A8rLH3GZ/awgRqg3JRgyLlWyDhSOrPrs+VHC2Uh+Jv4Iz+s40fgE
H4lHE1X1uuBvMB8A/hZXZyClk+iEwjmUyoV02fHD9nmxympvHFapSGHSctkaKrCx4U98S1yjzKaL
q52H1hmO+b6TjGEkgA1BQRS1ObKJ841fR6nuA880Tc6/bVgQUccV0FzQsW05QDATLk7QQ22YfiAS
Y3O1BOeQzfqsayc6a0GLytFanKuFefuAVhSXyyLBVAEyncE0kS1V58rd7jogTIFr+b3ZnsLy9mqF
hUIC8p2GXKSTW3/K/1mkPirmYT97XDq0raZerg==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13136)
`protect data_block
KXLgOo5PrehDYmVq2SIyUS/vkmgTGd3Jtek8TC5qPCaRDYoSGbIEgsKvBxRn3Dm4vsaRk9b2tEqV
WzQ4LZp5TV6f14szb0lYUz0qTXgMho/LfSC5byjXynMAC0lkSOvNAgnZfZ39uZ/koQv6Ry2mSxaj
lmotNntN3BITalu4KF3Otgrk1sNqrCJmohoMVhSkvZmBJkf8IAdFWIqqnmePrY7MP12p9F/sTNJx
1Xhi/moIW4W2EL8mP6FE9+qL3LRRxtdr0Lpr5bWrRrr3DcXY6jrHImR/tAB8bYnVmfm4OUl7HWjq
/Dk9ycnTyvM+jUdGhwHueq3uUmu6H9ijSSDrYEuoFSIs1T15hIrnOwt4O0e0UO9O8jdLiYjbcxW0
b3dDZA0KsgmLEbfi7wVKsXJ/4Wr89O6f3stlPqTM15MMwgfJ92IPRaj17Zs/hXWpJ+PROVk4N1oe
LQNXHt01a4japjGaPP4ajniV7p0TUgycaXUeu6GLCfhpQmiQx69MVbUa4ZXl4TkvXcw1jOt6rvDs
ENUgCwyqvVGi2kWJL0vRgZ7axM/afag+2OMGWzaQkUlQONGTmiR1Jc4ggMM88jctX1z37jSIMgWT
jVb+4fBnsobieteNItuTglpp2pf4TJhUI9VR08mhkl8x4anF+//xrR0+fCLd6T/UdbNqZpcTir2y
n4l2FYaH22busoszzW6lOs9WtQ1/JtWdfLeCCAThKtDZ3r6tYYdgfY22BU9tXqT4PQRlC6bM4a5Q
LDgZeRKtB64lqyUgHATM0DzhvlKW8I/BjYgD6rk19KRuqX+ViMRf+Ks58mrG2lTCzeUOCRbnm6uD
e+uQv7/Jpcv4YouOFovTlXnV2FfJhEpesHKhEc2expgQj9kmNec6nK54Ed6Mmdz7D31oPLCIQ6IM
tZzL/lvDZKWqH6AG+MAh+bAMGAepCc5pquYE2NY3GwyFLgZLuDGmQy/TfbuVAixYv5RiUWRbahd0
tEghyXnORMu5OAcYG8j78oI6XZzPTI2KTskCKFfMxORx495NafNPhrdrdeZNW8Z59yBPnePDs2Qm
MNG9FJpZV93ZqWjFcHSgnsB2OOVy1xKK1sBJ8brVxHPHg9pcgjziC+YyKlW73I6sem0+GBoArAZi
ld1oblkvOAnptoubeiXe/lM4jBTQoxAhsWQZzMTd3wFEaROYQKDUJaFFk8WhFJpdWm16q6Xj2cLU
PSYdpBRSMX+XhFSAdQue6YXFwtxdV/4khAU/Zbf9ifj1v8JYu3Y5w4TwCuVy7y8Mf3JeZfj2Wy6j
VkwBscAnByVP5Wgjodd+zaHm9rqtJ7INtZQSUR5PUeFbHIPhEdsf/ujxIW7wR0rgZDofWq/DbiRt
yDt9+ElLFQPYZm+AOCvmv+F8VjniERhlIfI9uWO/0u8Lyh1ln3cXAAJY2ykwA0zMWEMqNWIGS39n
pFlhTgVM204kH0NjgKXNhXo5cpZpCgj7rEtFdesFHw3ddcDZr1SHAUOAl7qLjNvKmN9hj1oETrVK
gUU/hQEkOjuugJlPysKjzPlu7mKb0MOl8pxN92nTT1NtgdfWXtBOeNmyXf9Od9a7bomnVBB+Tu8q
qA1Urh2aGEZCczxe6P6rXTqmMsYvlfw2xkpyYWhFbxzoniGygHWPs6H9W72Mk8P17jt7SgpUIqfE
u7Ys9qTx3r4JxasAWmVZUgeYsDAVeJR6pqOee2Dhr9t0bF+oZpd/azYuhJlvmL1YEEQmgBHitD3s
3am0pD0okUBYr4fBMIuH77MK0DMEA0JnL6xXj1Hx8rnlHjD3kqp0WDyifHijJt5PZwnFSD5wxTwI
tCkewkWzx9yK4NMzl6d1+HtgZCMH+RPEoSDyiCTmEDeWS4DxH8mPtbebeXTAHDEu81Ur6L/DXdGp
ik77sQEe+3nEqvEfPleutWNzuMyOKQTrdXpwxKeao/NEuN6AC/qEhoZl56MD9XIafBJsYMdGyiyD
X/krKy2YgfqJnuDyzb1gTABJh2dueqcTE/N0+mXGJF4eCbCiMeRNFzU6o6QcCMxIo83DZdikKyad
JSZ+wi4Ca7z2sUSsR96iShpBeFiZOwW2xzkKKMNjysQBcUGJTGHHVhILaSxDJi3g14b/MUaoA0B4
aoh9gC+5AX2JOWiFwZDt14LKd3AE/dFAYzCMrGTtqC4n2BhF/hbkGIj6ubNY1mtFetIkujklhkAT
rb4p/zMCLyjjdnzv8hyDRR2nCDPrgAT8+mLVLEE6rbnUUokB3bSBD1NlrTxf8kNBZagLp2vuPXxj
FX3RbfTw/GS5skKa/c0xyZTV++uOYaNjem6tUA8SQq1jW8N72OdsQHCbkUwhXAvRgJsY8guAboJm
5aXQbXneihxOGv4G1rszeoah1TaG/a8GUvRpLK4wKztjcLGceWtKFg74n4i8k0gEE6zO7icVICKQ
+OFIaSnVTHZKRz/9+M4zcYRe1DvR97aQR8HT2vRpaP6fQfsp1aTCztfgbiaDVn4MURJAl8+7fWzb
uRhYCPL+sUNKt5n3rdVBE5z9j2Ec3ufaxetLcM7vMpRGbwOL2dTbJaEoHCKmqiKAvzsnuUAOKAYj
w75WzQCT+5T8YvYftXjgI1OdLml7xqtfa7jIVjY9r60iSGsA31i03pAujZctLjKjik5QAi/PGy6W
RLMaSTKXg3HkqUGdu1uoR+oQGb8ygKn3z2dP11Q+3iHovrOW6lzbDcRs3vsgCB7AWXidVUKV9iLk
YzSC/00qw97xwzShB64kWyUD3gKSsd0FzNb8A8n2x12HXipqKOF9q0sOeQlX9331zbWmO/nfKx/b
4tU5Ggwaaky7+3mv89zbuTihOCaYeJls+EO/thD1V180sDx7TQHqallczJOaLJ3YZXAkX5C9MyvD
wwf40U6LIFaKSQqgaR/zRyckwKQfRxMpMZeq4MGBfV6PCRoDUOyGHuZfJK/JWFCD2sYQXEPq7eMW
BR/TXAdYAvMRME1Mw8d76NgzvRjkIf7AQ+1sM9z5zHSJ1D8PBr/FJU5UIAeX2cTQPH6GZRA91PwT
npqHlS6FhadtzcqeLmZRbrASCBvnYxxWEr7GNOvoMml/SYHhjg+bC9WxV0/XHowUnEA40lg7k7tB
c711CZCr9M2nXPttNRtGz9U5RjwSVH/V3daVccv+eVccEtUjUOUWd0+OW8iSDR0i3eOsisIAsruL
0EUrc8BGMT5cE4aiieBOS0Uqnpj3lPjGcNkZmlHsRDhQuX5smoRxU7wYTlg1Nk/wHaZKQC30STFS
DzlarOmAvnlIMZQXCyiP3oiGf2GOyZltf76Pq2UMJkK7WoURDyzL+8WxIYDGyXopxyeZCb5jPNHo
jJ7WGqysGJanJOSBsPxml3VeYmBClfwQ/K65XQn9JuNfKpAPxAlUmDC7lOsji7SF4KtBybi7YK5y
Qx5adVAvccwb6ycSS/5VRi1QtDNEJunHjb4sxKOdPqvzHbTRiRgIyEXeXsBhfk29NqDIi2+oZCH+
ZhsoGBt+aZvs8K5TpnmJNp9l146/tK7k4KHQb0wyyNthZmCCGbeZvsk2Srrvbjpssb2q+VkW0vGT
Lbe5/59QmZcGmPeh8gDtbj+MgFHZlocJmsaN5UUDNagOAwnALJ7cWQWX/QsCX/ody0Z9ghtDwX2f
+U9RwkOwu+H025b8jEiTfHVTbLj36tfcy696fUCfqgdKnCWrq3AYW5ECsqiOSWA2BOPmY1C2Fuh1
Ijio3EIBXyKejysN/eCx1iO2j1flyd4uqMXm51/fKv6gqoq0ILO+cgy2w3cGVbc/rEysjs5flUbz
YkF8IR4HHAyrjFHLkw2vnd0g9PtUlvf3ZGdvVH3YyHmOWwlkIeB2zB0SuJWVEvpDCZerlu748h6O
CPFqUHpLZd31MIH+1L1GPLZTht8msBkPfr36QDl1+231zqIBXll/PTXuEIY45hoKMo7BvBZC5kgI
BjPVZUv3/kBA9754EjTDyAl1A4ULyuSFY3zGfdO2WXmvTsIWYriqrx+BKABEtPuZ7F1sxFXH7air
zJ8WhrXRyK7J59vV/xZPHDTMmxf+1bYyOE2hNftKMtvVr52+9blBUqL0BRZRJ+QlTJeEBQTHwYkI
zq30ob/rn1eg8fTptoWfhjgQWiG+p58oARAkwHxxdTSLTdS/wZ72vs/qNM5+w3YzLq2llOvwF9/R
G08XsdBgMAUSj+S4l8btKf6/c693uCVTeQzstnLmkNonW5yVZVoZMjCQ/seDrLtxWDOatqTRhtTO
atPDldsB3llfknfAQtJRJ9errKQk/XTO8hYuxNuHIleKwAIgFmLnmZT+K73Nk3bi28R5oIGfshxy
cKRhZnWaXSURPBafLubhJJJqgBPsQqCfxIQ19tjMCY/sNtEIqtZaZ+zgnRmFZgLjIo6asXmuZXjn
A3nu+bQe5CP4Q5NDaTt+2EWjd0qpF6wExbBP6/Sxx2B0SX+MtM6sPERpPvvcuOl++Q7wPFX/8tUj
Nhu7XpaE2/1eKKpQ+Dd++dfNATq5cn5cpHs1vTYlEHqJDvgTJ4i9RLtxYxyZTpo5AWq3evgGXByd
dY2kSfAT17oSolFD9g0O/4Vjw1xhu2FutUTnQAkdH67W8NijhdHwUKpilVQr9G7tF3CHPezaizSZ
FoYzg/tFa3B7BHrq6M9aHg5vNJfH/wHf2XbzyJhDJza2zXoxOx1pXowquJIGY56LiV+jljoVN4ow
5oFZtDzZInX0A6AxnoCJ6vfAwbqOdGBDvYNyRFiWp9EXTvrtLJ/W/9VEK94oDWWykQn4kAgeALeL
vRKplySHVMCsm/jKidCf+uJl8N4WYgAbBO623r+VYV6BmjnN6LpzNwIncdzEipCgaemVU0ZvgCIe
cPkm/PO8sWnuv/KwFtB1pP4LCVrypzhBOMxJbsVekuRIDMb6MmSjxXtT3YvEcWo95m+ox6FD8mzA
E3AsdLQiC+cszatG2x0zFy5/DYMiPMFhXG0rDJ5a0uMGG1pp/MHD8SclBg7cJIVSEMjT8q95GRaQ
FUyfQqzSaFwv46XnFbdr2FWw9I649cNgIo9ZfiDuigJGFdC/OStEL7LXWvXRQVNKi+t3QJ2vJiQk
WvkNYdWAibiO3WtEAXPpzlrKecW7tRRkNBIBKfitIGGGkCG1r8fCSrWliAv0rL5bn3YiCuIo3IC3
l9vpQCgPTBdAkS0WaVsgWSBHLdq6b7ixnSW7fef5Xj3N61e/LFju0GjAWFrytvSUcpdUvTMZrghj
n/8P8MfDUhy3FYt0pQTAj990VBS/XBfLl2oiIXrTe0lthtq42GhuqIsfL0fW8jhhUZ0QtnEOLDjZ
GsXkL3AV7tss6wRgZZThKADT+Y66NjPmiHgA+Iak524eT16JmKsGf1GwziDT51cY7XDoU+9gGsIQ
E+KPTypeQeY6pBjmVC/3BAKjzmfcL5o7CUqAr30iKnSrPPKz1V9iEhOOCfaCBj8QT4iUKatAHtLA
f139mupEuueVVRdEYS3Fy8+E2QvlckT6U6vlz+nKUYs9JdrgsWI/3LOLGybcB67k7sxG1U6sV1b9
4X8Coz+nFSBOCGKDP0s+ysvrDut/ktjuKfSdsv2uXKixeAZTzhXFSOSWcMpYJ3jKAH80NTIIvBvx
FXU87XPiwe8W3yDb1079jo1e23GwaTqQ2UsNrBCUzq5bFpDCF+JBriYtWc1kAbYxeiZPWGzqEq6k
gBUVjv/sTfYcNdRjw9MWJZuFAXXSC6SxcITSbP9DDhwRu8oVm6ctpa/1GIcP5rhloFk2jIVLMws5
zGWrt+IIiOC9BeQc6D5DIzqInu/FX67G1LNoox1ZS31GMVPuGhAjqz0N/5dcLvj6Wp+nAdjkPHvN
sT0HbI9macXPmF9rvndnPzW7WAebyBLR9bR7V1b02A1XC9iOMO2L8xoscN2B6fsrhn8VDePP4CYi
my2wHgi9ruebDJHATrcxKvBIDsoLv1BC7u3EPAjNHew4xL+ZvBIb/GEtWbTn+z2HCQA4tg21TowS
72o6F/k+vzzO3cfGyuViLw4+Wcc13ZPY68FwVWbfMPuQOHKjsAitQqK9UuGAOUD8lNL9Od3nCB86
yFP7UT1A/e7K2hzgjcyf1sao6YpOUqUzlkdonSwCe3we3bHYIQAHcG4MxpqI+1GtlEwzRopaElV/
XxeI9h7sxIT/CEQGiFCOH03mUVUcGg2sOcrKkCIXgGLFTl8SqbCXwzk3js0NUmEFzHzSuptrYYjh
+kaKEKltSdx5b5WtRHwcAJ1DnKIq9uWVccm7MG1ffGGLLa7MteRfOe+hVqSz75M6NEoWovIaSakJ
qEhszhHg673qxj8qcuUo3EW2hDcXSk3qdBnr3FE6dsjnlqNRRv92XiE5MQzb2c/op9R1zjOlvcck
o7uUlaWMNadxp/EuNHavcfNv+HzQs2BRevTSvFZfzfsTDDigDcgjKvaeUqZpa5Jdl6ozmQFPoCsX
W0pX39MwbwmFDkT1yU9FKK9GXeWkxt5zAfXjvVjnT1tHZ19tsk2LTqgUSOeSC0xARDr5uAnSJIC9
Z9Fidzto4EDyYNor1xTXRGpb/xxC5vAdPlHrQCP/KR37cn4mlwk4RlH8BcE9YF1kQxui92+pTThH
AsXog7928eFvasl5Ckf9kjV/la6flCPkmWNgeeGFfU/fg59Q2FRNWzv8/jy363UmaXu77T3PFr/Z
CJc7JP8XyJKD2t8GgQqyYkLBeM9Pi6EqSjHp2iGDDJEhSJ8PTkhMRQ+J5w7iI+Jfa5Q+6exJHjTS
Pb1YjzILtzJaX6oeazoPMMuq3pkAk5mPy6eMsKWUGR4B9IOMdDDEZBZBUQC9AX7aAPD/SEMCbRkN
ue/4NXxWIxB/dj8q7La0lDqiKPlGWr6yctqyeogBuIdkOeGKDOcvBqnl96+3ePGAv/zyZWawngNL
vrXqW/sgT2R8P0kWBvd1TSelS6e1Fordf8j5d00xuJMiZ7xdI+mdAf86o+YKVeQCtIbezrZyU2xc
gAtPaC9cElTvQQQALNei6i7jkhgviq/upAW/oJBExqiwZlYsd76pqRXmivxlYBS1/2cePWASF0y1
/6MXs+gESws73xHuxRv44P3TvzLEu3YFNJ7Iq4WDu9TkyG4dpHGcDNeKcDuzGijndhHmq0Nx1EhC
F3IvALGczlnHVbyp9GMrCorV8l5uk9t3gj+V/4+F5b/GcMvN6UlLW6vpvD8xBj0jcE+h6idphOQF
IXhxg2EZmN+IDec3IUAMmU4BP0XK/XSM++t66tc+eE9eq0/6QfQa6lvIr55qtIXlGARmcE3G30JA
BsL70yMwvJx++4QC3dZjR0snQhWjlJyTATmEGp2q3bRPi0eT7NgbymN4w1FJ+wJcnmMS092rQQq/
lcmjQSVp2QgEmHG1Uh9A0WHDG8/p5dFm8oWsKzda7no/5HdNG73i6f2s9+uVpEQsA+1g04XZQ/ih
IgDwZ5aEnqSXwkMOJ19vkqT3KeARFysRdHkt7L1lo2k4RuOYFd9nyjldJxRRSMDS6Yy92za5Czqs
Vtn//oMbjJtc5hak51X18nziZR5pLztIU5Fu4OV7m/XI4teYWkFKFQ9g5PyXi+yAKO8/aax1pFRW
D5/57qW8o5Ui3UOjTHBXpNUmcbUZFZ0qg8tlrz351nhFcbdS4BD/JMoIX3ERyjDXDP6xRGulnIHt
5lrj0J8ikiZ2YtIIEwObEzv5pPTrYS7mgSCSRSqQSW3dNxkIu+UbXrO5By6+6Ys2rArtZZAK4iPh
VvKs5QgroVp1aw8usHspA568eXb9avp4J7T1GIbrIroVfgXQGhdCHE42ZzUqpQt0fIW9q01omMw+
zfMGckfn0V6duP3L6Bpdqw7wjQ21maP8dUyaGqx1aOJwwzLAp8lfiYdpOf06F7EPcIRQiQJI2UgH
gFIJ9hBDh/AhZQJTi2llHZr95QHrrRKOPkiVJ0PFAou42FeDuzVIDEzcXP7s2VuJ9A7UG5TfMNAz
battSw0r0nMCuvwmmhQi89NBzHstX7ZCgnLfNxYHmCkIF/j+sQFmLqrdwjJ5jO0oyFxpRlhhKeWj
BB+YhS7NqQgC1QAwv672Mo7fh/I/tmIvlLf8u84O7/M06J+6EPOA0R9pT21PCoJYe9QrizLsYXGQ
YLzsxeG8zfMcou8r4J7qukE4WpcNzesPfOveMQSIZlB4WEhLG2BN3SW/17/KjQAGH/hF4mhEA+2v
2fekagRPJ1ciRBCLvPPl4WHoB367KAg6Dc7gh7StXps0WbmbCrv2nt3vCNrdw3WQWXlp9ugqqtQK
DHf6MJ+bjhex83ZKOCILTEgg6SdvIAfe10mZRD7aXd01Etjp2AvHODqDOfut5F4ESkWjnpNI3t9G
pJljeuD1MMPUNaV6Co5T468Xs9ePWKKNXERW3EB5F9cUMOs847yq4v3SLh8Y3VcjOASzSjBWixUG
M0Z3SZdxo0o16smxQH3LB/XZz+litgpW0bZA4widSJceQ+IWd1CjE+BVq7bcsYdavNd9nAxjfHHS
I2Lv3fE9URgNScHa9qW5tOITh8UhFfM7Xi3EwEPAZiGLw0iJOgvGgNKrV3FZU9uO6L7K8urrhdSE
muZAN8VCiNUHvwO6XbTPHw9Iamo3A80Un9hZdcUHKuBWD0DXfLGwbtawiSc/WgfN13mTCAVmrDaU
Dl8NocpTYp8q+6S4DrXzTHaTTlpqk+gAwkR9v6ws+u9lMqCzsdx6ogp21kMjPt25Ji22lThxcqh/
sNJP+OUvXQRJUt/HCHvHLfqF+sGVSQ9h94UbeG5Df02dp+A2vnMC54vrIvS7VQkp/UymH1uPiWk9
8luU1vCFgRq9E0mpd2QcfsTUz1dy3Qgbspu3Ojj/rnS9gutfLpKJjLIYJJtS1FML5cSW5goPxqfx
RgwiUsoi0Am0mCOG+sjQVB9fA2EtQRUDqNb3TIdr36QS9pItu4XlRRA7DV18z8s1sED1EyavttZr
A7dIjTV/mZJOtQSZXNIxSnCMQiewqZlXoRPaXsxyGd+OU6ZLOQeGe2LnWUSODwQOFQNV5bzUITmR
pC9RS6OqfmbiSl+XGMz24wcmRk9U/rDRQjlyOyyE29QV5Q+TFffz2jw3vPIUTVBXXExYY8QuFVhU
x1gq4HPH15pKF6PJ05x3/B7/8W3t3JXD5i9hl66oZijLlVA45x+IrK5HtQCZMPCOFnP87R/zE4tS
g9BsOHQTcqk9sP96mLk3YX5VdJeTlGHHZoMpL49tYoWKbWvAO/dk4WhEjbcAXC+49oPGDggUYSTy
fGaEgA3khPziMMqUkgXNk5sY/+2zU6yKkUdE94ZIsfB36LxB6ER6JTHGRDA0ByAt62y3rl0TZEzw
XLEqRcV//A9b8XmF4HatTsyNbCtku18ORyLaVWx9PtCnYVvCx+b/kURvApMCdLYbfYYh+sJYqMh5
PD8v6NylIIjgpDJfpqMEaAt80ynUr7fwCPtalh3KN/VOyrjJ9tc1GnOJP2FWQfMSTEKz6JLAUrYP
WwPtVn9X+MThy9XOeFVlq4MIbPQtwBuPjLJHbYtab3ekbUYDhjDpYHklFM4SvGQNhb7OvKvio7SK
jv1rsLC2vGP0ubhkw63oB73utiHowe8PlwNuHyER+Gv308EsERqV8N/NP7UyQGzke2ymnfDiVibz
NeqGU0fmaGbBWUx0cS8uOEJc0dK2mnAyl2k+X1gg4kUNBjFuug80Ilrn5Dutiioy1YZRUQTO81kq
EVfFoS66yadzj0GM1chayqkfzCqgmChDMBC8U1blBltcS4I952VrqAEst3dpQEPXtmUoRZBNmfug
rjvlNuDvZsd+v9ubfTW3OVSVEX+iTpAVEbUQS9EzAyYb/+Uaz5pDT+6T6KF/nWJGSerb9YKNJPWh
8iiiRkKDa6UZH/6vB5yDxiCMmim4FQt/BphqKnZaQfY6X9Y3m94IcB0+GLG1A9lTkPj6t6eXXPF3
eh4TmoI7sghr96c7rxADONbZ5sGJxKZvam62NEU48MLsksCszkMcB04CEmEcurGUsRS8RE6e26gR
Pp1nxHbk5+GUEB2bDPExrvcP9lssprrvo0Rtv+xrWwI+zt6exvoJ5OQStZxh6i6RlPVzlAsLpplu
5dE7Yhssg2sHIFdTWvgsZe/6iwi8XGbLaTlNml327tgoUSkd97QV8+ORiqik//ATW+D5BtlcoluS
7LfpxnN1xEAw82mxUgG1P02uIPUREMRqSaqQsJbXwyyP6nmn859WBm+qsMOwDWjzN5dn+7T5d/bR
y6oGrsvDlO0PQpJ/ZZNvCAWyY0l7GusCf0xFH83XyuLaTPkF0vfhAgrKGu24NAQiCKBHGA+p4Zsb
HkFG/5HSse+V4ggeZ7cPcgO/Yl6eH2kLcx7nzMMWldabNbw3DbDIfjYXYGanZritnhF2Qhz7GqgM
PyJJjVU+Gd6RL3SjPRAt8poJhunW/XrvQOn2nLZshA/Qs/ivWtEPIYfcu6U8JEGnI5bBFuQ6eRqY
7znE/pKNjgxhTA5rL9c94FhxiL0Fl1D5S7jyImzyO+ydR3UTUiHkW8bZy0UqNYgBF3k931cdYY9E
j1wLENg24hsfQdwpISkasFCdoXDVRmuuKtnMzDq2UZ4ZZSdjUqt2KSTigLjKcqhdQCsqwQPHNEsX
pkXDZZiFIqe0U5wQi114P6A/BFWrXDwOtjbqlzowmvrXi96sHhFOoIADjU1XSqONOIJmknxfdEW2
BzzjQwWK5Fr4pVcjwdNjbNbAg0kqjPHA/qSrAJcaOVPclC5c/kK/qt4pRVu3zKpJTIwp+npIjZ2Q
ItvWtj21qW1/0Daj6bicJtjuWgru3zqTxF1YXt31zMu61N0huDV915OdHTs17E3yMWvmVoOMu4Z6
6sXhtz0cGeys5BpDXgoUO18V+WSEe69+I5EIMaNjzhhvyEPVtRYDq9Fd6g3ndJyjlUKq+p/q1J5h
uKvwww2ujAISSWUdY8OQ4+ajy/CEYO1qxMYs75HpdemrZf8qdotQcVrebMufDLBblyhzO4qPLZvS
T4dD0rK8vsCRWxqcadw4IfhJLTF2AfHFCoACgiFqhOEBGeVmGTcmTYUQ1uQH/dvIYyjoecTpWOs6
Po+1iJcfr8yVrNyXmd+KhraYNcPbf6WPSltarxQf/10mdFVF5pDH6qzx+Mqdj709eTd9ZxXZte5L
ZgM8LgDWGjVCpjvGNVs4ut6x3sehUkuEGNSMw27gz3ZIpk85OjAR8gnNuM1LRRaswjJOcST2nGRa
qnDSDBz/+HMtmCGoM0MeInGYU2jTWfGr4wPHCS4so9s/h2nBj8LIpppAhjjDak+OTjLD7XNTYvTm
dYKDmZ6Xj7xyB7aPuZCZFfUpU+RuYKw5nHsVP1mojcwyCSuMxOtmiinwGXtkPO4UhP5hLgIr2pd+
fA9D+5G3+49OgmfYFgVCvBapeAVJ9LKpmfx10OgRlXmM8h59RxOBfwvd68BAIgcuC8/iL2a2xsUL
H9Pc+8zNCTWJWtB+6mzLhjoZbisju/BeBkZBY0QFOQ+ZZFTWFbjEks5eisGhNvGKSVHlb4EAmeNT
Rpvmx/ekUNo2au2HraaUyTQJOhLcH+/iMoY6gdD/VwmBVTxBFhNlJ39unADnuQE0oeiYIKXt5pZH
OIKSJwyateJmckVQaea7cOb3fmZ5thviBD6goDmnZtjmemP7Msg4Dio5/GotaTK3u19C59SfBkA2
zsRdEPSaR2gUPzMSRm6r1SMtqWn+wHqcs3LQxotpHTJZdbxymkVbcwZ13b27AWmMVqnR1FoU5O9Q
gJ4UCoxwwm+OhL4kWKCgFKZzoxLHR3q9uAgNNtI6933awm98PEEe6d6c0zlC0ZjtxSJCrQGosYr3
MrwOD5OUw6Nd+AfLkKYDzepMDLTh+itPpewAl8TwyJDcl2S2LDNAwzQCRCBkIaCcricWzl3k8f4N
dC/wqhhGjwJOYU7D069e9Abebv7DUBU99Zh53xwLJvT48Wc329/2XPtZy374vMvoVB7W3r/ic8R0
3i4KSz7/7w1L847nGhdRY3bfApWzTr3lTYkxpdkhxlphj+sXgc9SMFBZsasUG9b+dQsQNcsnaqc1
BGvpjhOjZAF39r710ce19eSzBzkKsiT6P71GIK7+7MHa2Fgy8T+jdxeTPUuRMV6Sb9YTx3d8ChEY
C+SGGOzcTv+7HVhLaumzIHHPFWMekQAtiVPM/F/HKa4vy5hpUFucXNpjNjoGqYVYdVFsbxNs7Bii
/3U8/Y5E7g9GaicglK+mc3Z2kvwI0joqvMNjB26vATI703TjVVL3P0nvr5iaGd2sNOWwAzFp8bp/
cVT40Ae47BsgnUd2m9baStosuHgbsN04YThFsJjMZUnfuFWQlAEfC3PNmF06SIl9s/EeEV/K4IIm
rohKJN9kCGZBy8hAkaEU/vgWd2XxYoaH1WBbS2gwLGcMLWr7zaJFdQb+MuGc0zfp/K28u2o2UeQZ
a1HLiUPXi9o8j8jdhbuEoqEEEBEHeS9qfYgEQ+I7R2KfXdd95Qv0NXG7d0Xvco2rzURMuGNkwe1A
7Ay1lItMWRwLKxq2gesladKz80DugFiOuhMPM4iIBvkEHdBk+fnYE9rR3wetEJ5Uvu7PbQ5MSqYD
fl8LnzFSCsHH12/sMeA70UPnC3CNbE/5YcQYHWglUu5OGJc8OU941LU6mEgbkOWXqtUqdB9pNYtV
pby3w1RrJYJUBMyl2AN39ECuYEmA3Sk1rM5fhP/kDag+JJ4ae24GNWPddZy0guqUeh/SbC0ZqxhL
grKV8ghjF+EYAAWod3BfEbLG7hrcsLp4IQwqQSmbp1+kQ8QST92SPtcVS0AogOUwuRouBlIU6E2v
ta5qut1/sJnflIkBJGEz8OLAbA2R8SJsxLPqTG6oa+xsQe0zkmlS7ksHSNloFm/ogBzTx6Ydd2WZ
gmO7CzunrpEcIPTnWc5dlcKI3rPAt2zU3Uof9b6wMPpPPPftnlZ3mmtJuh1aVjrRq1tSFLPIJsDZ
PYWN6pZY1Pq0RsdPcxmkrIhVWb0IxJKpcSdt7RvrjxcaReVP6v6Q3uTCmCvDJT27Y7IY3xyt0f+1
Pr2/kNR14uvz50KbYXhlqh8MgB5M10p+LCIND5m8534YDBLKaL1pYB1fHn0/jiqo7JRBvjdvF0/o
n9QY4Eb1CyGIuMT2HAdq557+/Tm6Q+GSC70Rdq2kdUbPYyzHp8D9oYEsCymcfBVxNetb7dukh6/X
02j4JMbYYrg0aj7NjQGjfjtaoZgNPrNGXYRq0Zn0muQWqkDGcBHYAAKZfCX5KPOizkKkqh0zxeEE
Fz7DmUP1SMZIbSDNKvg8i8iIYexwkxBOUrc8x+evs0fj7tsUlwhIUs+Sbuo/7okfDvnj40B5ve0q
0PlPfKYQXuBnj243g2s4qFHnWUN666bFldRxSW3H3+LmXrde1hG5dgI5l/dhhhTW/cp1QmZvgb3g
UT2DGwBb+FJ+LrKxtE0oy2H4nXv6p5FheofMmMc+dCIowZg1BTrT4SEWA3QFPu28YRlCpMZkPiih
aN+01edyJvAkdJ4ib2qyRPWQfV+dYyK6W9rEwkC7Ua3EAsnZYF+rKmvFKeovHogS8HQeSpHADdF0
clMZ84DmVj8J9X1NgpKxseSldyNEu5ULAPMb9w/fZ6MaYydkIs9LtqdQT4+9+JjJrcHMfH6IzM8Y
poIyk7FfPMZ9xag2k896UvHD4mHSKzhylA0EP5PHgYU8swT5xL7qnrEMaGjGYqsPGSwHVzZ+0NA8
9AXpLFOWCyOrhEqCKBRsM7T33Mhu0cKFYbz/PvzjxkkCTIzV2OYhfjTvpXwig9z4dSRnLk7n0fsh
kXVx8bcJTo+0AZrndSyjVEOKIdvgTzFe2he/AM5H8IhCCdSLiYvp8/CvFOCwArGyI+yoo8xoLXCb
js02mrbxMKkW4mdrRG04OPDr2m18BBbwa761i6TDooNh2SibWgIJ2uuhrTk2sFcrxcAjd+fnlUDG
4kTvuu/Ord2fmak7L7uT+1sXrtZTo8XMw4OfgyxMnQJPbhKfD2R/2MPCpT+afkWoviIUiJa7AWQ9
Ca1F0NxM26ZMh8bOCtqw9MkCRiWUM88sPGsFaqPHVj3xi8A/635PHxLh5butIEkHO2itEW3CAalx
8iV0hybk8NqC6LRbumGEKPJWwKEOPyGYFBKXWlTybt3lHI0/O0vqggb6Tg041YuJXxEjPJvaD8TE
gMzb4+rLKDrQ7cz5O4zyKz1XUd0xM52pmigNG8ou3n9j5S9gQTNd+IsDK2Rh5kIAWvRlAlrsJYOb
J1Pf6gVsK8Wh9BeKQdBuxOwCeEbjKPPrMZD8X037GkZqoAOkqkxFC4OkqT13QC7T/B2duEHpn7EX
e/bng202QKApfmNdA7VfRySDFRELiJNo3nhnTaW46KBaE63P67vY9A5whrIA5RlhZ5ebB27Opln/
/mGYnGUnJ+WoQwvQiBjeZQ8J2QHkirc4Y5suMQb1g7ZQNy+o98F3mtmfyo+0FL6uwwhttj90A/4l
Ebv33zFUviypODVBROkvHVZYj+he4/ZpvuLUPk99jyJuW2cqhPHqcIoD4tSF2zkAB14zI/BcbOHE
BoGKDpVdXHwb+oZfg9f7+exmEuXAZLXuEZd39M4fJpVa4VoP98PG+4z8Y00fPoEE6HFXhKXi9o1y
zZcZs+ppt6pJCGeUZSxFQPboZroHv+vWuypRwB2HVxwTeNkH7HytItCUdajasFWM4Xv6SWAeUIh3
a+tetzFEn9dBU0g/5q2AEMDh2istbFDK5IngMd8YYKs3INJ4F5tagIgHMEAC/NHurq5yAGw7VPZS
k+JxqSustHBh8Yo3A2w0TGzQEI9uhDvXoWFqthhkKhK1vcsSYBsAKlErOHmzz3U7mD9zr0oJh4d5
t+umRwqU8kMD7ecv0xcwS1zuLaqdh5FIHrsif4atPiRec7AKyzMNgPPlOSHC096ZgV32z7d1Y/4P
aid5Tz42Jda1k38wMBaRgNFGJCwJRjpdy6y78MrYmh8qeHfIQtA/WobaraLheps6hVL0F/+m0Qdj
eR3ZqeHSKXDFBEV+5sKqZqkjVokDlKJ4zEHE263z8aLHkyouH79g7J/LlWWtpGrUGNmFW2DWAR+7
ASiyEI2IoylbGGsddZ+6P3Vt7I6xa5h9tjd/rXVKG+VK756OyefK85eIk11YZWcU0kxcI1xMe6Sg
d1mzfzpq3WGFnvc61f7UR3Fv1NWKOYrID5+14CbBJaTFT3W42OmUDyyjoxHaTgk42pAVsVZ41y27
xEzVI2AAi5tMeiR9V5PtcWNL9+1cAIcSmXwH39SnapBPaQd5PybNJyhGiMXPTLsusBaiYCzTIdVG
y6MuiQM8xUnrPM6tp1DKYPw0C11FS/+u8eN3wmDccR/OqqM7B16V1nwkJ0gKUn3scWd/nGpkky65
sC3542GufRLwnmXsrXjxFdUSMM1By+s1ypcYHKiUmT3XH01g0gfdeF4tVTyDhhtD1SRdhqIrwN57
1TJlRllmPMjCcUrAIgct9ExCD/l0C3A2pkokoYVGEgfyZ0+Xqp+QD7RPqa0RB3PXgDYldWvdkU8K
nRLxlpYiwki6pdmQ9UU6Cogp8LjjxUb4IakeGUjamG2J5Ou9XPkHAI0VmAGgJMbWTPBl3Obqccgt
FioiK7zycJWL1Z3mElaeAY/aw8gGLaDgSxaqk1Bn296qeWJ6U7DvAtkSDCW0F9v9C+Xde4ptMDXk
SaCpto911cocQweO/WC9l9HsMEVKSl/ZZeIbwc64l0FdO6tV4c2isGxDMQZy5ZOV6CE12dthB9Kh
kJWospy5RhyA0FSldtX6KakiIwUpY2vTjlXho4LkrqbXr4GkQsv3Jg2rUrbg5MQQbzDnM7SmfpjE
tRjD43vmPi+qOq/B4mMau/zRyHfkb2v3/vUvhSrmJLZjkt2Jtjb/yxo1682n/m6mbzIafwOaNXRR
eMYlnrWU4+FJ+XGGWybvtKB5aphhfUinrqzwJj9HeCawXwqH4NaKk2CztcGJsmjGitdMjLYjeSmD
aKPHi4U8GfDtjEgtCIU8B0C6w5c85RP/uA0RGWWrXdTCbYPPFhhnrrgrM/bX0Z3JUNI1CVOptujb
BQaqavDvzI8fbaxQmnEYBtl0VIBMSDHu+MKbdIcBgxzL3XcDsOdo5he0+zYiNDuet8orb/RfGN/t
qkC0foC2vU4xo/nJTlY6Z0lNAFIrcoMnXsPWgH0vt9tj9aTqnkxgpDk2jcKujgnpKTB8HIoJY7wO
E8PZIAiRL6eqnPyk43sMwYV34fmjyS3UcwfHQlm2OKyMJ+aRp/wy1UbzsGC0CAlXmETObpP6iQmv
pZckVgrCRdmW5wIPNCbxLZSqq1W+cgWB0i9wuIZR2oeP3by73FvlUPldI+XU6+42dUlYbZElfwHI
ojhFSDYfG6W2c33FqeikDDYNR0KPgtzjiKYMPll+DKwdF21+ZarLVPIFHO2TRm1r5l6iYKFRD2Vi
qYHk5I5dQx/X8uEAz0Tb//MtmVS1z0up1uAAKHc2ZnkhIlYr5GQ0/5D+tletFsb+MHLMzcZhOsBw
fRUFYWflocYrC9GdV7Ury/CnS+eKQ1SepxYPjiqWFvn8krYJBH33PFAW5vM6+Fm2MMCymmTPI+uv
MZaFfR3EVyyOXYiDsYY12+/ouOM68NXLbLRO+7yUhtpqpJaUfT99mtGO9X9xbA5ArlrwVi4PRMcE
11ys3wahjmPAAurLV5khaj8yXhSPvOjgfsPE82TvUjlNv8dWu7AFdcBwtdDAjW4UBhLwmj76mfwt
SHi8xHXXqMhtcqXLM9YNzM1vkBd1FNVAVbAP4/N+zmdLwH2474uS6rlwUwPbWnYbml9fEPT5UhNH
wxOvUX2nF1XYZRJw1hoTEZu5qyXHicQ7qIGoMBHZrqgNBxYJ3VL+aWyfXs5EE9T1bjEQP+Xgd85y
aX2270MG3ONujUEouT4G02lvcJFRojAsvAg/QcwTNTxDZImfgmRV1OEy1AKrGKKg9pdqPUjW6Y4t
HtZdGs1NNv6G5jXv5U8RBNpUlDkO6HpRLzsKUkV6U7Ql9+3e8XXoklBHTx7nmj5+8Km7ljhqndiP
k8PAmAg5aCt47LUvrE+8e2IofbOm++66OCe35jJSq2uHUTdc2TZ1rS+fl0jGca9jCYUXBEa6/cEH
z7Zdvh7ZYkWCpdQ8KDhoE21N1f4xgRZ49bIQ/jJ0omR2GkSh0VG3xRI2WacTZCJsftPDhuUwbK0n
JPAkkhuN9JOpxT+DZfUEhFQrgrBmLmatQlKKL2DecNcaAVXZoA5fzHqw9ACJbwbT5EN1S6NVZKPA
HvfsQfdyEYyLAXUk5exHOwKeCz/4kGCqE2bgKgm8O6uxSqDMwtUnR9XSAyh7hUUiqrIGSyACdIl3
XmcGSFlhE7m8eUDEay0AUOrm1wcbnx9XRWFSSjlWUan4mS7/bPfHW9RBGx9dixYF0t4Dxh6hu0iu
X9+ZRCJr17uBfnklMg4o2v5kGETPiftw+0Q=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    UP : in STD_LOGIC;
    LOAD : in STD_LOGIC;
    L : in STD_LOGIC_VECTOR ( 4 downto 0 );
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_COUNT_BY : string;
  attribute C_COUNT_BY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "1";
  attribute C_COUNT_MODE : integer;
  attribute C_COUNT_MODE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_COUNT_TO : string;
  attribute C_COUNT_TO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "10011";
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_LOAD : integer;
  attribute C_HAS_LOAD of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_HAS_THRESH0 : integer;
  attribute C_HAS_THRESH0 of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_LOAD_LOW : integer;
  attribute C_LOAD_LOW of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_RESTRICT_COUNT : integer;
  attribute C_RESTRICT_COUNT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "0";
  attribute C_THRESH0_VALUE : string;
  attribute C_THRESH0_VALUE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "10010";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_CE_OVERRIDES_SYNC of i_synth : label is 0;
  attribute C_FB_LATENCY of i_synth : label is 0;
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_IMPLEMENTATION of i_synth : label is 0;
  attribute C_SCLR_OVERRIDES_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_VERBOSITY of i_synth : label is 0;
  attribute C_WIDTH of i_synth : label is 5;
  attribute C_XDEVICEFAMILY of i_synth : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_count_by of i_synth : label is "1";
  attribute c_count_mode of i_synth : label is 0;
  attribute c_count_to of i_synth : label is "10011";
  attribute c_has_load of i_synth : label is 0;
  attribute c_has_thresh0 of i_synth : label is 1;
  attribute c_latency of i_synth : label is 1;
  attribute c_load_low of i_synth : label is 0;
  attribute c_restrict_count of i_synth : label is 1;
  attribute c_thresh0_value of i_synth : label is "10010";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv
     port map (
      CE => '0',
      CLK => CLK,
      L(4 downto 0) => B"00000",
      LOAD => '0',
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0',
      THRESH0 => THRESH0,
      UP => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    THRESH0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_counter_binary_19,c_counter_binary_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_counter_binary_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_CE_OVERRIDES_SYNC : integer;
  attribute C_CE_OVERRIDES_SYNC of U0 : label is 0;
  attribute C_FB_LATENCY : integer;
  attribute C_FB_LATENCY of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_count_by : string;
  attribute c_count_by of U0 : label is "1";
  attribute c_count_mode : integer;
  attribute c_count_mode of U0 : label is 0;
  attribute c_count_to : string;
  attribute c_count_to of U0 : label is "10011";
  attribute c_has_load : integer;
  attribute c_has_load of U0 : label is 0;
  attribute c_has_thresh0 : integer;
  attribute c_has_thresh0 of U0 : label is 1;
  attribute c_latency : integer;
  attribute c_latency of U0 : label is 1;
  attribute c_load_low : integer;
  attribute c_load_low of U0 : label is 0;
  attribute c_restrict_count : integer;
  attribute c_restrict_count of U0 : label is 1;
  attribute c_thresh0_value : string;
  attribute c_thresh0_value of U0 : label is "10010";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of THRESH0 : signal is "xilinx.com:signal:data:1.0 thresh0_intf DATA";
  attribute x_interface_parameter of THRESH0 : signal is "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
     port map (
      CE => '1',
      CLK => CLK,
      L(4 downto 0) => B"00000",
      LOAD => '0',
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0',
      THRESH0 => THRESH0,
      UP => '1'
    );
end STRUCTURE;
