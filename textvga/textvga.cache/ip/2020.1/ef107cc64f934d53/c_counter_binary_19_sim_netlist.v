// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Thu Sep  3 22:53:24 2020
// Host        : T460p running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_19_sim_netlist.v
// Design      : c_counter_binary_19
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_19,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [4:0]Q;

  wire CLK;
  wire [4:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "5" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "10011" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "10010" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "10011" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "10010" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [4:0]L;
  output THRESH0;
  output [4:0]Q;

  wire CLK;
  wire [4:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "5" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "10011" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "10010" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JBVGCDbHZhvGSLplhpEkIjo4aQpOGwRlC++T1zxKgtPKl5vTtQKEhubR5ltTFUonlwQq67SskbS5
24nXdX0kpF9R+UlNm8xM4uicMHwfgtmoT7y4q6sAQ8h1qhOHrX28XUPD1kat3g17Dx2jHb38AseI
q2JLvnuKzQKV6AHG45oLWHJlRxMUDuJO3JUfYGgDmktInhorOXpY/+0e/1LtgBIuXfH/ncWNvsQT
9LcEQTntKRnWJw4/iFYMOSraz8CbtHxFnOlBvicLXdbULPlc57eiD+kNaXHQECmzpELYOshTyL3j
ELir+m0Z0y7bkW4sInPENTEk9KlPCoGiBxL0Fg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
tj0rK0RYHiMtowK3QCKhnXjRq+y6y5V5CQLz0wWktw5RH/V56pCfrabZEgvr4b3SyGsdaXZp6g1e
4MNYXmjy5zekEDQ7dpm73nJWhO1HQfIWYlEjcwaL1FU6K9g8zqGGtBeRloYuIemNv8iLAe8NgnTE
xH6xySCFuyF4qVaWPltGtFBxqaTmjCzqmb47PElilv5wXHbgzRITi6YTp9CTXQnD9pRsP3wOxT31
QT6iVB95/pI3A7myL37GOpQxcqYRIR66m7oezPmbvXWqOfkUNwXsiyCU+RTJutcY5n6kWA92Juo8
nVb1Zkm5l0PrrrGjHU33voL20ePlNeV7Sk/GUg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 6208)
`pragma protect data_block
YMbgYn2KJB107wNqVpXRGzpl1HtMtgRrgsZfdlAhtuQT3u1XKFNxDd/wQ7wXiya8BYw2QGd9nuxy
1UXumlf1QA+L5wxu0GDBF/AZJcGhnFlzkQCb9yBVSJk0jtAGt1JNUliqW4eIJKA29zfOWWmY3rfI
qeXBeSjmWqFHJdlGIyYaqpnm7fIgs5sA5U+un96NksdVekCm2xuN3jpAsdMOgc6FD1Egw5ZKspbv
UY+C1Q2603WfQh6XTNvDsClEFpMvfpW0U+UD6WD6bXPXL3vVY161a2rYvLnTa5Z9iQmh3dfhpe2Y
6V2oQqUJOmIaZoR+1F+RwPZi4GvE1M3vTYlFPl1kMLq4Db+8OKR4kwVnjU0JzfbtzDv208Kg6N38
n78+CXA/bpNfZpWwpc9Aj4n/rmZ37HgOnF+NMCoCeaX9yTA4TulQl7WggF7GymZpKS11v2ODShMi
tPUwh5s50aFNGF+8MGkrOY4sgr22BF9V2OgLw3or2GgwMjhcq0EkWbqMj4Y79n1P1HttxY/+Z29p
q7PJ/fKAOpHGCfNwLN9Tm6l0hS6ZukdfjTkvMz7GbLFIHLGkeYEmtKO8k6T2WdNOQqg7oZf5KLRI
PiLs0q8TBOGRnppX98QHT/t6HeSc+gD9bjLZE+SZDoUgE+3Q+sfcNSM5d1IyFh2kx0AdbJSeLftv
blCeA+RfHFFZRaaNlrb3xkVH3seg5VdCId7W17hUYIXAykFmg524QXaDK9stXnKnPW9b7bebhdTO
pIlo+qE4Nvdczuy0PyU07acUwBikHTn/VbiHrYMwZ5cQkwW9RLK4SPJWcXRPQvZNFzzksReKhi2u
q+kFdiDyjhvNuckkF8TJNCo7YZWM5pSyaGE6fGtKNe7nAw46SDqf4dQ6thnoFE54CzNsrOfD4xZp
UnQaU5t1hm68jJ71zePv0XNO38RzgeVmf4spNlb6ViZHhbUjuUYSd2hu4zyseUZAbPE+gF2gmTms
/QPm/9wLHihc/QXt5FEnquVMJ921xzblIlTkEuL9jAywTgef1oxnOzgG2MP+qMYRKI3iYonMfnEF
+X7msJjYWD7kTjE/ZalUVmzkJQrbWnxuJbuw6GcKo1VY9iRnFPyF/esp8K7m1QYhflmyVNJ43vsk
swHBibgeEu+yaK3G7qn+gUQiQnQStpE7X81TDw3v3VMUxrXQdoJHz81SapkERKhSX9Lgqw7QTT64
qSFf3Cx4WPjX3Sa5LdU7QNC6GiTTKUcTooX6vl1F6Mk2oHy5kKx3qJxpNszbZc7h0sI06N7jQsGy
SDMfPTK4Ata929mJT1aNwB6OQPWS0Jit8dOx5BxCXXTmN6Xbt68rQJFT/IJ9za1ts+nRgZtSLCLQ
B4oCxRfq6syodoPrrJ6+msxzTzv9NMunrktHhBxBiJbd1u+YTksizOrf3CrzlR3TkpXzc1FRBlRA
eZopA2bm3kue0wJQa79TeSjj0HSS+4KrKR3IamBAqmwoWYhqbpG8oGQnE5+riQAh1B0WvVEZnSVd
WQ3x2idhzSuPijFa742ixPdGfVNtTAwtuTe8qIJbYBfprLFH8SI7u3AtXcpNtuSiICqfFpNdFFgg
oJY+X5OIlwJlKIFd8nl9UaR1x2038/MKKQw82qEQeIljSd3lF7jmAW4Fppni88syOCa6YHcyAQPL
1HWKKNZFwt5LD58/YrGrkpEqMarEsK3UAs2oIWtc/82hNrdyp7ucwP7i5R6l5cexVXQXvbpk0vZI
jJT+jW6JTIT5b32eX/wKedSWkDJMgIYiruGuf3ULuBtj39kqLEcYHY3okAqv1MRS4Xa/vgVsDwON
5wVfDD5EJ3esl5Ay7YSZWfYoDbKmiTqmCyXFJbUxh5Xflh3852FX3k0/edHE/EIcS/JLiVOy5w5u
9XMy6capk/f/6pikWWfQLG1W4LfG9FmWM1jVkKRdU45iccKBMcHQFCBrf9ac8DIoGzs1+3jawGs/
jSvNG5GI/7nwYORe6iccX+HZMFjJtdmD119FUHQ8gUNdw3bSoL4StwVpX7LUnFYuLqaKa3BdFcq2
gp8hW1hgq3mcA7TgrRbw51gNmG5Jglm8+GCsK7z8N7nJu7LsOo9S8bB5pB7PqolGPn46h8gx+QBP
lzf9tcBufoomw+HcGSKXztx/lNwgDfvTL1omqB74bcAEEiBIDDrFSH8Oij3G2Z20Es/3vHNoeGa2
yjAt27gY3Z21puwHgY3l6HEXgA1WbEB+nHg+q6NqZRfS05tvmSzkwYCLzEZFFWaYon2CQZjaMnEs
kHgN7Kd2NLjBO658zLLTyIRWz8a5Xkmoi20M0Fxsb0x80yjKpAlrMWGc1rN0pHGU41iUliTphK8V
GVyzKqxnFcKoDvy/sU3e68JPh/bkuZshQTaK/BkV5KlTt3JXuVNvmnPf3crhGHDVpM1d+Dmc9K0f
Yul3TutwkhnM3Zlazgf+SfoCB/9F8PGOTWKACr2gDYmylCTLdwBTDO3qiAUBv50Tga8t+Ac5E341
XIUh9stW9keEoODf+23NA4nmfFhENBHiqxIx2wGzhOsq/LWfIB0r2YkPWY0K/BU/KZTbG4Db+nUU
Z4W/AVf8/KnaE/rAYdVZeJPHK30XS4LjEcPh4Z8/Xy7AzGltrTBwlI3zzZiJCVVazeKSRlYgAAen
giCkysVkOYfSNFrA2X3zMyBXsoIcKIObSpmshGijpAqqoWB/usSnmvexYHJ8y8bdllW36vTxHpVL
vW6t4gOrrsxih5kql+xsDrudflelcApChoAyUQu3749W99YGp/As7Af+QqxyEm2bkvkPdm3nRPTk
vh0h4IbCpbx3iwIcFXDMV79murmdWJkkk/erazWyGceUjDCnX2OtvVUV3uyf0gu/5eIzOM8qIbP1
4TMbc37VYmKcC7LwdDCVFDModyeo85RFc+PCxnCeKqaXQwUzNhTzYnGjtvdOen3mT6R7G9S62iH1
Xs/5aH0C5ITwEPw72+f2tgHYvTpw0fyUvV6UEjUe+QYFJX/ABsGL8+5Yvhoj+9hZU+5N4jqogNgk
PgatNvNN5VlYFXdt2E26Y3143FkSZZQct61S+TPKIubPywBeJvkNvgNImTfrm/sdRSsWJGmPkJbU
OCxGQwSoI3yLv1P3dBKVY7UnzsZMyx15RHv5iD5FhrZ0mfkw6miQdADl82GXitL57MJKUDnhnD1A
xh4/7XSjtIByz1UgwArCEUOSYtaj73q3JbZ/lV3J2zOFQ6SxEf+aS3fsXwR5rmndzdCh9I4DmSLF
6HrJNiTmCdL/B8TzHDR3NWXcbdBSSXaE5xB1EE6XVYsaP7O+oki5n92ZSCe6n29pqA2vb3ICk+aa
nVGC0LxvaKgPwJoQtbArXTynjy+ayUUeT/Jxjs3WPRHesvEaXaA0IdgdWp/XTFTkklyl2KYj8jQ8
H3c/7iplEHJCIIJwgJyipL84+FNdkWCX69/WcPQ3GBqbTLtR8iESGpZfQawx/bY7L0Hop1By35q0
pMAFVVRUP2TKzx4GRgTW8N/hPpg6PV4k/KPc1Fg9SOa7ebb41zY4eZVf7TrmbIBwaedgT+cI8upW
lv5/Beyvc7vwmnrXt3iQ+LQmUo2gvsEqYU2FJvopsGnSN+VQIcaMYDl3e+3yykP57t6Rljr61vqg
EamQZYeYxGXVsWCJ2qkE8ZX/7IMfZokUOzNHZwAl4QYbzsPMqGBoJDoM3lbY65SDM+/6OXM+Ee+8
unlIZmL2ayo+1LSK3BEq/Hc2Z6mSD1F5GWZk5Tr4xbeZYn9Y6C4fqzTHApX7hm3m4zM1I6HwVJFK
aKSDwdNvIUnnN8M5S9pxoWX/oMIMY8WSjhB9uZZxqtj6qVySy8bJd/8TgoqFJF1ZrOwDshqD1GL5
NHQv6PgUkYbYZqGevyrzkSsXvj0eCzzPXFgrptqQUiOL5zKg1naZZkuLBvi/UQ1KM3HkF/TPO8da
zY+V/thtCS+5l/nTMYNKIyZoFVRrbInPXUkN+AfnwF70TqrEnj8qcbtB6Uv/j5DlBxXURk2+QZCh
CAFYnqzhT312jUQKRYdNW1fJz27bpqevsL0KMpx7LjuCdWzjbN/pOMgVGCAC6zU3esum+kJlAQSG
5O/GrwJM8OigZizoXACpYq0weVH9/FD8So5WSVq/7CxKrSIIDLIPrPrbV/TriAEQoJnkRrc7rPHG
OVRfsSLEsCKccmOn+YV/xRX36dGG7pNwWxnJ104bOuujUyUJmfkaIBKW+cM6KoUer6slFosLNQii
BNTsnQ1/iQMv+0y7AHtMA8fyCnyEgqFpdoMea/yWFm9uJvfKvlt0wcKFOBUY63XXvpvHa55BkTqO
5/sLqqWNuWW2VlVp0pWthuAUdze229ntS/si7QDC7QMM/bMriZ/HtaZYNQ/wC2EaTMYy6MNdxj0v
3WgUPB8Uh1K+j/1qK3tTm5fOnB5yNc9pXuy7gzMWGEt554F342upKNtxVaUSOQmr7pH4WJSaF1cL
Yw6YgqJc0pkUY0OWNoTpJn4bIplHh7CAPlVd/yYhHx9Wl43ySYIf4Io7D4rOMHA4L528b8uf5fHF
McjsOz0Rx2guKBdHzPZJqjS91m+YhGXMyN3M9b1DWjFg9W5WKH98zLqIjjUQbdhrp9IA/8dDE5G+
acpZAQSv+zQcZzEC3ZhZhG6b5z29X0SSxLzhfVHbLKVP2njK1/mYZlyKxQ3GRwrF7LRobIPrFz6F
OBChLs1Y0rgTrzWtz4slFCiuMmYTOCEH9OpdjrzbqlM+b1YmH7OsbdW3TPdcRDMsDCVTaoskIlQB
LBExGY37IhFgbpTY9UIR4UMwI4oWU4dsJxgpUms8iC5h141PsWR6+sv/0+ChYtnUxOjnoOOc2q8+
SHhN1STiU66l1CTucAqB9C9uce8GWyCeVK6tYmlc4Y0sdlDPf01xCFBBGAUVZ+NvgKepmg4aPuND
7lelqOdv5lqcKvYNCDTPenzIYmcXvKpL50hZhkW/RtYE9qIpBNQ6j0IPT10Z2I8tJlXXBrrSQrgi
SirBr6M1EX7q9qmq731Ht/iRM+kUzm1Pz26JP7ZP/JYBPw6d5fjCFt1WLfPSjRw98hB6QbqAkNYZ
ZJ7iG7KVbepmxMrvTOFwKoNzVDzUTyHhStMnLMabyHSdvC18VQfHwjfr60KwDU4zmUqQDqCTsDF4
W8k4T5ydHFCU7G6l48ckNwUUAU0HIrqUGKr+iV74AGh4lOHC8u2CjCR2hdCYpzJhjqUy84CXuOyz
DWXUwLTIw2qiW27PJ7PR7/uN4p99ncQE8Xtmihxv+uo80vdd9DEXqvsi8mXRqvgMNqOnQzhvABki
bIzas1ScezkHzB9Eg9rmmCBVP91uAGFA0ae7AjbV2InOXzr6MWZpKfSvdIQ1cRM9VQcuGk6csO5L
hLyG0WDX3tYUreCQ7zQq03KwNrKnuJ0ozt9S1J+O+sJk4z498gKmwduoXA+neX4Y6FFFQIrbRkq2
dljNAiYO9HNMndM8TtDTCx9druw9hblhGm/VKpan0q8QCL3D4e3EefV0YrdgvcEuDKqYmrlj3wwf
bSzOmHKSJYsHFQwyw13gu/PoNJOpMtiSe93wTYpVFVxx4eI8+X/7f4l6c+amXJhlSxDmTdbrmZbk
vnGjtJ40d7ON8RyHHa7gfsWGJlXs4Lj6XZtUQNYWJ49YWXt9PPCM76pF94fyYlDBgL/w4WRiTM22
0Zu8Dnw3MVwreh1PQkry5o/Vd6rzaIblPDU1J6jkOoFzEcAKoh2e8x16DQLiut2MNox+j+YHxqB1
iPQWGuukAypl2GJ1XACjeZ6R87bei4Kbd7rD4/GJO80UFoq9N1mjekFAb0RgFXyA1VRxgl677Rm/
2v/DidU9ofdjEpmSr30eTDsaCiJqKQIkgNktbEWnJF7v/uMSIt0posi1PGbaFo06N4dxTo70vPAs
VIrO1ciyL29BFJQAXmYZCvcB3Mp7nF6oGMd7AKXH7HHni+p9fRv7YjiaVzxE4vbdl7JKKqFrwasc
ejucFsX21c9XzoAxr+sXphv0QDMTp0Re3guX5eeYhuyZVBS4bejJzP/Z+xFiiz0w8cc4ssuVSPqb
3hIvyjIzEzYQtnibjz6Cwh+S3ZpF0XHJVDi4ebGj4nQRCmbtDeC16lNmXh7AYAnnf/K8ZE96mYPU
zqZLuZKPE/ztu7uo2DqyRtSnXp4BWk3BB9L13drD0M2/DQkZrxfE8fbOMzWKTImZUoftLZzA350f
w/eNXHU6jtdjq4GNwGGQryJdvHduDdMlUcqPL6IxPL7oMP13iWoPS5FqkHdB0SK7ulSJ6jpASBBG
QmogWfjhbaB92bMswCWTM6zT7gkWnR54k4zS3E4aV0TT+514/PCnSbsh5Ks9W/auo+qmQJUAuyWY
nfsXsYtik20WtoU/9RGg4t5UiXeGuY68X0x5nQ5phZ8RjbzbrTe6pp8oOAheUphF8v1z/Kxbyl+I
1dJbF94sfBlB0W7cFY15kPfCDcsfhw5RYz5DMcUjuSMErSLjP2zeoUW9YqQJ0EAR/nsm0RJiBK2t
s8I50DTs1qKH6Ror27zIRirWbXaaTr1zRCufSNNm9IJqHIBG2odefflEtrtFqeoviC91aaSKrOWz
baNZbVKQ9jlNwsS+0cNBcLmu8sqNj/8mwhXXIpezOpYY3IlMd7LSpNqWLJ18SejKF2tnA41oQK8p
V9Yh8HJhHYnCTv1RrOcCvXd0BoAwSKFQScCVQDYoPzbeYmHoMgPa+HwTMVH/MjwVM9DKWmV9mNoX
Rbe2F1GXD3sDc6LmKBExd14q8MB8MCDkcns27UywMYJSJuqp9W2tUbCo7Yo0Mk7TtzMbzzOeD/1I
CnCP7FXhpjCiVtgLOnmQdJQUvC6QJh7qmEvyLJFOL+K1UuYLFWSH72QWvJaRXV+04mflWX4I/KvX
9mO01C1DimSnM3Esyjuaag2SZeCxwkTLnwCvvkFGmae95vhW849W81oEUtJo6qzOGoKIU0AnmqgN
zs5lQmpuUdq9cA9UtWlhH7q0y31N5MRiNxwzSxlGQ/FJW+ZWGNpPVpyxf5GwNx28OgWkioUaGGOY
Xm9mFJIz99O4bqsZHV+YJaY0npOjw2VXiaKYbrpaBPveUzq1oIT5z0BhJAMV6QykTk2yC0zVaqwd
CXwBVny3jWj2NnUk2smHMOiz3Z6T78Z8kTyBAK9kSBhD/Ci46wvZvvO2EOC8WBbAqcNABU5TBXzX
ObleiHqMDs8e0ZjriMuyqGhXhimA3I7xk/QBare5VB9s/7URn4WrclqJAK02Z8/vglPRA1WIU8ze
K6/ChGrnCfCAHvLezL0pfck7dhN4z1v39z5+3xod+vd4YH5AWFKWW62qA5BqwywjpGegCtp59+kF
DECDYmR6RBITFPlq1Oz2y0Q3RhOoIcWtmtSrqp4oejId1L2ae2uwjB00hd/A87Zw1G5I/in5Gz83
62rdkEPSZQ34M1hGEtkvpwtHsOrcQtRnqiRQdqbqMmWVCQudKVqYSw5sXSSw63Byr/Z/OQVlLXWk
wrJcJexi1ySUKkHvIRAAezBMfbS+eWVs1GSsM6ApAXEW9ulveaReIudN2W+6dmeckhJCSGn1Izlr
qZc7g0HOlTU9VZnM+vsv5UOsrdSPPMu4lpEPAClr0z8ZokkUIzOfPxdMzg25FwwH0VTx96P1NLM9
cbKvIvtQhe5mdQhxivmVtclmQjecA6kmK2uMjCTcK7rk5MmkfwxUGTcZ/a/ozeLHhuHQY0lFyZQB
1gC+dpVK6r8vkVa6KZmjIXpjzh47seLw2l1BQO70u7Ld6UtZ1BALtFPDdlFJo2Ve2pfJFau5D65h
ObljgV4mvBGiSaTyp0kT68d70y7FhDOAvRAG3MQx509p4K+A5Wp1pUULvgLmrN3udByTt+vY2wEl
9TRB3jpZdOgMpDOzEagKE2siGM1UYlMVHSARn4W2jQzPsekhmurTJyra6Afb0plk9PlFWFigOPYk
kufjE07HSRF4IUTMhZD0cntwNwkMuuNjp6fLjzRfcQEpTiiHFvEFCyqdkYkx0Vsdv9hR/MlNuQBt
Ys0h0CZ8ZLXw7f06udMbyNsIzhgSa+hET6ySjyC3irOfZ8nZSMbq4VHoHvJZ6tTPxgSZ245NFDQC
hERGeXvbRvV5v3H0bzBhw7IZHnUGQ/zMS7SrGXNJrfKk36wc9YcloG9DXzAGbQwUa+6vdGyYGrAa
BMeG1wRxWDEbGIxOYBrKeefcaUM37G5QVMi8Mz1Z0W+gXw96zo0M9XBgxWsW4oB7GqdEKA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
