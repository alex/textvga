// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Thu Sep  3 22:43:30 2020
// Host        : T460p running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_800_sim_netlist.v
// Design      : c_counter_binary_800
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_800,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [9:0]Q;

  wire CLK;
  wire [9:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1100100000" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1100100000" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1100100000" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1100100000" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "10" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [9:0]L;
  output THRESH0;
  output [9:0]Q;

  wire CLK;
  wire [9:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "10" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1100100000" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1100100000" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
WWhF9j5Dv0CaoOpc173++2wYovz8nCtawtSZS08SpD8BqEP8N82uUwDQ9Harfw2rAKPx2jFIF+Qb
e3qpdVsmUeYGLFy7uZfSJF7R/oMI/y75tdBqPs/izYb7R0Nd5L+9/g1/7C9SqrqB2SexqDBb1VIl
3wTPgwT9FRaJkcIr7Mg84boaQSoIXfEixnZNs+egTTR+BiaE+kwcosKc5v0dqG0BovV+3+5U4mRD
tV8I2Z92MuPD5HmEII7pIr5M9omrXBuxWUbpyjvpqQQgbk4S5NXaEAGaF0BpV4EqzA4kJmCIRyiK
mkx8ac3U97f0XUx69qE5mMLA6oobeuuxjsAX0g==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
xTOpjYBb4z+5z0w925KuYUUgD3Udn0ISjv7//n7C1LHRAX5S5PIZsf4n+dzESz7Et29gl8wS2F3f
8OM8tZbaf4CJsT+WEgh1vQDQEQTcorkjVW+SBUzrzT5qeLDj0mlvXmtjAuFSwzELceMPbJNvSHhJ
gg27UOkpSyVo+NbfwKw1FaNXKyCWTtl80hNaUgoc4U4EdJmfKKsN7XEkFaFDq3pz6Tl5hXUQ0auh
+EzXZ46thc3c7D+o6/pPWLs8cMd43h7xx1Yq7Pmz+8rrSx3dMUppokIvFtXtq4/cL6yhM5ZuuX0C
4DT5hyMuayZYOMWkbjZAoyVKOdEvL0EpkJsHEg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7920)
`pragma protect data_block
6daZ01VtTXh3eYHyZRxWFl7nZATDdVkuK6CizOY0f90uH6c20BHX+TQZWuozbzGLuY8+IxlRHo0m
Erck90NUJKivz5d7N1tQ33aCs01VhQ5RCvqdkIx2GiFgdy17s5l5BwDNp1aIA9rzhm7IwDjzVZ1+
+mY5VTtSDVAWpprTwU+Qfq83PfdYmEitoSADY3zW0w1MR6cWW15VJZSiu3LTuor8v2lRhSVOawkZ
+CMx+LAyCpNdQ5yAy0S0fpXw/3ltYN87/2LCHTVx5Gz5/BlI2b6cEg0OH2fc6oaa0mW9JgAcJW1H
t51pVlJNrbr5SwIuI+VbrbKlyh9YArNB2eLNFW6Z2MY0HHOSuYX0Rr0dquH5Go+2l4jrAACw+5BG
p5rqQY8dGauvTjZr84/h3AUP/mYJPK8LcbZOnoS3ySCVHyi6onJrri9Zwl1pUmGnqnwhTEVvapaH
Ccg3fsb+cbTmCy5jbvtbDbv2Enz30Ez3oaev1l9g37j/ZH0CnLM7DcsEXeNLg0MwMUxfPIcNCUKP
f94kRAJV1v+G2pqEmtzBq0Pxsch8M4bFo7H1gakeuUGDhDIa6r6xvKUH7/nHFEedWJFNiztskw5W
T6Hnl0OCSoVqlA1y/5Rl9LplzH5myGcJpMjT6De44k2IDFQkm4Lcp3S9vi2RX22qfGl+i8Y4U4/6
xW1gPkxtQ+9faOFnq36xagyI1ScAJdOjnMaq3R3WW67t8T1XxGjk5AvOhKgDK0n7rDzYwbN07RWU
F/uT+foSBkXEgmnQG52a/Q3d464Im32CXSBL5xkqHCQow8aKw8cEm1k3M8Y+mPnZSz7dw1auFnAm
bk9kaLKuwcmzK1yt/K6XqzPSR92klm7+RQWb9hIHWKYfFHxHbphImbwePmVYx+hzqDnA87e1yRXw
gj3z+lQok/2PIicsE+dKt38BbjsA7EaDs+8SJnve0BY5QjbnZ6fbOsaDyX85COi+m7pFk6G65W7r
O1vhvRWPTjCwClb9/NoKp6AfNFRVIFe+LBUPrCkk+TU9legUWcxWRv6Ux3p6X254hIRsnetP3Hy1
3RBiF9We3vchX81zppPKLL/Ivufl6eq6u2ipLBdAhRBk36fSf2+kO/+3C8c9+utOX/svmFyZCH55
rGsgaRruwwLwYfTVN61YTHkX5DZ1VCDQSuFZm5GfCH8djStFCLody6La/F/LVvlHoOFcehP1dKf7
XTJ6dPbJSGLESUbFMLutvRF9zqqQnjhdpMuASo8XujaxkmkEFAgYcUVIZ9YChW6dM6HFEdTEg8OI
1VA65gUPOXN59t5dTZj55/ffbwa3bc5j6Wb0rTnMA4mgwsowY4cpfIDY2KR5UKRLBkDxz/YXZ5ts
PmfGT4OMsLCIYax557umsdWUDNPhvvpz3ZXn8yZO2Ugz9Xku/wQAwHifwRjKHC3AiY065z+KyuMc
1nAK38cD4jkBW2UPE/bV2T5Frfgk9dGrBMZtYGf/X5BV9/OSYx4LJ57OIRyRqMLsFuJODq1pC1cM
u2TTcj5IAabC1DYyVmAdBP9x7e9RVV/NRQ/R5gY8u+tnGhZ1mfKtPR7bHObyS3RJtpm9T8WY/Arr
Z7Y5+lGjy6BWI2U9p7NhGi4nwToYi0mE14YDVQXXPW7viMtSWyQR/fwkfJ9slO43oX6f4X9bwuYC
ly791yooVcV6/GKuzHkoO4ignz7mN8FO4j439alAkUQUPCo8QXPn2JibxTKE5X/QAgiIBEL/hxi+
Evzcyy+WeFMwdFZkpKZ6efleO8cS+l/aTh+F0f51svJ389Ix2pytZs7KjMVrlIheR1yi8+jlgW9D
/u+Qt68KjDo9o0CIWreeRdR60ZhnbfNz6q8IBgveZidHe9J1kJNOiGXgb4DN4PA/NnhUSArSYuDJ
ZjB7B1RPyTJVIAtY5rOvCluYiEfKYWJhkMDIPHPcw7XRIZRSVEb+VVKQECbtrUkkJGhZm1uoxBUG
RLE1PDLitXLoQY6pbxt4kvAfnSWi01AcEjIIPBUtGJQ3emyQPbD6B7uSyDQVG+s+3DxNMJYPStZm
d0/zIQKsRvIQaB06s1WtbwKEgIv0UonINPuYsQu45xkme6ECEt9LByKm3cOxgpapXSYwdSavI11U
oULrcOI27s6zHKKVFyNbTW032PhLmC0BfZLTO4FhKVk8Q4EKsbykxZuoDcXCLnj2f1egAv25l2oJ
twcTG0jAdXSIRkxlKuo9GWc83c0sqIBsHw6c/eTw8qGdd2w20rOdgDxefswuBvOt7nD0mOlCNU6r
aWusIByw/VrDZ1ruCJCYxM1v2PaFIPgglU8bnTJUleIKUVB1auzykXRNwHtKtI1cybTZww7JoYSb
uPEummlU1rGsBwjNs65PxJdEV1/+4IzatQuoSat3pn6esPtC6jQoRkJaz4qY48vQh4n4OKQEX1z0
0ztUoINXbNV3KABPIsd+VRs8Z2DW0FddBjJIY4M9zndwTTxzjFb4rlbfaPVpEWU7bugOvqghjimg
8rCfAbH5JwRqtVNBBNMJxI0ZXsiei5W6bGQgNQnLuQtjiKViq5WU474Jw46FoDarwpvNt3gPHEd5
g/hGB3JxmuBng0MpQo+NV2fwKFk//xiZzDE/7+SifGr7wD94d8oWJ/1nZrJ7fD6qtvoQSB9g2xF6
6/ep0fPHPH9kaa+p8UFtMIACha7xX2r7NJi9hq45n6YcObF/UuaZomZpC50hk021bmzvar1KUflj
AIDOuMnQfDnhXEeJOGRa95DCqX2GksUUmiw7XJtJP36oN1rymD4ilbIl7m5AmoOB7jaeqSKE6/mI
kUgEMJfSjr7GnGvkRm+cDqhEYakDA4VMbPyHlU4gL1cNymcYLGTlWBghiVEBKy4g289ThmclHig2
UmtGPwZXabQ50BhsXxOvh5B2f6jmDUO7uE2pbUl3LEusPNh02EAeyp2wJkistTKHltX61XjALi6Y
hsejDJohZAijoWYySEMO/gYVeZuqGaivxVRTJmShOngdNCRUAF7+gd3CrfLHjYiOYS9E4RsTfJYR
PP4PBYFDkw0QuB4VS6f55oC0mDlI2UrBq3d6RXgOi3ZD3BYKJzlP5V+Ntm/6YJLVYhQinPgIM4hQ
jcmHjAgs3beiOQShdueZEqbXuUkYU5gHas8MGDjwBrgyel8n6+RBrT3LpFMifkXeBSsS/0k09FIf
tAX83TpdSJb3osK8wdL4Xc6G52syKZL4o7exUwzEljUJnBRohovWe/aFMhtc5Bumq7loxAv88Jfw
BIGIxVTxzHVX796PCU28XjvQUuFwF1XQIq5bCL2mSwlQEhdbFhu/cOBKiFUvowgGDVxAI0L/scMT
khd7l7/m+36wbDdA6n2lAiy//hZzAcdAXFAEf4v8CPrKdh/6Xr1LGFaXPCv3jpI+MjJW4k09DuA0
S7RSdedej8HKC5S8eSa5mE9EkzJ4hlvpUApv4E0ywhOGrF5QWaCv9EmPSedbHA3kjbxQbkh9Qeo1
f2zMB6ApwMJC+x5qzrsS9znFuHUyuZS5pijjKLicrirM9ybLLQBOsLIRF1GJpJrxE5c5Fm2QpaBs
o31YIzMhvtt6d4rtdanW+iRQlvskhTxqG4Az+o3MMhroeYYOZCkxKHqYBb/FnKE5YBF3V/pkRPTt
NicEK4Etck/MIJ9Cb1zkHoy3ll5jLsAf5bMbKc9kPNU6L6NpRCPkg/JPFPNFQ9mNBT08hcYJZ0Ja
wiItJYxToU9VuIph8V6AAmlgxYk+f1xRPGX/YA5rnRNVcJsq5zvqvyYxdWgXTS0SJAKIQMNlo3Fx
huUJVLUZpfaDTAxOQD82ZFcQW27UuwY/XFFTVbuQfNN4QObYFkPrcG3vK8ofknp624fFHz7LAphX
fT02DMvug9E+mrwJ/erZa9XxzDKgmCHkNKZo932k+M9IPB7TbYtmcKh1/LKe3yZjHGGW2d1Hcwmn
twV6LAaYUdYRzZ9WbVyefpeYIBcIgf9rk/RYb3VMXdZujfAgnTjkmP9vS5VhnId+QwWARzUknYH6
KXjU9Xk8KQ3L3afzjCWFscY4b55OV1bR+mFdeZSuni2YmkUKf9rBwvzkflpczYxo6KwCgjr6IkQC
6kwjN4/U2IwTPFYfn4Zwy7rxLJE/d6F4T71UVKIEr/VKPnB3M6m3yut1C2IkdKiu8aT1q3L7A9fl
uTfnqtuzZjpinAdFDxv+jTvFM7D7hvUcpqa5/WuuYtO1raSp5oxfbCBQYutzkg4LVZkc8C3wgcDP
SW7FsOFzeWcH6wzVS+NgGC2ypz2Jtmxp31DlxtAGD1HH1DE3fX8BboStf4DY1/ZQRxiaHMISzC51
85Oc0qf2xBZMi8z/doM1EFchlhWjbIQlx5vNvSevPUNoB8KGVaIOlzAwwuWrxeIFFXZymiifsQ7b
Gw1f73FqWaQ7jxGRkFdvm65RxZS0jmNqvObWPW0/jTvHv0CV+y9oUR+MwryYLdwjw/z2AUL/6eMc
FNbMy3mjdTYj8pvpOnMkq5TPg2ro5/7v+vGFhmkght2T+miZzGzSVwoUlOVk85G3nb2lV3xriJEE
lH7WtnmU0uw3YN+Sgwe7L635jLRqht+eh6MM2kCbhkQz3qL5Zfqt4kwLBb4ZnMtb6F1IcEV/ju4W
ZDdV8tTZghim1GQZ4fs9G9NBo344vVLuPMmepGyS6GvKoQuxn9cdsmv8+o/NwOjlwd+gQw22Dlfu
1axa/2D8o+O4dLYFv+7M6vJv3MZBSLtYkKBOmAKHbqRO/+NqcYByirB+AOQMCqpC61P14PeGesV3
6WhBChLvX5PXos+YC2Sia9Ua0i3LT3TuOD4pgxqo8O5y3/4Y+tPdEaOC66OjZzCyyt4EEZNSdt6R
81ctdhTmQiTfaY/t2Mn/UQotsHqmGi1rWQKHWxjYsEKbhdmCG5LYNg2D37Q9wyvbZsGGLrQXXUmp
szwYUQVQ9jpNw2O430dTVzcp1PoRpwTSVSK+iIiy1WasPqgWXffLbrL109atlew3MKYKgxzA5w+L
Q7rV9kb6k+w3tBizF2rY3wfcRwdKSzX4y3y5RPh2hwZvkJO89Fpz0SO/3zcj7vbIFxS3tB1V2mM1
TkCYUqITeAcuIOY9fxkcJon8DC7RAi8Lhvg4IbHkaUu9FPPdtn8FW37cnaJEGnvHR8LllrrWUh1V
qG+sbRBZ2sfaELXpc1+4IWLT+AEDTww1sOAJjN8VTO2tg+eZMLsHLk2QA1yfj/trBU5SdJGOjoTo
9fKjAGaDq2u63gOn9DIFIYrdO21z5LVjkufeLVMp4RwH0aBlBk0eWLLS5GwAM5YCi+3aBUw7xfM3
b2ZxqQybrTB5ATc+GndYqYUlso4GV7YwDAnR2k90MC4GUFUWaAUMdyrgeMbslSerTxK6WHg9pRQg
4GPlExRccSDekA6O+RKWrzg/9fZtTFwfuzwAjBIQKYN5KWTY3a3fRRyJS2MhUn9h+Dcbq4UAgiQZ
UlHD5DU3a/GG2h7aENj9F5Y1n5oO3x8dFSeuzlFxj/gB5WCYq3618pMdDhOfAqMyyCICrk+PlKlH
TnMuCCyxU9SSPs673T+AAaAJ6AqfQokBBsKrHeyIYQMDKF/TvrNrlVfJSrPzbddoebWdg1Zjo/S3
fUvC+HqA6aOYu4EuUzyxf0r/EHwbk4bFOLzfbW3whbLA6ZOsl16utcY8bfnuUwh3teIFSkQP6cQ7
3TaKuaQiwSf/Fjy90ax4/yNm58Fp8RIZA092F+piB/mkb2HxIYbl0u+1t9lFqBxm0G5dYTiX3uJe
UFxMqNki9y3DwWA/ylBOjikgjOBjSOp1ns0rTTDO3wx1kEiJLgN+srBgKwST1yy8cYYJKm5xf5+U
uzfSvvB8izf6Q7B+wiQKD/i/7PehvS335axLbDBFsuAoEkSAbRvQ6Qcdn8s01JfpG0mu3y2LJ9lg
bz/oKLx01OYFiLjt2boPduTqphWRSLmeP3DgqDAGVn3UBU7HiaCoMriQZ/5+H9oM0o0qZ/f37JL0
n8/vCCAsHWwa3keHb00jIfngEJCPAAvvPmak+0HDtPjM1hmJygGSGlPjlp11BnQ0K9qvSCFEt7f5
4qmFKbJlksqG5R0lLCYh9/ScPc08swX7pKVzTTnJmG+A7x5asKmOlOrOsYICkCg37UDF2B/z0bHY
HPZh+k5k8NfA9BBkZejbG0rVMZxlbdbqnFKWhnv6JL4csSYgYVQxVq/WZYDADJOVnHiSMCMkfc70
XdgYiNxdJro62jFdmZALvaWpfSAzPEt7zj/9LZ3v8Su9HzEwAyYNnIMWYk/ah0bMCCzYxNpKJFvS
hxubSkFghSkwqp6yGk9lI8vd+rVEctpVgMmKxdnIgz5V9n2raRpiH1PIMOJjMNOHR/0DSMjVyy1+
1FGi7XMNk6LQRx1WTJNFqCqC4zP+JlgHuvTJzlJZimZyCv4JUAjiqdbVkWZR2fxvhtrN5FdRb7mQ
/2kt8L1JO6rDQ5gl57IPCIURcCq3PtiXBk52Wj0/nO2mdHrn/VhKlQzpK1yljNt0OgNZDsKngg38
EQbtwiBgZ3tl79Sdgzr0yy/ufKFpxBXCW4JlY9oey5MpL1WzspB0lIkSxe+ieSPdARf0NQENT68a
EBYwxAyKs036ev6n83PBjhpNpv38cAIB9MNOcPuloCPnjH7MY+T0I5MEbbGbxbz9wQKIF5Ql7BIl
f9y7q2KIaiYStg3/W20utcSL0jicwTqxT9D923exnOdLMXhfCN78+ceFzM7gfjsl4PSQKcw+rEyC
SmkEXIqZ7rmpQ3rJdYDtg3pgMOQRYBr1Fr4V1KCwsN4rccXwUVvvzx8SK2c/8FkHnfydVh3RD5ia
vLBUJoJMmQnRA8Ck9iId5W6lwJsiyTv5WHAUgtOGrJ+uHip1ovOpYoAjSziQZkAsWeuPOzfmlPVN
IFgzqxxG7ddPx4R7pT4scYwIpPnBrLO8gdHoQoXBczwxBSZ1Fbf9tj3VLXjaxXQGTpZB0uMt5FMl
nmO4aWafx+UXsaHoL1fBKBp5bBF5lNQ1IUuR/dZhSiyXwpvpQVnPJlM37k02RafZlglSyzMn9Qsc
oQwLp1RI9Gq0VLmmkiCcNrZcdEi7HrLCrIZro5Rukeqv2NRQJBhJ3dphkMv/R4bavSbMrV3OLrVp
enn7sW4LycgjN6/I2bByl8X3qroN85L3TjkcX+L8wYBpv5L4HcSqIo7DbSYFQpu2ZfHr8iAF/EC6
r0iiyrtd7QWYO5d/63xEC+K1pwyOHrUiqOmtIlAimTpXpkUyOGg3sT1WxiiHLVBOvd/8yUeeSVph
whTxpcloy3mptewGMFqF8ADfcItg0VT72yhTyq4lbvKkh4L4nNC8ipQnRD09Wee6DS/KDXn0MZ0G
ExfTyoeruiwQyv+jO2c2+ZURZN4nKUVX/VrIibPiLm8Ubw4Sw/vkrREpQ8u7YLf+Xxol6hA+Kbm8
+581C0KgdTbG4IeJHatJt66gIf5QKEdTLGUjjyxUkq4SavEEtRbZckUkZOlhymilC1v/hERIVz7G
2Ydg9R7D+QzsjhBxtvHqgTc2n9/GnmYTrwXrrd2V4meGwo5tKqVmsTQ66GM/kEzQh98MFepdxYdR
sdxIOlYMB3zgJ1O6K5t+AmJr8HRMaxHF4zRVU7AzDEY9ZBlZowFEbPgSLnbm7WNWhudfeYp3zznC
le58+2iuxNrgWZ9KbJIL2M+2lOaPzDR9YkZn+lI7/J3UNyLzTtIHItl8Hhb48E9OfEP/SbZFGTsA
Hr6rqOAR65ChLO0BmITA9L2owkf1aoSBAy0ORm53NIzzpl1V3EK/nkyMlW9VeIovtPs/vkqswLkh
Utb4kizySztRa8Bu5GM+qcOUMTEsN4ZXbmT859i3EHmkNWVkly+Vr6ueIFJUPSzW1ZRZJ04uA28r
SsPb/1daX/bbqqj28Ce2gcYvIoTqXKBWimsX6wFqMkSbNmDunlCreymIC39XrVb1A56Q+H7UtnIY
W1II7rCHFJCG0xcODl16+/eqJBYd+au4ujK9ROfRB6ReX+MAf/vuicCqMp5rdaVht1GZoytbySSN
DMyTM6iP7yewhb+YVar2W/hJvzlLmbaIwTTfct8iE6BtHbioekcHuVAQZdET6VxI9nWduMuTFJja
np1xHMQZU7Vz03js5scTm3uWYgaBep6P8yqOTtKjqtRShco5D54deUlP5O7U6T/gvMv4binJrvIm
MJ8uZIbfJOOr1JEdLgltCa/vFoLdnWu3WEDHCL43Ln6lwyle8xBQrg9yWYf1n4rmTFZFSo/vP8XL
8ATOEXcjCYBWwWum6F0+68xtv351/37aQPou3MYSroMTs6rEkLfC5LVHgZJbXM8bc+Ua1x32CGR0
SoCZsA/gPFMCW6HCi1KXyt+svCKab8gEQ+NwXOmUquC5djITX7bBd+9D5n8rwlGwfoSYF3rYjVHy
znfFj4bQv8tlbAc82c6bpp+bdEVAFiZK1vNVRljp9o+FLf90jhzBhk/Wg5TD/++5Ok03KbNyJa8C
Of1zPWKxNXAh091EUCkKzLP3ZASjh6fwa7jIsDszCiW/CsN2c/u57IJ6Fk5p06qItAeZd+RtGn1l
9ffCo+9y/racgutvskUqxOIU6c2U4npfM6flZq/ztYK9kdZsLo6WAZ5O5G7jRK7j1P6j07hKSFHx
laRuppfCFcqz1wFAIqDTUhyyCCDwDRDfCfkuv/1Afy00k7QXpDfhnbImcx86N9hX3QzRL1SFL07S
q7+K30RbPLFiPJ3e2TZLHu4xp4gSWdDvFjSXIPJIyFyVl+JmXPNGclqs00xTUAKz/n0east7Nonx
6EpC/n9UOx+0BiCrixgNHNILp3U/m7x2wzB25bJvjignR8BpbGB8ffWWXK+JaEiKcWWXWPvNBUtG
bflrSXtasqL0iZ1cLI9Zu7UMpjfUFxTMlsvIwbAWMu/kPRrqief5P91/6tOyyaXJGs8oaXasCmRw
BFF7kLyY1diGlojrXBhkn7jNpkuwFXSSbkZZpfsqvs0dPWdDSfZ+0hxRiKNeCFTMFT9vtjwDwd3o
7vUUwOOyV8AWN/eMC9zURaB57a7Nu1ayA/7opolmr8JPVWKaWoxywa0crwmpHIM7G/XWeX6uhlA8
Pd5aqJcQwlX3eNzL5sS/hHU0caYDyiA3cZWApEjYvSXhYskhezhQcOALTWyHo/UI0I8y2HQZjbVQ
QwtoAvFE76wobDF4MZ/0+pJsWvEHWIrS69R3CJt6ZaaWjJ7PhYHi7KlXhEazo6LM83CbEL5TUMIX
9mrPwfmIYUWR22j2sKHbHEvATRUJyR0o09eeB9JgtOjry1ZW/oydjHtA6/QhX0bpRg4hjQEf+0zM
cZ9H5uobJjau+7Mw3mGhbYfZMNnNYp+tGAJgUUljl7K76SeZNMaH6OBxalr7YSrYlwD8SH8pUkS0
r2XymENc84ruLiO9hBZEfZOYk4VjBk8dOLz/amj4RcIe+eJST2OFr+/TWroydNFj1gZwLqBWcuHf
R3xUauGaEzySbpynSAYJtlTRGXSmP62sQVs1UYNAPZowTVrP31U3C0AWiLhnz2A2g1n0zU6pA8qz
UiJuMDu5IZY+eUhchUp89j+f3dcqLGLQtq9OYNnHA7BNKV2K/TfuVWjOU6RUpMCfp34RqPDXv7W8
W3UHLK4Mnp9fNXulYwCHXWiOsGsqLvhTNJidWjZPODEhFZjdwnXmwAIB8OTE/0At6lHHuJIp0YmP
jN9eLOkQeT1rq4tdRtjzu/2/lNmtiy34SPYFWuAuc9DGkFywZPPI8yRSDXQ6x2Vh1XAm0Vgm+tqx
Mc9h+PuPxN8lamVEbzwJpUdv7p0SKW8L5eSR44ZXUtrGZD2uvC9ah74AfrZ87IVXxRqhpXBE/NPT
uTfIwgB90FbJsspnwbKsrE2zqpA8qJNG3LJ9Vgfwsp7lJNBP+Fiw1l3uF5vFKe7I9xbhxwsiP+OW
XlL4dnKeqYJ7uORf5zwJWpt+mCr0Aq170rd6QkxpCf3AnRmqQDPQdoCav6LfHiBvJeAo/aBvRQpt
S+eYhs9Tx8pu4XneI9eMclqj30jWggOeu2FEjzPn0SxkSNWcoFfB+ACsnX5lOTck6duijLPIXKB2
52xZJRWEzqth3Rs0KgidvFhzE77HHNQu6Z6Q7K5DXmya8nJ7Wyzcby0wv9U1WJh3wQ4zr4L5G9eO
7VXkU+9JTlfFYo/FLpkIgOHWb/TYbe170UEqMbRexyxjGZDKvycsQW/tBle6eSkIn25C2DI3Tj42
UeUU02KLfHfLElJszcQ9ikrkA1G975i+16Jew5z1tY1/LqafvI8KGTl8JLnzTlH31WBWxUPdNKnM
T+kGyNouQnHsrBv4zyHFUViW1WL2tSEIIUaTLte6Ej0xoSXNJZNWwlkZnNlROltMADuvKKw/+E+M
FlAmzQ61AS9GZ9v0VF23bSC+n34Bl+4J7m29PZ2cDr/AOi5eZc04UdtzBBtv1UcM1gNnCHFDGw/I
giSibeyfUATFkgvYx2bHed1k3Za043l63Bm27aLOKfMswyfGO4+EFnbNX6NPA/0S2jOE93sX
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
