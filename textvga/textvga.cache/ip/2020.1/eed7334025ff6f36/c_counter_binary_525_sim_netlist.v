// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Fri Sep  4 01:37:03 2020
// Host        : T460p running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_counter_binary_525_sim_netlist.v
// Design      : c_counter_binary_525
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_counter_binary_525,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    SCLR,
    THRESH0,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 thresh0_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME thresh0_intf, LAYERED_METADATA undef" *) output THRESH0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [15:0]Q;

  wire CLK;
  wire [15:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "16" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1000001100" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1000001100" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1000001100" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "1" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "1" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "1" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1000001100" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "16" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [15:0]L;
  output THRESH0;
  output [15:0]Q;

  wire CLK;
  wire [15:0]Q;
  wire SCLR;
  wire THRESH0;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "16" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1000001100" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "1" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "1" *) 
  (* c_thresh0_value = "1000001100" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(THRESH0),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UzTER0o9L0HOAveD6zaIqeDzpIY3gLGlpGIgCL2IvyV1UKj+X9PadYvsIKwZo6J4jm2od50YMKss
RM9DnI9SOPheHi7CCkp1RIs/AYlg3tGbqAQm4RnYidiDM7CxX1T2Nzd4dC4p6dmvSGwiC9TRv1/s
AbQYZC6SFv/yzVgmgUKxvTZGLxOElC2XMGurgKyWv4LipZbC7Y8RB7bE2UIaO6i8TpgLw3zIpdJH
ddOPjv0suGS349EBeUvvwBwb3yOnOSfP9E43SCGlsCA+ED1BpO84H6LopW8eOH3cx+F468D31Q/Y
E2D2ChxDGkYSWaOsGkAgupTTiRqqzphnIoFAUQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gai2wneGwJuyA/QsAaczhrNjze/E3wFrKMoDAgHIS3EC3iE5lwr12w8/1qB9+Aop1DwqwoRbQ0xT
/mWbeH2yOy/lfoZOan48MkXahpEn5JuRAS+tx/KO2yUGLQdhTyt1Dazj3o8233yKrjqNKAreNoIB
CqYYJHoF3IBAw0SaYXujTihvfRHE+LSjHYqdY5XLhjvKk5eqOPAlRKmBkG96juuwGKNIiHhI0RxT
M7fNJIRl+OVtePqPZ764o1fI4AEO7fgPK/zQWqdF3fJM+2opvIe9255AgqApXTuy8/GY5W12NFj9
INJK7jfAlhqSwC5doG17rEHybOq0lwHsI3wxCw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9744)
`pragma protect data_block
IFFIpWwLZPa3E9z6ijRiYGKvvX7lfGgxI862Xo7i6yaciI9fwtUTjH5dZNVhOl6jLHJ+CxpBiLnS
RbWzQcS3Fo1Y/Fb7abEjga4UjVcPH9cilmc6l3Bf1X5O3Zb2EBQHJ479iD7Idqfr+CRDmHGj/Dy1
+A3NL9DESuFUyXsg2RREw0GzhSDisaPVrzUbTy5tlhcG6/MXh04eAqSaELbUz4wxws2yNAcC9Se4
VERiQflRvfcjWksq/Om/QKE9nAhoLJBgg1MZNPGPbzVBVAbHKxBG9ES9BVZDOmbfbVkm+dgixU8V
2JyjvVOC4G5bcmbYBiGaaFDjjqUlCNgvVkpP9ggh//jRiOx17TjVKtATXu0iUpsSDpkLP8xqrYEg
xdPleU1Gy7KzkJnE1KAUN2VZlsrn9CaD4ihNErrzDb2FOwVtvf4wYTOlxAizSUL11ru+MV4o73O/
59pAGZzwwWypqKKri6a8guqNDHd0Ys4tcrnxSp46qUwmwN7srSGcZOHtlUjzjL7LQMcqiDmvzu42
LFTgHoYjAZ00QPCQIS37Q3FMNW5wkWj+9LCQjQyQSP7WCmO/QOGjIAiO3VojlqLUXrqe93whaOO7
fvN+lfNO8+Nhmm5Tm7eqP5yQ8jiB1YdCEgktpoJ2YMS1duuUPXHOjOHoGq4F6V5YbHxpgW9ryPtp
1OOw0163KDFJNAWxGL/CSUzVc52RrPpKOw37sLn2+gXv8fPRcwy4uf8uJdP6n4PUBZxZ6tn3OJpS
Zh0QO+e22YdsVgPHSvjUup0JjPdb5ZA8umMPjc20xHS3LP8oimHCXvOsv2/luh2eZd3fXeO21fDX
v4sUOMtLn7fvXPMot0f7kzSV0OssRU1LLINIy7XVhco/ZwaI/iw1pxkTQCiSsqthuCOs7lYv/u7v
gn2wbsb+eL0KLCuZTirugiZVeF409G8TeRKqKPYN+5PFyVZTlcDrkv3HCGR0IYkTQOsQHJ7++ORf
mtqwlljlmJuE/31Dqj1+o8cb/dC0SuAjZ2FCIYCN6+5hEIcL8frV65W2GMG9hceh52+QHuu+/XM9
BNb8XT/hOp24/4WYkw8EGPiDQaQr6arVPT5uV6wGFCa8A4FEQPDPvcBQ090LiaFwZi3QExLaxAlu
eh7FXJ9HWoMZ8gVQmidnL3hQBeO00LqDBW1sy60p7GgvACHKCllsjOgwuIFT4jO8PC00Q4CNuow0
ZAomr1kfL/2sBAjcLIf4vFFb/Fp6AszrmZsXkD94g+MQVj3j9gKkcrG8erQCzF7oTTSYBKYRvTNC
JlSMe9v9EdwBZXwrxPbtZjKVjIXGenF8eZjZZHX1u955esIJljZE2uvBSsct0wWNu4bKzhIiH0Pz
tMCoENEwsjAMkw/Qgijm95tr+CeLSzglo9ZW/i+D/Nj2hsDG6aF292C89a5yLjBmwoNYYWNUybNv
q8iTx5+jNx89xDvLvERC6a7bCmwpe6RtuH8zjbcgXDRD+ZoCYU1ZFueaBzisoIPcM9Fa45EXDUn/
yggUIUYwyMvNj/sF6MGkqwQ42qVTv8YFf6aNIDiCetqqmfNO46PyREoliAmlma9C4mM7T7THHbzx
Q9ehNct2ao5FGK+p9tCETtqjS5/UJlPPwTIwfLhqOQk2f0djtMwgK75d6PjUej1lEilAY1zIir98
r0PA3lDoU9UV4t+AoG7ut9ED2hAlJ0ZsOrOxUAEoFvlveokSsYzEYUXGt6xIVDUrq7006Xh/9Itr
Voeh9X6UtSlaw7MO5/l3qrSxeb7iHN028jpUqaXwTjwF26yl03TOEC7bqqfE7pTTAyZlyr9vdkQA
fKuFG4fmVU7E9ZsGP0GHajajwDiF+cGNEZnBru8hiuZ/nKJTykJyc/M5peuufcFHvMKRJlBePnVK
+xXAoFrx2fiRuVWhNh9tZJk+y0DJB3WJp0c/41vmbz02QxnDS+SVihi+smZQxkxPe0gMte3P166N
e/Y7fEBq4axWTrTvSbParPKIewWksmwGMWwH+P23ANOp77zE/fpkVq+mwqoBqivSIs87UCTufusf
8rjG+T3S5fEf65QJoLRq4a32h3yygLaDTTG3Ri5zcFoS2NBiqGoHsLDJI8QLdmGlN+LAZ05ko9sX
2cIMF51EEpZ2BdbMv25i8z+Ma8TmnyZmPVxV/SW29RyOmilzWqfkoh3U2VbyeJ/WwzIxx7USAN3n
RBPZQCDNRMydAJEEjZkvwJoTtIWwEV2TXOHXQZ1F+8c/E+EmB2NjCfzq3XQ7wxDTOWoAlMIZ423h
6SyP9VLyKO6MrHXmAQxFT99O1diofi7vz56HmIp6JMx2S3B0c+i97iAoaZhmy5VoXCGfeQPC/s5v
MONG3gzEX/gddyfunY/3VRY8k0KkpztZQs3x/0A6TnPJTHzLGepSrphYGC4aZyusLKAbGj7k3hhs
Wk6L0JSTmNcAPyRtjYKaMibD0/GckUDfmkLq3pGlnYMRpRCmigz3FG4MXiUFq+Q3tgJG3TP3COx2
x1CXCYFL/b46K9VJdjakzl+jivl30qHXiwbWk4s3wUh78sohZx52aukDTDt1YUyo68MGW2d92av5
6TSlraMPrumpF395YwhjdV3UJ4B2+K+VkOCbSjr146mXzGOHSDzeIbpn6oDMsWV/UhicCtrdFMeI
P+uVt14xwauneoYAZrn1U8SnJcBferB1Tyl4C5eCYKzJaY/wfOEwW8IPxJ2SrXMF36OYzRLhNiKo
XpNHI/zGLxiK90SRnO1GZmXc2xvJiJamGH1TU9JcY0r4WsV6XjilJnhX2slKL638nmeSGkokXvql
0KDDaOXEO4a8EitXzzNyOnZ+WvbYmkjozY6FC17Re+V0xKNKlTOettj4CtcKyvXX9YpfD2RdT7NG
6+KBlatoShTMWpz+/eXeqUmMXGvMaDMk8V2NAKEK6xAhEls3jdTt4LXg+yEnABAQWSxdOYU77ylZ
ZO/fR9cD3CWaA7fKQe3y32moHnmCQfWKZ6jo5YngOysw2mus/jzLw3gO75g0xY/5Dnxgx+dkxAw6
8pl0iC6zEGnWzgFG/RlfTA09VBdRkLB9ds4UoOfoOhg0l/KmsyMiT0AL00ykORrh2c3UfmElA5/m
U3pxdJGte5pxCF7zk+Wun4oEHDbFItPfYzP0O8AQMNscCKidu6EoqCSoVp2HSTGSrYbSWxVz8Noq
3UkSr7fm/Su3yO9hfAtuFxlm0bYAdv9GH/qTeEMaO7p1hw4SZxwpz9ehxe3A2tq450hWFMsOhGqM
06RmjgpJexQRau1wVdmvMazhfjQQMfOUBYfHKws361d+22YjrvzHH+ESgcVbEAw6U4jCPHxiXkVL
YBTMY2Dsdqvsmag8uuOq7MCpPQ5JrVA3gbiEb5UioXtktBmy+qpGwsaCsl7NhawcMnKvhPtxT5rC
KenDKTYhOfNwCxnHPxiuZ5Ks8oBOKYH6vGpfBK4zE1tjZWVMJxr4MDLfvwHd5o8k3vuAf9W+AGUR
/oxaOLOEvrOp15MLPZRZVhJxz+Ft3/u9TMjtdBT40cLIJVtGhH+LfZUjsbs5tnfWEY3NVBWVniL0
OER3SxxWQ/I58Lxao/vq+TeO5ec2L90PzMpxG1esRHC8veHjZmIGzgwH7z577QfuvxTBMBpD5tYo
pBLzzC89517Es/RlPTIrhDnTYGELvYo3g8ZrakSiIAoxjk6kDrnAYw5ug8jTbXmOyjPs6QxyQ1+A
+0yFqO4SEvBsMaJ6wXm4+Tl+pvgW9g8XOndEMZpsM5MMhHsc5qVAh8lXwvVeGXvOkkdb9PB8FV/3
5M4FYGhp7bw/RCZLw6tw4wixWLYMhcpHiibRnQR3X7epmu9q0iQn5Lob4ne1DVUmhxcjEFMBtqxN
b7XDHDBzM5eIh60QQCnMx8iS0oAgs8KdAmB3XXwPGfz96mu9NMGCL6IUprcQvgSD9NbmmJelnR7l
l9Q/j3uQ3XLEHwJG93M7Jy4O0wU0uiDnAtIKDdb3AWWxxiriNv0HW/ZlC4quc9dw08gsgm8fD3yO
at0dgcr/dJd//PjzAZpbhUQXIFBlMGGV2fUyoBNw+vrXelOXj/Lt2WwmQ7yAr0RdH6/Ta/WgDuAM
coJlRuDmMr32TIiwSJyZM7y/lQ37cX3U+3c9pw1KMmH4vQGm1puXfVvn6igXbSruOUSJD4q/BL82
bGGQogkotvi7SnTAJLeLRi7cRNutnQdJKP3vcnQ3sDNUIQCyBZik/OuI6FSWprfKCEqil8pPnlIa
DVUHgTXP0jhscy7iTVfmiXSPPh7EIO/cY1IdOMFAbvg8ON/i1/qHvY0S+QLTYdDZpvAOHimuNIvY
d+heuoez3mt+MWp6m4B3JpOG+0NdV7TJouHdAprgZkVDBeCqyntHh3E6fH9N1pc6dcAQdDJsm1Uu
czfDHfWDhWjbGfJ0wMp01TzTL3lLV0bjRD2qGqgBkbiWFnUyvucLm4icV2kQagUowsBBKB9dkF8d
Xxnuq5OBnCw6GdLXHeukbBjzC/rVtrVRIDYO8qpyLXfkOFuufS6RCYJeJTPF2GkDMj3Xzlr8gybn
xCVh12loAS+j9vW7g9b3McW21PNidUJqEwukEPmWTX3wn50B4l3nL4DJyq93pjjAqLcYX2tWk1rY
NJ4FZdU2XO9ABhekaGnpIHFxMMzaJCGL3/fywtzbhsSyphzscFmmfWK2pXdFVJw1BD7xc3GLIBBp
LvzdydMCJxWqCb1/7F22+hbIB5swPmJfqXptTqy6gbGIMLF9M/g67l+4PfVpkRQ3tFktKEDd9gHW
7nQcMVvweLrmXrQLvs+30Z8QG4WryUbW1DpllMql+fT9vepjk2efzcSc5FYphwS3/tYDuRwapghN
xj894UfN2ZcEzOqmF3JI4sXdt9YdaBQfo13/zIHUGqrxFQAHHIg+0TF5Bju5oMW04zwKA8gA0yDn
9IedsaYn1vx5R+oXDFtfnbh9XNfJNaWtniO5/a4qr3zDHGK5mzUIqa077IGzOht8FF4R5RjO+iMC
lpsgP2V5OhE8np/M7mR/FkeMMZXz3Dby6tZwqo/+Fv9d85uEo4KYu+KUpB7wRTAKK0DIitIOSK5C
JtyDitFCjwL+/w6+FiaMYCdsjcH6a3JA47lAfRCu/CSkoyHtMmZMfUSyOaSlWtuy6IrD8Y972lbP
Hz+EWOYBkbHkR2thvvH+TBkiFqKU9wg2cU+IIa44GxulemdT6fyzXMdofWQMG1bxRO+F7aKKeEHd
mpgDRcc8WqIIMUbgI7ftFseLlPo29T1UE3hSrWuaz8EJ7auz+8o1CoLrJpxZVoWTTGO04p383wBb
cdNzRAlCD/rieyxIvEd+GGOzwtmnKgy+wCzliRs9mNXytR+mhnVvAlHAXe4rEU1Qh5VnLpXf+j2g
6Pi7XRMlOFLhrB+Hsid2/FwRlOUuSczMUc3icGaft9e9e+enJpg4aIo0TlMfzM8WlENcufAEfR9p
aMrhgMKWKkIrNDhfUS3sCAoxV/UfMgrIRA7Uv47kQH8evbvOxjxROztR2luF4EjuoTo+cK43agv5
QFnsgKfSIhkvNoQnIEVZ+GCbSAqanD8f0B3/2zwzjI88RsMiXcxPPMDzsM1gcnd3vi4wyJGJ2pTk
DiylPtvV+quOSWk5k+EN8LvamkJ/YpSPqvaYW85ZgrTLBA73ydcbHsGdV9b9+N1XWRCLeum6W5Uv
C7FCBQ9ZtrLp1UA6pNi1hXUp0NDAt/PUYOuVR+aJqADgy3RZaEeXUzSWeVZR/jXjGiZi8NNYuOxa
XfNqHFh76yiKOYee52s9aaFRyXTPyuFuajMhPHQPlJDbKxusI2VYR6RXOtXAlbwDzgmYimS6EH27
uJJIoZ2iyTE0la5oOzC84gIrCPYdBuxs0TUB8bzPu7v6SrxYt/JJ9n1PwDQpATG8mgPWjJAVefZD
C0s90y+I4GP6noa0vTm+bkyKcnK45sx/dZmlZWqMXiHJH6mrYx9kWJdSEOp16wqenkCayCrkrO3F
4LwSnkLM2u5X9A/GqKyd6m4efeqA40DsYWKq8PnLoeEXUBzWxTzP16RU7rISeQAbl6LTMjwQWEnX
2Gf03+h6eBoye+XngkFp7cGrmKjb007E6XMmvkCfE2lzhq3Ne5g1ieFSmVk+g2kdfsE0rB0dysSY
D3IIktXDpMjST5Lagq5o6dbjq+ykv7jsPbXbf4tdyJRFOxscoJS2OsT/J/dpvn2cpEspSvsJCatS
aulujM7npHS/ElhVS7ejqDBZYPfuU9ko49Jf6SDQdPb3NS2HVPBDCIkFoce8yBh3jNCCqZxEa8BQ
gtTD3Bn57UoG4Bho95ahVTCZoXb9W55yTNImqY+8Y/kHoQrzMDM8pen0/49LXp3/Qn8JsMav5D21
JH12ChzhD7GXQZaQu9Qrm5duEBjP7BD8MLvd2dYdK/lak//oom7y+3CUEMDQD8JZds8TOdwMTLYb
AjdFRdnDAShNCvUMDQEeDVNg/xo7P11XObs8LRvvU0MdGCjyeCOb8oTSHx1xqRmVycWKu1aYl4NY
jMTRuNhX9ZZmtdCWNmw8rdZMptQ1KilGdq7yUlfFAbgyab3bfD9nhuxyFP0nIJNCcyH30SVIqs0I
xS0JiI2KYKlmsP0J8ITol6CKkGSyL4NVjJJxXBYaHK7APjjg2v90BNx1MlFoCadb2/F3Z42GfItS
WJMNz2W1pZd1ASTSll/YImEXQ8D0UHsoOTHL2DZmg/foQi9qyXoQC5zoeX2Cw56PdknwJJ8SdtfD
nwuvBMKtdaGnQ0eZAYjVYevLWrHw4Ukqcz2Po0NisJEi/JylwdGcG9eWEFTcNdFVtAgei7bfBU27
t48th7EooH0ZpX62PeSorFOYEvVycknogemTwjZeTmicLNOJ7sNlLFt7sSFw0hMRUMyV7GoETOm2
xwN0CXkNPjJG2GNsQ6hhGkkVHB75iVeipOSe5P7lkCWad5bgh9DFWnuo8c+q9dkIn8vT6diZWXKw
mCZ2Eu1UF7mK+O77JxeROVMTaYTvRrnnmhfLIjSVPP1J754M1QnTplR/EJYjCVGB/AaPYuHH+Ijj
vQ+sLTx2QsaaPjL2wrZGUkY1fowFsIFy4pQEwGUw0RYhmS1yo25DsUZgmFKsmF6XEBjJthiLAePu
sPNLPEPsrIg1Z6qoLUk+Roh1e8/Jo80ea5tU6T7GuWA6KsuS1YHCM4kCcB4QPiTASmcym2CnaFU0
MTb+sPrqGm5gTwz6iyRfoo8+pSd+y5IYpvogL0y2dgrU20A7fBKa1ElwKG5sPvHNv1nBNEqRty+3
i2Vgu/5XoguLtVOQwM401YuaIkKoIc2DasFoNVDKn9WjCfwCZF4y6Z5BIaSbpVPZt4RSCguJx2UT
nwtPUqcLdeBDvSxayxJsZvD8qQo7xvn6rnYRNJlgQfz3VmsPrHeLLH+Tbpn3d3FXenZ9QR6kN58/
ihYqyhQmzlvWp0An7QVsW8mjuZGDQARG94aV8J/J2Im9/KGLVRssQcTE8wsiRnTmRSNarE/Ig3yA
NdfpfjoJAfrsj8YE+I8uDitwrjOOgpzmkbijI4ZFTCC8GBwiscdBk8n4WfH33umfz74e6EoNDr/Z
MJYCKplAxkJcu1BIx/VCf6uyJaXGI3aI5VvXUZK22CcGwQVqZDrK2uc67tzO3fqdtRZvwCU7ewoc
vfT5G21QU9/CGBU+WhIeUIcAAj1WShsMMfr8EnIiBX4uRuU/zOnARHsorA3AB0rNFL9KxSV9CsqM
0AnvjoFHcOj/M43kba112GZNAirq+lfy/ivUc06/S+KROp3Ny3SqYdwZZcJw+5iTm5VeNAY9uqEk
Br3tcqI3XbYf0Xe0elXzZd8WT8O2Bwyl4q9MmarG4BkIOKhQA1zZDJvQxzB9Er1NXP5hovPh6LFN
lJ4ctDPQRHiZH9+TrjDUPrO2yYUlv6ZEOCh0ovtuQzDA02JRuzAxYj0Kljb5Y5xzhM+N0X+Jdmmp
VViyIGbX3ugYpl5KxnRaChWeoJHf3F+969uR1wyDv/vD85PjHOOv/A8ucZGTdN+V9+gU59r60znz
Gi92EM8uM0JA23dlrMX93rRrC5ndD0qRgIJ4E+nlH3LNote/SMZ3qgzG9JcqpHncJoy8kz62Xqnm
3WpoJyVHEeEAEoVBmMp/ZEbUxAHa4Rg4ARZN3qMAa7EKOvCho0j6Vv0naV1sMViwQZTZKRjDwc2N
MUVx/+16fdlFcJ/C6E1ue0CoXYp6HEDcWX2O0FJLtv3rlHG1cUsV+vzZVjLM4gVGePpdKOEFwt2n
HrmX677Tw9Z1FN6p2qhEBPrZ9tDBsFxXuGXnQ/x+KmNqoc4oEB3iMIA8Faaklb0NIFPcYb/gURbs
S4Da6epJWiMiq4mUYHmORLuW0Omjp2+pOoA4MShITC5YLiO7j3EE4+gL/GWxjO8/7QOZqodJ0Yhv
9EKeXl2Herkhyma0dvtBhGwOYLKBa/FzfBfiTPe6V5jY86oh+BLklVlioMGSi0vborNV+FbMsVFh
Sx0FmkGsbgazLgNgo+m2P0uSW1D5vvwlCR7P6zihqwVsM/1OEZsi3kcvtflrdrcfxnanpDTm6GQZ
nsL3RiCdSbnhSefEC6QdcLasaPGh+ZV6dLTeZYUMbzdGQB8hDQUvSLByTj+sUUvwSa6BH3xgtGO5
swDI/edErIxR/kx+PltOn01dswHjI60LWuYYlUnHSXNVdlofbZRRxcmGSigprmbBcrjqIRRhVUm2
tjWVnyro+NCNHPtJyL/2CvUMJrWOHRmW/a8zF7FP5MhlJlgQwfVr48XdodkHRkAG1kVzlTIyhAPf
9MS/Pbl4UEGBFvlp+Nk94EnEad91JMWR2IxWoFcKMk8hbWtKsRJ3K+aa36KiMh1ER3WBK79XU+wD
f2jD1RfXcX6BEbBiaI+ZWa+mDWl27NNMnXlrcBRD/PYkRN5p6B/PqXfFbxLpvy3JmZ5gUU4qkmM6
5cZQ001DmbMzkm7ADqFrLj7txCITg7vAQaESraho6+FhleZ5FVd08itEQpcdaOda8AWwkz1Bpdex
WKP+FMmAfOzggQrLVLYW2uVJT+brLbzEZrgSjwSLXcQDuKx5TuGMqw4X9u/9NuSuQlgTkX+3IFfb
xCjDGLb6XzpAHjsZ6Rn5AmKBi1PN14Sv4J9nHh6wtOYm8137JYu2Q1lV6/meUh3CokmIeOc6xlYU
Yx4Bnmm+Oz2FcIXOtoyQGK3NchRxsOlgxNlzFAu5SR38qFL+PgnKrfQO5jBuc46s4uvT+FZZ5hMW
YSnBXG7ODm7x3M7TsPe+rVqxQrojQYIRhdP5/g/TTz7AqazuWt3xOng7ItxjG/sY7wIRviab0a5X
jti+bp/13eWqM4WoY40nKshIFfQXINx5DLPCyr8WHzIn7IpFwc4Y5Um/MXTME2Zdle/sYu5vClb/
aU9Wqnl1KGlt13xL3y2j8EwrwlLiYeZYlEuQr/uPM+1IhSj3CeIyRRNqvjUQNO9mCiYB/DeLjDWm
BaXD1METEeyH5pUv3B/ZQZXf4nWXXHgMUv8vC4MjSI9nhZEa5Fwkp5RpW4aq4iARs6XGSpYsOG++
VyHxIFApGvLjbQ9YD/U8h7fjP6+j667ESmjsgJPuDCfA+Yy+uPghjpwsRK2gtMA/8wqgqmJ/z5TR
VcN8eOlao8LHjxYhI+59ceDNWorw7t1JP5AU2hYktQaFLzXFkZVEqItjTBhmrtL9umu2U56ECv0A
ab+KDWxJC5KPhweSzB/T1+CsOHuvNoS+sXLrN2wMjIyahU691oEM0+QenmrZwxbHzyF4mVG/yikZ
6h2MZ5m+ZoBnMuBRBlTF+i1RUMUyZ5kicNSu83Ry6kZU3o7AM947wolBIonYtjY8vpBctZ3+7p0f
kfrF7VpYKAArmMdHVWwhofKFCCSb6SMbdmfsRuO/AlVxr/j/cQHsgonZpuQ7Y0pIHlZWy7C6dxS5
Raa6migU9RIDTIe2EQvenNQCJFAfUblc5BYVrdo1sMtmZaNVMceflLslV5uzEjSJsLcDpYfT/16k
bR3kfC6CJvfMW23Y3+xuQDX0pNgUKWdSZ01SevQDqOK3xRf3VbZp6WVTe6gtU0GyJ06DPUc3XTUH
+PT7SxwSkvPheYf2HJr0U/+TKJukwvZm66aGkF65duFyO0Hc3N5MdcuAFB2EVTyP5jQOsycmwShW
UGNmuqX48AfjIFkujQNq87ZXFM16riodFaAfcY8k3FxQuheCte7yB62u8eacLH4Bt/DQL8o1ZI2O
e00/BwMAtT92R953jzguCg7aeag8go1Bj5azmmaPAv6+9MDS7uTWFfBU3efL/s8MngBLF6tD2+NH
9xY1inqNU1vmQREJn0CZNzP4gTEb+8N3/SyTozdgFvOvZxOPW+A3RqcM/22SJWpYMME3cELIs7QE
Bu/y1E/vAuSXqwEQW4AfCoBnauM4DA7AEp5d1ZnZ32DwRH0a6dK7hnWc+JJ5wKwH77rkF13vFw/m
2KAnY2T7OPbUEfGW3ueRtRLwIBQtJSCTBD3Ej88Kxb5mVlO8d2taDc7zKm7b345nQu2IcRclREc2
rXxMfQaRLE9bwjTg533tdc5iEmM2N2wf3pgWni/kMM70eMbWxzvNfUuY3aF0Zq3hz+9iV9Ji77IG
VihwETJ/KngOlK993T2//MoGbetlhqp1JWHLuGRNNY2Riy9T1I5nuwjCZtCUjCDs2ZFg/GEPbVxp
GVCCgOHQ5zvUn5qwyVNByfJzyliLHY78ZXTKxPPOq8HwIdxz7K3S4LShYN1pM5FLnijHg4VdHum4
/GLKOYSuWLOGjn+4NGIIzsu9AGrVb+lELNK/9kurlljVNzLhGJaKL0iz3jD4qx7tAE8LSQaHNudx
Sf8ujI9gVtiwYueeDIyVhE1Usa8Mo9vfGNUh5uMCAAI1kJ0eXvQnuJ9CD65GTjj2N7zsuHAYgU8G
vf+TYLiCyHaPpeOhbPb0XKXRMx0abztflH1pL1JfQG3jDnG5PybxP99pyITBmuT05iPc91gdu7Qt
vfR/s2rKIUDeEfrKUVaYkFjV3ZAP2bAZ8J7YIEBpdPe4Af2mKH6NXxHJeihwX7YyTqyBlOj9wXpv
GRYx4y66BLn4n/FTOqGyeEkOsc6aaSln1smEzmj/dHo8VVFr03m4CPCpmGj/8/HlPAcJDeAwObEt
a+IdQvvHNDV11vWYBLw/vyS4kYyPxThyQLencJeoA1gRc6VeRw7HGmBcw7NyX2ORLYRzFc16jAUk
bdYoJUomOHTn4HbDlvCAiyf+gCY5yxkyMxBxEXn9m9mbtLUTFxl+AD/76EsWPUstCv8gJAQL8JT0
y5zegH1Heu7+Q1yeA4dx3hYWekT8hmIzYUCC00s0/Fv+1NyLtXcfu5dqnJr/wpAO3N2DCJsx9NpC
zQMUx8USrIIo+oe8IVoUTamSECGqT+Y5yZhhG7G1QqWC9O5OhVPkUAnFvz98ARR9Fw8jgwYhflW7
vyJbrSc27dkTWQ/6/xNbCRSJNz//ij9FXwT2tpLc6hYDZ3Ur2SPJhwlO6Bx4h9bfM9PvD2YkPi/1
QWb6oNYskGhfwsWFkJRuDzRc3AOUsfnnQrtFZRGSVgYz1l31XM/7L3EhLj3HMm3qgWE0rgIe5U7u
F5qSZM5b4vprcn61QepHXmCTKMSkcheRvFE7JRjDpQEsrjSdMXS0x6b9gkf1n8FrgVNpk0EO3oDz
likDejoIcBcuiSfkHQwnYsuAembiFh7iJ8ToRzaHRSK6pvWJ+/VDjCiZxog+fcn3viM9bG8BU/rY
L6D+1haEddeXFx6KMQ6WmBOMYnThT+gULl+TzOY+7sKzAcdAEN/ZnF0vGbNMtL7AgvZWLaR0o23M
3jdg/q/CmcC9TDMMyCoz+efToMxeDlN6PTtPapGFrX6KXrSVFnYSe6mVx1QLqOKPI7wKPn5a4+BZ
VBc6AsAuXicRCEkvrxcEphJ0+e7s3B0zfgbFDhl5cYrA9ADnSs/t4jR1+IBfnYFr9xHuqCCAohCc
Bo2G/Gc6Q5nozrRptk2qCr6jYWsui7ClVxrh4hQ4LfsDH81K5xJjsfyFn5hZ/i8fXVcNCREBtk2h
s7pAmiVtLr3qlvxzTNbuhywcMIDMW1+eHBgs4MBsMK8gyZO++lmM9+m5rmkx7lmdpHOtfJaVS2bg
F4+J+w3i61+obiVL54GFkXRyuaCJ0HusJvabwum0go1wK0c2R6k/1dkCHupZNLJ6sHPlEGzJQn7J
iErgL/JdFV9dOxQlTTNAEXAynwe683Kvu2Z+5l+g9Aakz6vRL05hTfU8ZbcSFRfwvvVwFyjl9CqY
1tlNklaChNU2bb1ZBfmmFS8ZHeAB5m1xrPlFPln43CZZAL7d+2U2LtxOMgzEQ62LoP082rvlaVWF
YZ2bktJ4yOa42w9OomFzUAz2jzh2DdJX2D02yuSASAr+Rw45cOAR7MEhukg1tWuS8yiEzNOBSRK5
8pNcZ4aVpKG1d9HWj4BzojwkIODB9fSr21cKlrkc/ndlGh2zsYLrVzQWDwFICAbYoZomxWc4ZHkz
zDzrv2UiQJpCBjZcSPJcH/CfYbwUA32+lIYa3zyYVe0FG3IK9wPRIW7kjMWWGZz+4fpuO1oKjMOK
5AZ2FC3AvDyvkZczpt381/hhmo7808Sjhvxum/tmWWE6sbkTaco2pCXkLM+QetYq+IZGzMaWOwa5
BTsi0baBMQ8VsbE9AQmHh/B2JK2gni6jL9Px4hTXBsN0BhKBc/q/vAAK1FN6lWevB47erVkq/rHQ
IpFTyNqo/3xxnVUV7f9RP8J6RRQMQ/00pxBMJAVdUg+jNGR4YvklB+BO7NAu9F9tk0TeWCPdpOVW
e/m2DCnWouOCxFGWs3WjOnQJmId4BeEhdfNCfzRT49W2y7t8mG3rmk6gndRyeekXJCjhpNRS6oB3
LGOFaPQ/Y93sPROV0sqMgnqaYr//RivYoU0ZG+AKLnrWa+oUJBqNEJ9wz2Cz0fG7CzcZOsZ2
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
