//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/05/2020 01:44:23 AM
// Design Name: 
// Module Name: sys_clk_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sys_clk_gen(
    input clk_in,
    input rst_in,
    output clk_out,
    output rst_out
    );

    clk_wiz_vga inst_cw(.clk_in1(clk_in), .reset(rst_in), .clk_out1(clk_out), .locked(clk_out_good));

    reg [2:0] r_pipe_rst;

    always @(posedge clk_out or negedge clk_out_good)
        if (~clk_out_good)
            r_pipe_rst <= 3'b111;
        else
            r_pipe_rst <= { r_pipe_rst[1:0], 1'b0 };

    assign rst_out = r_pipe_rst[2];
endmodule
