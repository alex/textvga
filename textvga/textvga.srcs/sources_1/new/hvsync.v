`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/04/2020 11:28:55 PM
// Design Name: 
// Module Name: hvsync
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hvsync(
    input clk,
    input rst,
    output reg [9:0] sx,
    output reg [9:0] sy,
    output active,
    output vga_h_sync,
    output vga_v_sync,
    output [6:0] text_x,
    output [4:0] text_y,
    output [2:0] char_x,
    output [3:0] char_y
    );

    reg [9:0] counter_x, counter_y;

    wire counter_x_max = (counter_x == 800 - 1);
    wire counter_y_max = (counter_y == 525 - 1);
    
    // horizontal timings
    parameter HA_END = 639;             // end of active pixels
    parameter HS_STA = HA_END + 16;     // sync starts after front porch
    parameter HS_END = HS_STA + 96;     // sync ends
    parameter LINE   = 799;             // last pixel on line (after back porch)

    // vertical timings
    parameter VA_END = 479;             // end of active pixels
    parameter VS_STA = VA_END + 10;     // sync starts after front porch
    parameter VS_END = VS_STA + 2;      // sync ends
    parameter SCREEN = 524;             // last line on screen (after back porch)

    always @(posedge clk)
        if (rst)
        begin
            sx <= 0;
            sy <= 0;
        end
        else if (sx == LINE)
        begin
            sx <= 0;
            sy <= (sy == SCREEN) ? 0 : sy + 1;
        end
        else
            sx <= sx + 1;

    assign text_x = sx[9:3];
    assign text_y = sy[8:4];
    assign char_x = sx[2:0];
    assign char_y = sy[3:0];

    assign vga_h_sync = ~(sx >= HS_STA && sx < HS_END);
    assign vga_v_sync = ~(sy >= VS_STA && sy < VS_END);
    assign active = (sx <= HA_END && sy <= VA_END);
endmodule