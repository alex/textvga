//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/03/2020 10:35:06 PM
// Design Name: 
// Module Name: textvga_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module textvga_top(
    input CLK100MHZ,
    input ck_rst,
    output [3:0] VGA_R,
    output [3:0] VGA_G,
    output [3:0] VGA_B,
    output VGA_HS_O,
    output VGA_VS_O,
    output [7:6] jc,
    output [3:0] led
    );

    sys_clk_gen cgen(.clk_in(CLK100MHZ), .rst_in(~ck_rst), .clk_out(sys_clk), .rst_out(sys_rst));

    wire       T_active;
    wire       T_hsync;
    wire       T_vsync;
    wire [6:0] T_text_x;
    wire [4:0] T_text_y;
    wire [2:0] T_char_x;
    wire [3:0] T_char_y;

    hvsync hvgen(.clk(sys_clk), .rst(sys_rst), .active(T_active), .vga_h_sync(T_hsync), .vga_v_sync(T_vsync), .text_x(T_text_x), .text_y(T_text_y), .char_x(T_char_x), .char_y(T_char_y));

    reg       D_active;
    reg       D_hsync;
    reg       D_vsync;
    reg [6:0] D_text_x;
    reg [4:0] D_text_y;
    reg [2:0] D_char_x;
    reg [3:0] D_char_y;
    
    wire [7:0] D_glyph;
    
    text_sram ts(.clk(sys_clk), .addr(T_text_y * 80 + T_text_x), .data(D_glyph));
    
    always @(posedge sys_clk)
    begin
        D_active <= T_active;
        D_hsync  <= T_hsync;
        D_vsync  <= T_vsync;
        D_text_x <= T_text_x;
        D_text_y <= T_text_y;
        D_char_x <= T_char_x;
        D_char_y <= T_char_y;
    end

    reg       F_active;
    reg       F_hsync;
    reg       F_vsync;
    reg [6:0] F_text_x;
    reg [4:0] F_text_y;
    reg [2:0] F_char_x;
    reg [3:0] F_char_y;
    
    wire [7:0] glyph_row;
    wire [7:0] glyph_row_reversed = { glyph_row[0], glyph_row[1], glyph_row[2], glyph_row[3], glyph_row[4], glyph_row[5], glyph_row[6], glyph_row[7] };
    wire luma = glyph_row_reversed[F_char_x];
    
    font_rom font(.clock(sys_clk), .addr({ D_glyph[6:0], D_char_y }), .data(glyph_row));

    always @(posedge sys_clk)
    begin
        F_active <= D_active;
        F_hsync  <= D_hsync;
        F_vsync  <= D_vsync;
        F_text_x <= D_text_x;
        F_text_y <= D_text_y;
        F_char_x <= D_char_x;
        F_char_y <= D_char_y;
    end

    reg R_active;
    reg R_hsync;
    reg R_vsync;
    reg R_luma;

    always @(posedge sys_clk)
    begin
        R_active <= F_active;
        R_hsync <= F_hsync;
        R_vsync <= F_vsync;
        R_luma <= luma;
    end
    

    assign VGA_HS_O = R_hsync;
    assign VGA_VS_O = R_vsync;

    assign VGA_R = (R_active) ? (R_luma ? 4'b1111 : 4'b0000) : 4'b0000;
    assign VGA_G = (R_active) ? (R_luma ? 4'b1111 : 4'b0000) : 4'b0000;
    assign VGA_B = (R_active) ? (R_luma ? 4'b1111 : 4'b0000) : 4'b0000;
    
    // debug tools
    reg [22:0] activity_monitor;
    always @(posedge sys_clk)
        if (sys_rst) activity_monitor <= 0;
        else activity_monitor <= activity_monitor + 1;
    assign led[3] = activity_monitor[22];
    assign led[2] = luma;
    assign led[0] = sys_rst;

    assign jc[7] = sys_clk;
    assign jc[6] = sys_rst;
endmodule
