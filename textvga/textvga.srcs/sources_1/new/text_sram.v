`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/06/2020 12:58:03 AM
// Design Name: 
// Module Name: text_sram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module text_sram(
    input clk,
    input [11:0] addr,
    output reg [7:0] data
    );

    reg [7:0] text_memory [0:4095];
    initial begin
        $display("Loading inital text SRAM contents...");
        $readmemh("text_sram.mem", text_memory);
    end

    always @(posedge clk)
        data <= text_memory[addr];

endmodule
