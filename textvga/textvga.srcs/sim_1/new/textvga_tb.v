`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/09/2020 08:25:09 PM
// Design Name: 
// Module Name: textvga_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module textvga_tb;
    reg CLK100MHZ, ck_rst;
    
    initial begin
        CLK100MHZ = 0;
        ck_rst = 1;
        #200 ck_rst = 0;
    end
    always #5 CLK100MHZ = ~CLK100MHZ;

    wire [3:0] VGA_R, VGA_G, VGA_B;
    wire [7:6] jc;

    sys_clk_gen cgen(.clk_in(CLK100MHZ), .rst_in(ck_rst), .clk_out(sys_clk), .rst_out(sys_rst));
    
    wire [9:0] counter_x;
    wire [9:0] counter_y;
    
    wire [3:0] VGA_R;
    wire [3:0] VGA_G;
    wire [3:0] VGA_B;
    wire VGA_HS_O;
    wire VGA_VS_O;
    
    
    hvsync hvgen(.clk(sys_clk), .rst(sys_rst), .vga_h_sync(VGA_HS_O), .vga_v_sync(VGA_VS_O), .sx(counter_x), .sy(counter_y), .active(active));

endmodule
